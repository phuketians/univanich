<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [];

    public function region()
    {
        return $this->belongsTo('App\Region', 'region_id');
    }

    public function status()
    {
        return $this->belongsTo('App\ProjectStatus', 'status_id');
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'region_id' => 'required',
            'status_id' => 'required',
            'title'     => 'required',
            // 'image'     => 'mimes:jpeg,jpg,png,gif|required|max:10000'
        ];
    }
}

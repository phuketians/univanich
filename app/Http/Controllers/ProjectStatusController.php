<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectStatusRequest;
use App\ProjectStatus;


class ProjectStatusController extends Controller
{

    public function index()
    {
        $projectStatuses = ProjectStatus::paginate(20);

        $data['projectStatuses'] = $projectStatuses;

        return view('backend.project-status.index', $data);
    }


    public function create()
    {
        //
    }


    public function store(ProjectStatusRequest $request)
    {
        ProjectStatus::create($request->all());

        return redirect()->back()->with('success', 'Project Status Added Successfully!');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $projectStatuses = ProjectStatus::paginate(20);

        $data['projectStatuses'] = $projectStatuses;

        $projectStatus = ProjectStatus::findOrFail($id);

        $data['projectStatus'] = $projectStatus;        


        return view('backend.project-status.edit', $data);
    }


    public function update(ProjectStatusRequest $request, $id)
    {
        $projectStatus = ProjectStatus::findOrFail($id);

        $projectStatus->update($request->all());

        return redirect()->back()->with('success', 'Project Status Updated Successfully!');
    }


    public function destroy($id)
    {
        $projectStatus = ProjectStatus::findOrFail($id);

        $projectStatus->delete();

        return redirect()->route('status.index')->with('success', 'Project Status Deleted Successfully!');
    }
}

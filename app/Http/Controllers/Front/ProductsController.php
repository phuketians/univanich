<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    
    public function cpoPko()
    {
        return view('frontend.products.cpo-pko');
    }

    public function seedSeedlings()
    {
        return view('frontend.products.seed-seedlings');
    }
    public function seeds()
    {
        return view('frontend.products.seeds');
    }
    public function Seedlings()
    {
        return view('frontend.products.seedlings');
    }

    public function biogas()
    {
        return view('frontend.products.biogas');
    }

    public function byProducts()
    {
        return view('frontend.products.by-products');
    }
}

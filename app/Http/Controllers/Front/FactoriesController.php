<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class FactoriesController extends Controller
{

    public function index()
    {
        return view('frontend.operations.factories.index');
    }
    
    public function topi()
    {
        return view('frontend.operations.factories.topi');
    }

    public function siam()
    {
        return view('frontend.operations.factories.siam');
    }

    public function lamthap()
    {
        return view('frontend.operations.factories.lamthap');
    }
    
    public function chokvallapa()
    {
        return view('frontend.operations.factories.chokvallapa');
    }
    
    public function pabon()
    {
        return view('frontend.operations.factories.pabon');
    }
    
    public function ucpoc()
    {
        return view('frontend.operations.factories.ucpoc');
    }
    
}
<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class EstatesController extends Controller
{

    public function index()
    {
        return view('frontend.operations.estates.index');
    }
    
    public function topi()
    {
        return view('frontend.operations.estates.topi');
    }

    public function cheanvanich()
    {
        return view('frontend.operations.estates.cheanvanich');
    }

    public function siampalm()
    {
        return view('frontend.operations.estates.siampalm');
    }
    
    public function lamthap()
    {
        return view('frontend.operations.estates.lamthap');
    }
    
    public function nanua()
    {
        return view('frontend.operations.estates.nanua');
    }
    
    public function akapojana()
    {
        return view('frontend.operations.estates.akapojana');
    }
    
    public function chauat()
    {
        return view('frontend.operations.estates.chauat');
    }
    
    public function khlongtom()
    {
        return view('frontend.operations.estates.khlongtom');
    }
    
    public function wannee()
    {
        return view('frontend.operations.estates.wannee');
    }
    
    public function pabon()
    {
        return view('frontend.operations.estates.pabon');
    }
    
}
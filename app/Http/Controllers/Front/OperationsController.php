<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class OperationsController extends Controller
{
    
    public function index()
    {
        return view('frontend.operations.index');
    }

    public function estates()
    {
        return view('frontend.operations.estates');
    }

    public function factories()
    {
        return view('frontend.operations.factories');
    }

    public function nurseries(){
        return view('frontend.operations.nurseries');
    }
}

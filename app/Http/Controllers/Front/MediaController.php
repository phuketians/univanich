<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    
    public function index()
    {
        return view('frontend.media.index');
    }

    public function news2008()
    {
        return view('frontend.media.news2008');
    }
    
    public function news2007()
    {
        return view('frontend.media.news2007');
    }

    public function news2006()
    {
        return view('frontend.media.news2006');
    }
    
}

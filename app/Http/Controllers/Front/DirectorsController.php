<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class DirectorsController extends Controller
{
    
    public function index()
    {
        return view('frontend.directors.index');
    }
    
    public function apiragVanich()
    {
        return view('frontend.directors.apirag-vanich');
    }
    
    public function phortchanaManoch()
    {
        return view('frontend.directors.phortchana-manoch');
    }
    
    public function kanchanaVanich()
    {
        return view('frontend.directors.kanchana-vanich');
    }
    
    public function palatTittinutchanon()
    {
        return view('frontend.directors.palat-tittinutchanon');
    }
    
    public function chantipVanich()
    {
        return view('frontend.directors.chantip-vanich');
    }
    
    public function proteSosothikul()
    {
        return view('frontend.directors.prote-sosothikul');
    }
    
    public function johnClendon()
    {
        return view('frontend.directors.john-clendon');
    }
    
    public function suchadChiaranussati()
    {
        return view('frontend.directors.suchad-chiaranussati');
    }
    
    public function supapangChanlongbutra()
    {
        return view('frontend.directors.supapang-chanlongbutra');
    }
    
    public function pramoadPhornprapha()
    {
        return view('frontend.directors.pramoad-phornprapha');
    }
    
}
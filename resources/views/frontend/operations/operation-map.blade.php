@extends('frontend.layouts.master')
@section('title', 'Operation Map')
@section('content')

<script type="text/javascript">
function changeMap(loc) {
		 console.log('Change iframe', loc);
     document.getElementById('mapframe').src = loc;
}
</script>

<section class="PowerPlantWrap power-plants-map-wrapper">


    <div class="map" id="operation_map">

        <iframe id="mapframe" src="https://www.google.com/maps/d/u/1/embed?mid=1AqStJJ5HbXNjI16iInX03wDs8pXEPLcd"
            width="100%" allowfullscreen height="920"></iframe>
    </div>

    <div class="map-list-wrapper">

        <div class="plant-lists">
            <ul>
                <li>
                    <a onclick="changeMap('https://www.google.com/maps/d/u/1/embed?mid=1AqStJJ5HbXNjI16iInX03wDs8pXEPLcd')"
                        href="javascript:void(0);">Estates<span><img
                                src=" {{ asset('images/estate.png' ) }} "></span></a>
                </li>
                <li>
                    <a onclick="changeMap('https://www.google.com/maps/d/u/1/embed?mid=1WclUHdmAlAIILgajodqVQaMRQcu41AvD')"
                        href="javascript:void(0);">Nurseries<span><img
                                src=" {{ asset('images/nursery.png' ) }} "></span></a>
                </li>
                <li>
                    <a onclick="changeMap('https://www.google.com/maps/d/u/1/embed?mid=1DjKpmWpd57k0dO1jYToOHqfNAIXXMKKV')"
                        href="javascript:void(0);">Factories & Biogas<span><img
                                src=" {{ asset('images/factories.png' ) }} "></span></a>
                </li>
            </ul>
        </div>
    </div>

</section>
@endsection
@extends('frontend.layouts.master')
@section('title', 'Cha Uat')
@section('content')


@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Cha Uat",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],
                        
                        [
                            'item_title' => 'Operations',
                            'url' => 'operations'
                        ],

                        [
                            'item_title' => 'Estates',
                            'url' => 'estates'
                        ],

                        [
                            'item_title' => 'Cha Uat'
                        ],
                    ]
            ]
        )
        
<!--services details start-->
    <section class="service-details-section ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="service-details-wrap">
                        <img src="/images/estates/est6.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="sidebar-right pl-4">
                        <!--Factory Details-->
                        <aside class="widget widget-categories">
                            <div class="widget-title">
                                <h5>CHA UAT</h5>
                            </div>
                            <ul class="primary-list mt-25">

                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Perm. Workers: </strong>1</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Cont. Workers: </strong>43</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Total Workers: </strong>44</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Total Land (Ha): </strong>285.30</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Mature (Ha): </strong>85.30 </li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Mature (%): </strong>29.90%  </li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Immature (Ha): </strong>200.00  </li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Immature (%): </strong>70.10%  </li>
                            </ul>
                        </aside>
                        <div class="action-btns mt-4">
                            <a href="/operation-map" class="btn btn primary-solid-btn mr-1">Back to Operation Map</a>
                            <a href="/estates/akapojana" class="btn btn primary-solid-btn mr-2">Next</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--services details end-->

@endsection
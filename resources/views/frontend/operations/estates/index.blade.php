@extends('frontend.layouts.master')
@section('title', 'Estates')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Estates",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Estates'
]
]
]
)


<!--about us section start-->
<section class="about-us-section ptb-100">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-md-6 col-lg-6">
                <div class="about-img-wrap">
                    <img src="/images/estates/estates01.jpg" alt="organic planting image"
                        class="img-fluid rounded shadow-sm">
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="map" id="operation_map">

                    <iframe id="mapframe"
                        src="https://www.google.com/maps/d/u/1/embed?mid=1AqStJJ5HbXNjI16iInX03wDs8pXEPLcd" width="100%"
                        allowfullscreen height="405"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="col_wrap ptb-10" style="min-height: 500px">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-md-12 col-lg-12">
                <div class="about-content-right mb-md-4 mb-lg-0">
                    <h2>Estates</h2>
                    <p>Univanich owns a total of 10 estates, located in Krabi and Phang Nga province, Thailand.
                        The total area of these estates amount up to 37,316 rai (5970.6 Ha) with an additional
                        2,034 rai (325.4 Ha) of non-yielding areas or 5.45% of total estate area. </p>
                    <p>The company has started its first plantings in 1969 and started its replanting projects
                        from 1992 onwards at roughly 4% of total area, every following year. This creates a
                        stable, diverse, and continually upgrading population of palm within the estates. In
                        2020, 9.7% of all fresh palm fruits going into our mills are from our estates. </p>
                    <div class="action-btns mt-4">
                        <a href="/operation-map" class="btn primary-solid-btn mr-2">Operation Map</a>
                    </div><br>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/topi">TOPI</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/cheanvanich">CHEAN VANICH</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/nanua">NA NUA</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/lamthap">LAMTHAP</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/chauat">CHA UAT</a></li>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <ul>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/akapojana">AKAPOJANA</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/siampalm">SIAM PALM</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/wannee">WANNEE</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/khlongtom">KHLONGTOM</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a
                                    href="/estates/pabon">PABON</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </div>
    <!--container end-->
</section>
<!--about us section end-->

@endsection
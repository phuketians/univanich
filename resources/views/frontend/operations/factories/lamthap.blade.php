@extends('frontend.layouts.master')
@section('title', 'Lamthap')
@section('content')


@include('frontend.layouts.breadcrumb',
[
'page_title' => "Lamthap",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Operations',
'url' => 'operations'
],

[
'item_title' => 'Factories',
'url' => 'factories'
],

[
'item_title' => 'Lamthap'
],
]
]
)

<section class="col_wrap ptb-100" style="min-height: 830px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <br>
                <h3>Lamthap Factory</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="service-details-wrap">
                    <img src="/images/siam-fac-img.jpg" alt="services" class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="service-details-wrap">
                    <img src="/images/siam-fac-img2.jpg" alt="services" class="img-fluid rounded shadow-sm" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="sidebar-right pl-4">
                    <!--Factory Details-->
                    <aside class="widget widget-categories">
                        <div class="widget-title">
                        </div>
                        <table>
                            <tr>
                                <th colspan="3">Capacity (T/hr)</th>
                                <th>Biogas Electricity Generation (MW)</th>
                                <th>Commission Date</th>
                                <th>Location</th>
                            </tr>
                            <tr>
                                <th>Fresh Fruit Bunches</th>
                                <th colspan="2">Palm Kernel</th>
                                <td rowspan="2">
                                    <center>1.904</center>
                                </td>
                                <td rowspan="2">
                                    <center>2004</center>
                                </td>
                                <td rowspan="2">
                                    <center>Krabi Province</center>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <center>45</center>
                                </td>
                                <td>
                                    <center>155</center>
                                </td>
                            </tr>
                        </table>
                    </aside>
                    <div class="action-btns mt-4">
                        <a href="/operation-map" class="btn btn primary-solid-btn mr-1">Back to Operation Map</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <br><br>
                <h5>
                    <center>Quick Links to Other Factories</center>
                </h5>
            </div>
        </div>

        <!--clients logo start-->
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="owl-carousel owl-theme clients-carousel dot-indicator">
                <a href="/factories/topi">
                        <div class="item single-client">
                            <img src="/images/topi.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/siam">
                        <div class="item single-client">
                            <img src="/images/siam.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/lamthap">
                        <div class="item single-client">
                            <img src="/images/lamthap.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/chokvallapa">
                        <div class="item single-client">
                            <img src="/images/cvp.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/pabon">
                        <div class="item single-client">
                            <img src="/images/pabon.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/ucpoc">
                        <div class="item single-client">
                            <img src="/images/ucpoc.png" alt="factory" class="client-img">
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!--clients logo end-->
    </div>
</section>
<!--services details end-->

@endsection
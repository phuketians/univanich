@extends('frontend.layouts.master')
@section('title', 'Factories and Biogas')
@section('content')


@include('frontend.layouts.breadcrumb',
[
'page_title' => "Factories and Biogas",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Factories and Biogas'
],
]
]
)

<section class="col_wrap ptb-100" style="min-height: 830px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table responsive table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Capacity (T/hr)</th>
                            <th>Steam/electricity</th>
                            <th>Commission Date</th>
                            <th>Location</th>
                            <th># Employees</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="/factories/topi"><strong>TOPI</strong></a></td>
                            <td>60 FFB 190 PK</td>
                            <td>952 kW x 3 = 2.856 MW</td>
                            <td>
                                <center>1974</center>
                            </td>
                            <td>Krabi Province</td>
                            <td>
                                <center>128</center>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="/factories/siam"><strong>Siam</strong></a></td>
                            <td>45 FFB</td>
                            <td>952 kW x 3 = 2.856 MW</td>
                            <td>
                                <center>1980</center>
                            </td>
                            <td>Krabi Province </td>
                            <td>
                                <center>69</center>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="/factories/lamthap"><strong>Lamthap</strong></a></td>
                            <td>45 FFB 155 PK</td>
                            <td>Biogas 952 kW x 2 = 1.904 MW</td>
                            <td>
                                <center>2004</center>
                            </td>
                            <td>Krabi Province</center>
                            </td>
                            <td>
                                <center>92</center>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="/factories/chokvallapa"><strong>CVP</strong></a></td>
                            <td>60 FFB</td>
                            <td>Biogas 2.828 MW under BOOT by TBEC</td>
                            <td>
                                <center>2013
                            </td>
                            <td>Phang Nga</center>
                            </td>
                            <td>
                                <center>66</center>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="/factories/pabon"><strong>Pabon</strong></a></td>
                            <td>30 FFB</td>
                            <td></td>
                            <td>
                                <center>2017
                            </td>
                            <td>Phatthalung</td>
                            <td>
                                <center>46</center>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="/factories/ucpoc"><strong>UCPOC</strong></a></td>
                            <td>30/60 </td>
                            <td></td>
                            <td></td>
                            <td>Philippines</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-6">
                <img src=" {{ asset('images/siam-fac-img2.jpg') }} " alt="Biogas" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <div class="map" id="operation_map">

                    <iframe id="mapframe"
                        src="https://www.google.com/maps/d/u/1/embed?mid=1DjKpmWpd57k0dO1jYToOHqfNAIXXMKKV" width="100%"
                        allowfullscreen height="400"></iframe>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p>All byproducts of our factories do not go to waste. One important byproduct of crushing mills is the
                    wastewater- also known as POME (Palm Oil Mill Effluent). POME is moisture extracted from palm
                    bunches and is filled with nutrients, with a high biological oxygen demand which can be harmful to
                    local ecosystems if discharged untreated. Furthermore, POME generates and emits methane, a powerful
                    greenhouse gas that damages the atmosphere. To solve this problem, we congregate our POME into a
                    biogas reactor, where bacteria anaerobically digests POME, generating large amounts of methane in
                    the process that we can use to generate electricity through a gas engine. POME that is treated
                    through our biogas reactors have their BOD reduced substantially, up to 5 times its initial size and
                    down to safe levels for the environment.</p>
                <p>The methane we generate goes to a gas engine which generates up to 2 MW of electricity per biogas
                    reactor, which we utilize as a backup generator for our operations, as well as selling the surplus
                    back to the national grid.</p>

                <p style="margin-bottom: 0">In doing so, our environmental objectives have been achieved:</p>
                <ul class="ml-5">
                    <li>1. A large reduction in Greenhouse Gas Emissions</li>
                    <li>2. The renewable energy (biogas) source has replaced fossil fuels</li>
                    <li>3. Wastewater treatment has improved</li>
                    <li>4. Factory efficiencies have improved</li>
                    <li>5. New skilled jobs have been created in rural communities</li><br>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <br><br>
                <h5>
                    <center>Quick Links to Factories</center>
                </h5>
            </div>
        </div>

        <!--clients logo start-->
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="owl-carousel owl-theme clients-carousel dot-indicator">
                    <a href="/factories/topi">
                        <div class="item single-client">
                            <img src="/images/topi.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/siam">
                        <div class="item single-client">
                            <img src="/images/siam.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/lamthap">
                        <div class="item single-client">
                            <img src="/images/lamthap.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/chokvallapa">
                        <div class="item single-client">
                            <img src="/images/cvp.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/pabon">
                        <div class="item single-client">
                            <img src="/images/pabon.png" alt="factory" class="client-img">
                        </div>
                    </a>
                    <a href="/factories/ucpoc">
                        <div class="item single-client">
                            <img src="/images/ucpoc.png" alt="factory" class="client-img">
                        </div>
                    </a>
                </div>
            </div>
            <!--clients logo end-->
        </div><!-- /.container -->
</section><!-- /.col_wrap -->





@endsection
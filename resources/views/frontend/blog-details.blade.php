<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="BizBite corporate business template or agency and marketing template helps you easily create websites for your business.">
    <meta name="author" content="ThemeTags">


    <!--title-->    
    <title>News Details || Univanich</title>

    <!--favicon icon-->
    <link rel="icon" href="img/favicon.png" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans:400,600&display=swap" rel="stylesheet">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!--Magnific popup css-->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!--Themify icon css-->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!--Fontawesome icon css-->
    <link rel="stylesheet" href="css/all.min.css">
    <!--animated css-->
    <link rel="stylesheet" href="css/animate.min.css">
    <!--ytplayer css-->
    <link rel="stylesheet" href="css/jquery.mb.YTPlayer.min.css">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!--custom css-->
    <link rel="stylesheet" href="css/style.css">
    <!--responsive css-->
    <link rel="stylesheet" href="css/responsive.css">

</head>
<body>

<!--loader start-->
<div id="preloader">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!--loader end-->

<!--header section start-->
<header class="header">    
    <!--start navbar-->
    <nav class="navbar navbar-expand-lg fixed-top white-bg">
        <div class="container position-relative">
            <a class="navbar-brand" href="home.html">
                <img src="img/logo-white.png" alt="logo" class="img-fluid"/>
            </a>

            <div class="main-title">
                <div class="title"> Univanich Plam Oil Public Co., Ltd.</div>
                <div class="sub-title">
                    <span>Univanich | 45.50 (+2.25 | 5.70%)</span>                    
                    <span>
                        <a href="#">TH</a> |
                        <a href="#">EN</a>
                    </span> 
                </div>                
            </div>             

            <div class="navbar-header">

                <a class="navbar-brand text-uppercase" href="#">open menu</a>

                <button data-target="#top-nav" data-toggle="collapse" type="button" class="navbar-toggle">
                    <i class="fa fa-bars"></i>
                </button>
                
            </div>            
        </div>

        <div class="MenuWrap">
            <div class="container">
                <ul>
                    <li><a href="home.html">Home</a></li>
                    <li><a href="company_profile.html">Company Profile</a>
                        <ul>
                            <li><a href="chairman_report.html">Message from CEO and/or? Chairman</a></li>
                            <li><a href="principles-code.html">Code of Business Principles</a></li>
                            <li><a href="company_profile.html">Company Profile/Who we are</a></li>
                            <li><a href="timeline.html">Timeline</a></li>
                            <li><a href="#">Board of Directors</a></li>
                            <li><a href="#">Management Team</a></li>
                            <li><a href="#">Awards</a></li>                            
                        </ul>
                    </li>
                    <li><a href="#">Operations</a>
                        <ul>
                            <li><a href="estates.html">Estates</a></li>
                            <li><a href="factories.html">Factories</a></li>
                            <li><a href="#">Biogas</a></li>
                            <li><a href="nurseries.html">Nurseries</a></li>
                        </ul>

                        <a href="#">Products</a>

                        <ul>
                            <li><a href="#">CPO & PKO</a></li>
                            <li><a href="#">Seeds & Seedlings</a></li>
                            <li><a href="#">Byproducts</a></li>
                            <li><a href="#">Biogas</a></li>                        
                        </ul>
                    </li>

                    <li><a href="#">Investor Relations</a>
                        <ul>
                            <li><a href="company_profile.html">Company Profile</a></li>
                            <li><a href="financial_information.html">Financial Information</a></li>
                            <li><a href="#">Stock Information (stock quotes, embed like BGP)</a></li>
                            <li><a href="shareholder.html">Shareholder Information</a></li>
                            <li><a href="#">Newsroom</a></li>
                            <li><a href="awards.html">Awards</a></li>
                            <li><a href="#">Publications and Download</a>
                                <ul>
                                    <li><a href="#">Annual report</a></li>
                                    <li><a href="#">Company factsheet</a></li>
                                    <li><a href="#">Brochure</a></li>
                                </ul>
                            </li>                            
                        </ul>
                    </li>
                    <li><a href="#">Sustainability</a>
                        <ul>
                            <li><a href="#">CSR (Social Sustainability)</a></li>
                            <li><a href="#">RSPO (Environmental Sustainability)</a></li>
                            <li><a href="#">Corporate Governance</a></li>
                            <li><a href="#">Environment (Environmental Sustainability)</a></li>
                        </ul>                            
                    </li>
                    <li><a href="#">Media</a>
                        <ul>
                            <li><a href="#">News</a></li>
                            <li><a href="#">Downloads</a>
                                <ul>
                                    <li><a href="#">Fact sheets/brochures/etc</a></li>
                                </ul>
                            </li>
                        </ul>    

                        <a href="#">Careers</a>
                        <ul>
                            <li><a href="#">Careers Center</a></li>
                        </ul>

                        <a href="#">Contact Us</a>

                        <ul>
                            <li><a href="#">Whistleblowing</a></li>
                            <li><a href="#">Feedback and Inquiries</a></li>
                        </ul> 

                    </li> 
                </ul>
            </div>
        </div>        
    </nav>
</header>
<!--header section end-->

<!--body content wrap start-->
<div class="main">

    <!--header section start-->
    <section class="hero-section ptb-100 gradient-overlay"
             style="background: url('img/header-bg-5.jpg')no-repeat center center / cover">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-7">
                    <div class="page-header-content text-white text-center pt-sm-5 pt-md-5 pt-lg-0">
                        <h1 class="text-white mb-0">News Details</h1>
                        <div class="custom-breadcrumb">
                            <ol class="breadcrumb d-inline-block bg-transparent list-inline py-0">
                                <li class="list-inline-item breadcrumb-item">
                                    <a href="home.html">Home</a>
                                </li>
                                <li class="list-inline-item breadcrumb-item">
                                    <a href="blog.html">News</a>
                                </li>
                                <li class="list-inline-item breadcrumb-item active">News Details</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--header section end-->

    <!--blog section start-->
    <div class="module ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Post-->
                    <article class="post">
                        <div class="post-preview"><img src="img/blog/1-w.jpg" alt="article" class="img-fluid"/></div>
                        <div class="post-wrapper">
                            <div class="post-header">
                                <h1 class="post-title">Minimalist Chandelier</h1>
                                <ul class="post-meta">
                                    <li>November 18, 2016</li>
                                    <li>In <a href="#">Branding</a>, <a href="#">Design</a></li>
                                    <li><a href="#">3 Comments</a></li>
                                </ul>
                            </div>
                            <div class="post-content">
                                <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse nihil, flexitarian Truffaut synth art party deep v chillwave. Seitan High Life reprehenderit consectetur cupidatat kogi.</p>
                                <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod Pinterest in do umami readymade swag. Selfies iPhone Kickstarter, drinking vinegar jean shorts fixie consequat flexitarian four loko.</p>
                                <blockquote class="blockquote">
                                    <p>To be yourself in a world that is constantly trying to make you something else is the greatest accomplishment.</p>
                                    <footer class="blockquote-footer">Amanda Pollock, Google Inc.</footer>
                                </blockquote>
                                <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica.</p>
                                <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse nihil, flexitarian Truffaut synth art party deep v chillwave. Seitan High Life reprehenderit consectetur cupidatat kogi.</p>
                                <ol>
                                    <li>Digital Strategy</li>
                                    <li>Software Development</li>
                                    <li>Interaction Design</li>
                                </ol>
                                <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica.</p>
                                <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod Pinterest in do umami readymade swag. Selfies iPhone Kickstarter, drinking vinegar jean shorts fixie consequat flexitarian four loko.</p>
                            </div>
                            <div class="post-footer">
                                <div class="post-tags"><a href="#">Lifestyle</a><a href="#">Music</a><a href="#">News</a><a href="#">Travel</a></div>
                            </div>
                        </div>
                    </article>
                    <!-- Post end-->

                    <!-- Comments area-->
                    <div class="comments-area mb-5">
                        <h5 class="comments-title">3 Comments</h5>
                        <div class="comment-list">
                            <!-- Comment-->
                            <div class="comment">
                                <div class="comment-author"><img class="avatar img-fluid rounded-circle" src="img/client-2.jpg" alt="comment"/></div>
                                <div class="comment-body">
                                    <div class="comment-meta">
                                        <div class="comment-meta-author"><a href="#">Jason Ford</a></div>
                                        <div class="comment-meta-date"><a href="#">May 5, 2015 at 4:51 am</a></div>
                                    </div>
                                    <div class="comment-content">
                                        <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica.</p>
                                    </div>
                                    <div class="comment-reply"><a href="#">Reply</a></div>
                                </div>
                                <!-- Subcomment-->
                                <div class="children">
                                    <div class="comment">
                                        <div class="comment-author"><img class="avatar img-fluid rounded-circle" src="img/client-3.jpg" alt="comment"/></div>
                                        <div class="comment-body">
                                            <div class="comment-meta">
                                                <div class="comment-meta-author"><a href="#">Harry Benson</a></div>
                                                <div class="comment-meta-date"><a href="#">May 5, 2015 at 4:51 am</a></div>
                                            </div>
                                            <div class="comment-content">
                                                <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me. Photo booth anim 8-bit hella.</p>
                                            </div>
                                            <div class="comment-reply"><a href="#">Reply</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Comment-->
                            <div class="comment">
                                <div class="comment-author"><img class="avatar img-fluid rounded-circle" src="img/client-4.jpg" alt="comment"/></div>
                                <div class="comment-body">
                                    <div class="comment-meta">
                                        <div class="comment-meta-author"><a href="#">Henry Cain</a></div>
                                        <div class="comment-meta-date"><a href="#">May 5, 2015 at 4:51 am</a></div>
                                    </div>
                                    <div class="comment-content">
                                        <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica.</p>
                                    </div>
                                    <div class="comment-reply"><a href="#">Reply</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-respond">
                            <h5 class="comment-reply-title">Leave a Reply</h5>
                            <p class="comment-notes">Your email address will not be published. Required fields are marked</p>
                            <form class="comment-form row">
                                <div class="form-group col-md-4">
                                    <input class="form-control" type="text" placeholder="Name">
                                </div>
                                <div class="form-group col-md-4">
                                    <input class="form-control" type="text" placeholder="Email">
                                </div>
                                <div class="form-group col-md-4">
                                    <input class="form-control" type="url" placeholder="Website">
                                </div>
                                <div class="form-group col-md-12">
                                    <textarea class="form-control" rows="8" placeholder="Comment"></textarea>
                                </div>
                                <div class="form-submit col-md-12">
                                    <button class="btn secondary-solid-btn" type="submit">Post Comment</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Comments area end-->
                </div> 
            </div>
        </div>
    </div>
    <!--blog section end-->

</div>
<!--body content wrap end-->

<!--footer section start-->
<footer class="footer-section">  
    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg py-3">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12">
                    <p class="copyright-text pb-0 mb-0">Copyrights © 2020. All
                        rights reserved by Univanich Plam Oil Public Co. Ltd.                        
                </div>
                
            </div>
        </div>
    </div>
    <!--footer copyright end-->
</footer>
<!--footer section end-->

<!--bottom to top button start-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="ti-angle-up"></span>
</button>
<!--bottom to top button end-->


<!--jQuery-->
<script src="js/jquery-3.4.1.min.js"></script>
<!--Popper js-->
<script src="js/popper.min.js"></script>
<!--Bootstrap js-->
<script src="js/bootstrap.min.js"></script>
<!--Magnific popup js-->
<script src="js/jquery.magnific-popup.min.js"></script>
<!--jquery easing js-->
<script src="js/jquery.easing.min.js"></script>
<!--jquery ytplayer js-->
<script src="js/jquery.mb.YTPlayer.min.js"></script>
<!--Isotope filter js-->
<script src="js/mixitup.min.js"></script>
<!--wow js-->
<script src="js/wow.min.js"></script>
<!--owl carousel js-->
<script src="js/owl.carousel.min.js"></script>
<!--countdown js-->
<script src="js/jquery.countdown.min.js"></script>
<!--custom js-->
<script src="js/scripts.js"></script>
</body>
</html>
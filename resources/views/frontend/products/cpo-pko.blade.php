@extends('frontend.layouts.master')
@section('title', 'CPO-PKO')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "CPO-PKO",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Products',
                            'url' => 'products'
                        ],

                        [
                            'item_title' => 'CPO-PKO'
                        ],
                    ]
            ]
        )
        
<section class="col_wrap ptb-100" style="min-height: 550px">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src=" {{ asset('images/cpo.jpg') }} " alt="cpo-pko" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <h5>Crude Palm Oil (CPO)</h5>
                <p>Crude palm oil (CPO) is a thick, cloudy orange oil product that is extracted from the mechanical squeezing of fresh palm fruits. At Univanich, we strictly control all steps of production leading to the extraction of CPO, meeting international standards. CPO is a popular raw material in many ongoing industries, which can be divided into 2 different categories: consumer products and energy products. In this respect, Univanich produces and distributes CPO to both domestic and international crude palm oil refineries for use in the food industry and many other industries such as in the production of biscuits, creamer, ice cream, cosmetics, oleochemicals such as fatty acid and methyl ester production, as well as biodiesel.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>On the consumer products side, Palm oil is a massively popular oil attributed to lower prices due to higher yields as well as ideal properties for cooking. Palm oil is very suitable for frying due to its naturally high smoke point as well as neutral flavours. In addition, foods fried in palm oil remain crispy for a longer time than other vegetable oils. As such, we can see that palm oil is utilized in various food products such as biscuits, instant noodles, or food spreads. Furthermore, palm oil can be refined to produce everyday hygiene products such as soaps, shampoos, cleaning products, or cosmetics.</p>
                <p>In the renewable energy sector, palm oil is a crucial ingredient for biodiesel. Biodiesel is produced by refining palm oil with alcohol along with diesel from petroleum. Biodiesels are considered to be clean-burning and more environmentally-friendly than conventional diesel due to its absence of sulfur, which reduces greenhouse gas emissions drastically.</p>
                <p>The demand for palm oil in both the renewable energy sector and consumer goods sector is increasing over time. In the past years, the government has promoted the production of biodiesel with higher ratios, resulting in a higher proportion of palm oil demand and usage in the future. Furthermore, population growth around the world along with the ubiquity of palm oil in many products has led to its increasing demand in the consumer goods sector.</p>
            </div><!-- /.col-md-6 -->

        </div><!-- /.row --><br></br>
        <div class="row">
            <div class="col-md-6">
                <h5>Palm Kernel Oil (PKO)</h5>
                <p>In the production of CPO from palm bunches, one key byproduct is the palm kernel, the core of the palm fruit. Palm kernels are known to be extremely hard, with a milky-white interior. Its main use is to be squeezed, extracting the oil, known as Palm Kernel Oil (PKO). Meanwhile, the solid byproducts are known as Palm Kernel Cake, which is used in the process of animal feeds.</p>
                <p>PKO has desirable properties very similar to the popular coconut oil, with a nuttier aroma and taste. Light-yellow in colour but white and odourless when refined, PKO is very popular in the food industry as a more premium alternative to CPO. Furthermore, they are also used as raw material for products such as cosmetics, soaps, or health supplements such as vitamin A and vitamin E.</p>
            </div>
            <div class="col-md-6">
                <img src=" {{ asset('images/pko.jpg') }} " alt="cpo-pko" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap -->

@endsection
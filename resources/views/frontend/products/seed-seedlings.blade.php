@extends('frontend.layouts.master')
@section('title', 'Seed Sales')
@section('content')
<section class="seed_sales_wrap ptb-100 gradient-overlay">
    <div class="container">

        <div class="row justify-content-center pb-100">
            <div class="col-md-8 col-lg-7">
                <div class="page-header-content text-white text-center pt-sm-5 pt-md-5 pt-lg-0">
                    <h1 class="text-white mb-0">Seed & Seedlings</h1>
                    <div class="custom-breadcrumb">
                        <ol class="breadcrumb d-inline-block bg-transparent list-inline py-0">
                            <li class="list-inline-item breadcrumb-item">
                                <a href=" {{ route( 'home' ) }} ">Home</a>
                            </li>
                            <li class="list-inline-item breadcrumb-item active">Seed & Seedlings</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="seed_sales_content column_three">
                    <a href="/products/seeds" class="portfolio-wrapper">
<!--<img src="http://yourfancam.com/images/i-008.png">-->
                        <h1><i class="fa fa-seedling"></i></h1>

                        <h4>Seeds</h4>

                        <hr>

                        <a href="/products/seeds"><span>More Info</span> <i class="fa fa-arrow-right"></i></a>
                    </a>
                </div>
                <div class="seed_sales_content column_three">
                    <a href="/products/seedlings" class="portfolio-wrapper">

                        <h1><i class="fa fa-tree"></i></h1>

                        <h4>Seedlings</h4>

                        <hr>

                        <a href="/products/seedlings"><span>More Info</span> <i class="fa fa-arrow-right"></i></a>
                    </a>
                </div>                
            </div>
        </div>
    </div>
</section>

@endsection

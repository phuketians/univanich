@extends('frontend.layouts.master')
@section('title', 'Seeds')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Seeds",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Products',
                            'url' => 'seed-seedlings'
                        ],

                        [
                            'item_title' => 'Seeds'
                        ],
                    ]
            ]
        )

<section class="col_wrap ptb-100" style="min-height: 630px">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src=" {{ asset('images/products/seeds03.jpg') }} " alt="Biogas" class="img-fluid rounded shadow-sm"><br>
                <img src=" {{ asset('images/products/seeds01.jpg') }} " alt="Biogas" class="img-fluid rounded shadow-sm"><br>
                <img src=" {{ asset('images/products/seeds02.jpg') }} " alt="Biogas" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <h5>Seeds</h5><br>
                <p>The Univanich Collection of Deli dura (D) and Yangambi pisifera (P) has lineages which can be traced back through many generations of oil palm selection. The original selection has undergone further selection to create the Univanich DXP breeding population. During this selection process, additional breeding material was imported through the Unilever-Harrison Crossfield Combined Breeding programme (CBP). This CBP was derived from breeding programmes in Binga/Yangambi in Congo, Lobe in Cameroon and Dami in Papua New Guinea. Through this exchange, Univanich obtained the famous Yangambi pisifera which has undergone further selection through progeny testing in Univanich trials.</p>
                <p>Our breeding program was established by Fr RHV Corley, Head of Research for the Unilever Plantations. Univanich has continued to engage International consultants to advise on Breeding Selection and to ensure the highest standards of Seed production are maintained. Rigorous field testing and bunch analysis ensure only the highest yielding hybrid crosses are accepted into the Univanich Seed production programme.</p>
                <p>Univanich customers can be assured by ISO 9001:2008 Certification and regular annual auditing of production processes necessary to maintain this high-quality standard.
                    Recent audits continue to demonstrate that Univanich DxP 
                    crosses are 100% pure Tenera hybrids.</p>
                <p>The Oil Palm Industry in Thailand is relatively free of major diseases. Lethal diseases such as Vascular Wilt, Fatal Yellowing or Lethal Bud Rot that can infect oil palm in parts of Africa and South and Central America, do not occur in Thailand. A strict protocol is followed for each export consignment of Univanich Seed, issued by the Plant Quarantine Service of Thailand’s Ministry of Agriculture.</p>
                <p>The Univanich breeding programs main aim is to produce high yielding DxP planting materials which are especially tolerant of the seasonal droughts of subtropical environments. At Univanich, we have two available seed products: the Univanich Seed, and the Elite Univanich Clonal Seed.:</p>

                <p style="margin-bottom: 0">All our seeds have two key features</p>
                <ul class="ml-5">
                    <li>1. High Yield</li>
                    <li>2. Drought-tolerance</li>
                </ul>
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div>
</section>
<section class="col_wrap ptb-10" style="min-height: 360px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Tissue Culture and Univanich Elite Clonal Seed:</h5>
                <p>Univanich developed Thailand’s first Tissue Culture Laboratory to produce oil palm clones. Elite palms have been identified in the company’s breeding programme and cloned from tissue samples to support the next generation of high yielding planting material, the clonal seed generation.</p>
                <p>The Clonal seed has been developed through selecting our very best mother palms and cloning them. These mother palms are then planted to establish a seed garden, from which we can then produce large quantities of seed. Pollination is done using pollen from our best pisifera palms. This produces our clonal seed, the next generation of elite seed.</p>
                <p style="margin-bottom: 0">Benefits of Clonal Seed</p>
                <ul class="ml-5">
                    <li>• Optimisation of Operations with uniformity of seedlings and improved culling procedures.</li>
                    <li>• Reduced phenotypic variation</li>
                    <li>• Improved harvesting efficiency with homogenous palm height</li>
                    <li>• Improve FFB yield and Palm Product Yield with this refined selection of our very best breeding material.</li> 

                </ul>
            </div>
        </div>
    </div><!-- /.container -->
</section><!-- /.col_wrap -->

@endsection
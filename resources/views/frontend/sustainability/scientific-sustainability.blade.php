@extends('frontend.layouts.master')
@section('title', 'Scientific Sustainability')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Scientific Sustainability",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Sustainability',                         
                        ]
                    ]
            ]
        )
<!--team single section start-->
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="/images/sustainability/scnt01.jpg" alt="scientific sustainability" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">Scientific Sustainability</h4>
                            </div>
                            <div class="text-content mt-20">
                                <p>In 1983, Univanich commenced a collaboration with the UK based Unilever Plantations
                                    and Plant Science Group. During the ensuing fifteen years the company’s Oil Palm
                                    Research Centre (OPRC) was established to carry out research into unique features of
                                    Thailand’s oil palm agronomy and in particular, into oil palm breeding for improved
                                    drought tolerance.</p>
                                <p>Progressively productize vertical alignments after sticky process improvements.
                                    Competently scale transparent methods of empowerment and visionary products.</p>
                                <p>Today, the Univanich Oil palm Research Centre (OPRC) is internationally recognized
                                    for pioneering oil palm research and advanced palm breeding. Our oil palm breeding
                                    programme was established in 1983, and has since been producing the highest-yielding
                                    hybrid seeds for local and overseas oil palm growers. The company’s oil palm
                                    nurseries supply annually more than 1.5 million high yielding seedlings to
                                    Thailand’s smallholder growers, and our drought-tolerant hybrid seeds are now
                                    exported to oil palm growers in more than fifteen countries. In 2006, the company
                                    established Thailand’s first laboratory for oil palm tissue culture with an
                                    objective to clone the elite parent palms from the Univanich breeding programme.
                                    This long-term research is now coming to fruition with Thailand’s first plantings of
                                    high-yielding clones in 2010 and in 2016 the company produced Thailand’s first
                                    semi-clonal oil palm seeds. This latest advance in science should ensure the
                                    competitiveness of Thailand’s oil palm industry into future generations.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="team-single-section ptb-50" style="min-height: 500px">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="text-content mt-20">
                            <p>At Univanich, we are in constant search for the best genomes possible for our oil palms.
                                In our past joint venture with Unilever, we have acquired complete samples of genomes
                                from various regions around the world such as Malaysia, Congo, Cameroon, and Papua New
                                Guinea. Utilizing these genomes, our research objectives have been to create the best
                                oil palm breed possible that is suitable for Thailand’s climate.</p>
                            <p>With over 30 years of experience in research of high-yielding oil palms, we have produced
                                over 2,000 hybrid crosses and dedicated over 1,000 hectares of estate area for our
                                research, many of which are joint projects with internationally-acclaimed researchers.
                                Univanich also undertakes strong research in agronomic techniques which optimizes our
                                yields, economy, and sustainability. </p>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-single-img">
                                <img src="/images/sustainability/scnt02.jpg" alt="scientific sustainability" class="img-fluid rounded shadow-sm" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="about-us-section ptb-100 gray-light-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="service-details-wrap">
                            <p>Some research papers that we have published include: </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="about-us-section ptb-10 gray-light-bg" style="min-height: 500px">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="service-details-wrap">
                            <ul>
                                <li><strong>1. Irrigation of oil palms in Southern Thailand</strong></li>
                                <ul class="list-unstyled tech-feature-list mb-3">
                                    <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>Palat
                                        Tittinutchanon, B G Smith and R H V Corley </li>
                                    <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>ISP
                                        International Planters Conference, May 2000 </li>
                                </ul>
                                <li><strong>2. Replanting oil palms in Thailand : Underplanting with Various Thinning
                                        and Pruning Techniques </strong></li>
                                <ul class="list-unstyled tech-feature-list mb-3">
                                    <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>J H Clendon
                                        and Palat Tittinutchanon </li>
                                    <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>Incorporated
                                        Society of Planters : National Seminar, 4 – 15 June 2004 </li>
                                </ul>
                                <li><strong>3. The Univanich oil palm Breeding Programme and Progeny Trial Results from
                                        Thailand </strong></li>
                                <ul class="list-unstyled tech-feature-list mb-3">
                                    <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>V. Rao,
                                        Palat Tittinutchanon, Chayawat Nakharin and R H V Corley </li>
                                    <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>The Planter.
                                        Vol. 84. No. 989, August 2008 </li>
                                </ul>
                            </ul>
                        </div>
                    </div>
                <div class="col-md-12 col-sm-12 col-lg-6">
                    <div class="service-details-wrap">
                        <ul>
                            <li><strong>4. A Review of Three CDM Biogas Projects Based on Palm Oil Mill Effluent, in
                                    Southern Thailand </strong></li>
                            <ul class="list-unstyled tech-feature-list mb-3">
                                <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>Sompol
                                    Tantitham, Phiphit Khlaisombat, J H Clendon, M Campbell-Board and B McIntosh </li>
                                <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>PIPOC
                                    International Palm Oil Conference. 9 – 12 November 2009 </li>
                            </ul>
                            <li><strong>5. Maximising oil palm Yield by High Density Planting and Thinning </strong>
                            </li>
                            <ul class="list-unstyled tech-feature-list mb-3">
                                <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>Palat
                                    Tittinutchanon, Chayawat Nakharin and R H V Corley</li>
                                <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>The Planter, 88
                                    (1033) : 241 – 256 2012 </li>
                            </ul>
                            <li><strong>6. Maximising Lifetime Yield for Greater Economic Sustainability </strong></li>
                            <ul class="list-unstyled tech-feature-list mb-3">
                                <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>R H V Corley &
                                    Palat Tittinutchanon </li>
                                <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>PIPOC
                                    International Palm Oil Conference, Kuala Lumpur, 2013 </li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--team single section end-->

@endsection

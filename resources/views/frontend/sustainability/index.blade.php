@extends('frontend.layouts.master')
@section('title', 'Sustainability')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Sustainability",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Sustainability'
],
]
]
)

<!--body content wrap start-->
<div class="main">

    <!--about us section start-->
    <section class="about-us-section ptb-100">
        <div class="container">
            <div class="row justify-content-between align-items-top">
                <div class="col-md-12 col-lg-6">
                    <div class="video-promo-content mb-md-4 mb-lg-0">
                        <br>
                        <p class="lead">When the first oil palms were planted back in 1969 Thailand was regarded as an
                            unpromising
                            region for this new crop. Fruit yields were limited by a severe annual dry season and there
                            were few large areas of land available for commercial oil palm plantations. These
                            constraints
                            coupled with inadequate supporting infrastructure led to the view that Thailand’s oil palm
                            growers could never compete successfully with the much larger and more developed palm oil
                            industries in Malaysia and Indonesia.</p>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="video-promo-content mb-md-4 mb-lg-0">
                        <br>
                        <p class="lead">Univanich Palm Oil PCL has led the way in changing that negative perception into
                            a new
                            reality. Thailand is now the world’s third-largest palm oil producer with annual production
                            exceeding two million tonnes. In helping to promote this successful transformation the
                            company has emphasized what it perceives as the “Four Pillars of Sustainability.” </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--about us section end-->

    <section>
        <div class="container">
            <div class="row justify-content-between align-items-top">
                <div class="col-md-12 col-lg-12">
                    <h2><center>Four Pillars of Sustainability</center></h2>
                </div>
            </div>
        </div>
    </section>

    <!--4cards section start-->
    <section class="cardz-section">
        <div class="cardz-grid">
            <a class="cardz" href="/sustainability/social-sustainability">
                <div class="cardz__background"
                    style="background-image: url(http://univanich.siamedia.net/images/card01.jpg)"></div>
                <div class="cardz__content">
                    <h3 class="cardz__heading">Social Sustainability</h3>
                </div>
            </a>
            <a class="cardz" href="/sustainability/environmental-sustainability">
                <div class="cardz__background"
                    style="background-image: url(http://univanich.siamedia.net/images/card02.jpg)"></div>
                <div class="cardz__content">
                    <h3 class="cardz__heading">Environmental Sustainability</h3>
                </div>
            </a>
            <a class="cardz" href="/sustainability/economic-sustainability">
                <div class="cardz__background"
                    style="background-image: url(http://univanich.siamedia.net/images/card03.jpg)"></div>
                <div class="cardz__content">
                    <h3 class="cardz__heading">Economic Sustainability</h3>
                </div>
                </li>
                <a class="cardz" href="/sustainability/scientific-sustainability">
                    <div class="cardz__background"
                        style="background-image: url(http://univanich.siamedia.net/images/card04.jpg)"></div>
                    <div class="cardz__content">
                        <h3 class="cardz__heading">Scientific Sustainability</h3>
                    </div>
                </a>
                <div>
    </section>
    <!--4cards section end-->

</div>
<!--body content wrap end-->

@endsection
@extends('frontend.layouts.master')
@section('title', 'Economic Sustainability')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Economic Sustainability",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Sustainability',                         
                        ]
                    ]
            ]
        )
<!--about us section start-->
    <section class="team-single-section ptb-100">
        <div class="container">
            <div class="row justify-content-between align-items-top">
                <div class="col-md-12 col-lg-6">
                    <div class="video-promo-content mb-md-4 mb-lg-0">
                        <h5 class="mb-1">Development of Overseas markets</h5>
                        <p class="lead">Univanich also puts strong effort in the development of sustainable export markets. We operate a palm oil exporting facility at the deep-water port of Laemphong in Krabi Province, which ships 150,000 tonnes of Palm oil annually to overseas markets. Furthermore, Univanich has built port storage tanks in 2017-2018, which receives oil from Univanich factories and third-party suppliers. 
                            Currently, the Chean Vanich exporting facility is now Thailand’s best West Coast port for Palm oil exports. </p>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="team-single-img">
                        <img src="/images/sustainability/ecos03.jpg" alt="member" class="img-fluid rounded shadow-sm" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--about us section end-->

    <!--about us section start-->
    <section class="team-single-section ptb-100">
        <div class="container">
            <div class="row justify-content-between align-items-top">
                <div class="col-md-12 col-lg-6">
                    <div class="team-single-img">
                        <img src="/images/sustainability/ecos02.jpg" alt="economic sustainability" class="img-fluid rounded shadow-sm" />
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="team-single-img">
                        <img src="/images/sustainability/ecos04.jpg" alt="economic sustainability" class="img-fluid rounded shadow-sm" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--about us section end-->

@endsection

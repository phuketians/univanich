@extends('frontend.layouts.master')
@section('title', 'Projects')
@section('content')
<section class="our-portfolio-section ptb-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-heading text-center mb-5">
                    <h2>Project Highlights</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">                     
                <div class="portfolio-container" id="MixItUp">
                    <div class="mix portfolio-item branding" data-ref="mixitup-target">
                        <div class="portfolio-wrapper">
                            <a href="{{ route('project-details') }}">
                                <div class="content-overlay"></div>
                                <img class="img-fluid" src="img/portfolios/3.jpg" alt="portfolio"/>
                                <div class="content-details fadeIn-bottom text-white">
                                    <p class="text-white mb-1">Amata B.Grimm Power (Rayong) 5 Limited</p>
                                    <p class="text-white mb-1">Plant Type: Combined Cycle Co-generation</p>
                                    <p class="DetailsButton">Details</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="mix portfolio-item other animation" data-ref="mixitup-target">
                        <div class="portfolio-wrapper">
                            <a href="{{ route('project-details') }}">
                                <div class="content-overlay"></div>
                                <img class="img-fluid" src="img/portfolios/2.jpg" alt="portfolio"/>
                                <div class="content-details fadeIn-bottom text-white">
                                    <p class="text-white mb-1">Amata B.Grimm Power (Rayong) 5 Limited</p>
                                    <p class="text-white mb-1">Plant Type: Combined Cycle Co-generation</p>
                                    <p class="DetailsButton">Details</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="mix portfolio-item animation" data-ref="mixitup-target">
                        <div class="portfolio-wrapper">
                            <a href="{{ route('project-details') }}">
                                <div class="content-overlay"></div>
                                <img class="img-fluid" src="img/portfolios/4.jpg" alt="portfolio"/>
                                <div class="content-details fadeIn-bottom text-white">
                                    <p class="text-white mb-1">Amata B.Grimm Power (Rayong) 5 Limited</p>
                                    <p class="text-white mb-1">Plant Type: Combined Cycle Co-generation</p>
                                    <p class="DetailsButton">Details</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="mix portfolio-item branding" data-ref="mixitup-target">
                        <div class="portfolio-wrapper">
                            <a href="{{ route('project-details') }}">
                                <div class="content-overlay"></div>
                                <img class="img-fluid" src="img/portfolios/1.jpg" alt="portfolio"/>
                                <div class="content-details fadeIn-bottom text-white">
                                    <p class="text-white mb-1">Amata B.Grimm Power (Rayong) 5 Limited</p>
                                    <p class="text-white mb-1">Plant Type: Combined Cycle Co-generation</p>
                                    <p class="DetailsButton">Details</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="mix portfolio-item animation other" data-ref="mixitup-target">
                        <div class="portfolio-wrapper">
                            <a href="{{ route('project-details') }}">
                                <div class="content-overlay"></div>
                                <img class="img-fluid" src="img/portfolios/5.jpg" alt="portfolio"/>
                                <div class="content-details fadeIn-bottom text-white">
                                    <p class="text-white mb-1">Amata B.Grimm Power (Rayong) 5 Limited</p>
                                    <p class="text-white mb-1">Plant Type: Combined Cycle Co-generation</p>
                                    <p class="DetailsButton">Details</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="mix portfolio-item branding " data-ref="mixitup-target">
                        <div class="portfolio-wrapper">
                            <a href="{{ route('project-details') }}">
                                <div class="content-overlay"></div>
                                <img class="img-fluid" src="img/portfolios/6.jpg" alt="portfolio"/>
                                <div class="content-details fadeIn-bottom text-white">
                                    <p class="text-white mb-1">Amata B.Grimm Power (Rayong) 5 Limited</p>
                                    <p class="text-white mb-1">Plant Type: Combined Cycle Co-generation</p>
                                    <p class="DetailsButton">Details</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="gap"></div>
                    <div class="gap"></div>
                    <div class="gap"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
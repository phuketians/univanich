@extends('frontend.layouts.master')
@section('title', 'Apirag Vanich')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Apirag Vanich",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Board of Directors',
'url' => 'directors'
],

[
'item_title' => 'Apirag Vanich'
],
]
]
)

<section class="team-single-section ptb-100">
    <div class="container">
        <div class="row align-items-top">
            <div class="col-md-12 col-sm-12 col-lg-5">
                <div class="team-single-img">
                    <img src="{{ asset('images/directors/dir-1.jpg') }} " alt="board of directors member"
                        class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-7">
                <div class="team-single-text">
                    <div class="team-name mb-4">
                        <h4 class="mb-1">Mr. Apirag Vanich</h4>
                        <span>Director</span>
                    </div>
                    <div class="text-content mt-20">
                    <p></p><strong>Age:</strong> 54</p>
                        <strong>Education:</strong>
                        <p>Master's degree in Business Administration from Indiana University and a degree in
                            Agricultural Economics from Purdue University in the United States</p>
                        <strong>Experience:</strong>
                        <p>Director of the Vanich group of companies in 1990 and Executive Chairman of the group since
                            1992. The Vanich group of companies is concentrated mainly in Thailand and in addition to
                            the Univanich palm oil business the group has interests in mining, transport, property
                            development and hospital services.</p>
                        <h5>IOD Program</h5>
                        <ul>
                            <li>• Director Accreditation Program (DAP) #37/2005</li>
                            <li>• Director Certification Program (DCP) #60/2005</li>
                            <li>• Finance for Non-Finance Director (FND) #19/2005</li>
                            <li>• The Role of Chairman (RCP) #13/2006</li>
                            <li>• Leader Program, Capital Market Academy</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--team single section end-->

@endsection
@extends('frontend.layouts.master')
@section('title', 'Prote Sosothikul')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Prote Sosothikul",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Board of Directors',
'url' => 'directors'
],

[
'item_title' => 'Prote Sosothikul'
],
]
]
)

<section class="team-single-section ptb-100">
    <div class="container">
        <div class="row align-items-top">
            <div class="col-md-12 col-sm-12 col-lg-5">
                <div class="team-single-img">
                    <img src="{{ asset('images/directors/dir-6.jpg') }} " alt="board of directors member"
                        class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-7">
                <div class="team-single-text">
                    <div class="team-name mb-4">
                        <h4 class="mb-1">Mr. Prote Sosothikul</h4>
                        <span>Director</span>
                    </div>
                    <div class="text-content mt-20">
                        <p></p><strong>Age:</strong> 51</p>
                        <strong>Education:</strong>
                        <p>Doctorate in Business Administration from the American University of Hawaii and Master of
                            Science degree from Boston University, USA.</p>
                        <strong>Experience:</strong>
                        <p>Currently Vice President of Business Development for Seacon Development Public Company
                            Limited, the developer and operator of the largest retail shopping complex in Thailand. Dr
                            Prote Sosothikul was appointed as a non-executive director on the Univanich Board in 2000.
                        </p>
                        <h5>IOD Program</h5>
                        <ul>
                            <li>• Director Certification Program (DCP) #40/2004</li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--team single section end-->

@endsection
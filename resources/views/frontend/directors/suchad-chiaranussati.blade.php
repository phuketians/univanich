@extends('frontend.layouts.master')
@section('title', 'Suchad Chiaranussati')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Suchad Chiaranussati",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Board of Directors',
'url' => 'directors'
],

[
'item_title' => 'Suchad Chiaranussati'
],
]
]
)

<section class="team-single-section ptb-100">
    <div class="container">
        <div class="row align-items-top">
            <div class="col-md-12 col-sm-12 col-lg-5">
                <div class="team-single-img">
                    <img src="{{ asset('images/directors/dir-8.jpg') }} " alt="board of directors member"
                        class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-7">
                <div class="team-single-text">
                    <div class="team-name mb-4">
                        <h4 class="mb-1">Mr. Suchad Chiaranussati</h4>
                        <span>Director</span>
                    </div>
                    <div class="text-content mt-20">
                        <p></p><strong>Age:</strong> 55</p>
                        <strong>Education:</strong>
                        <p>Master's degree in Management Science from Imperial College, University of London and an
                            honours degree in economics and political science from the London School of Economics.</p>
                        <strong>Experience:</strong>
                        <p>Between 1991 and 1998, Mr Chiaranussati was a vice-president of Investment Banking at JP
                            Morgan's Proprietary Investment and Investment Banking Unit. In late 1998 he was appointed
                            as an advisor to the Bank of Thailand on matters relating to corporate sector debt
                            restructuring, and the Financial Institutions Development Fund. Mr Chiaranussati was the
                            Managing Principal of Westbrook Partners till early 2005. Today, Mr Chiaranussati is the
                            Managing Principal of RECAP Partners and is responsible for RECAP Partners' real estate
                            investment operations for Asia. Mr Chiaranussati was appointed to the Univanich Board as an
                            Independent Director on 12 February, 2000.</p>
                            <h5>IOD Program</h5>
                        <ul>
                            <li>• Director Accreditation Program (DAP) #37/2005</li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--team single section end-->

@endsection
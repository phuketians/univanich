@extends('frontend.layouts.master')
@section('title', 'Pramoad Phornprapha')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Pramoad Phornprapha",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Board of Directors',
'url' => 'directors'
],

[
'item_title' => 'Pramoad Phornprapha'
],
]
]
)


<section class="team-single-section ptb-100">
    <div class="container">
        <div class="row align-items-top">
            <div class="col-md-12 col-sm-12 col-lg-5">
                <div class="team-single-img">
                    <img src="{{ asset('images/directors/dir-10.jpg') }} " alt="management team member"
                        class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-7">
                <div class="team-single-text">
                    <div class="team-name mb-4">
                        <h4 class="mb-1">Mr. Pramoad Phornprapha</h4>
                        <span>Audit Committee / Independent Director</span>
                    </div>
                    <div class="text-content mt-20">
                        <p></p><strong>Age:</strong> 52</p>
                        <strong>Education:</strong>
                        <p>Mr. Pramoad Phornprapha has a MPA in Business and Government, Kennedy School of
                            Government, Harvard University (Honours), MBA in Marketing, Kellogg School of
                            Management, Northwestern University (Honours), and BS in Electrical Engineering,
                            Northwestern University (Honours)A</p>
                        <strong>Experience:</strong>
                        <p>Mr. Pramoad Phornprapha is the founding partner of consulting firm Claris Co.,Ltd.
                            and an ex-partner at The Boston Consulting Group (BCG). He has been advising leading
                            companies in a number of industries across different functional areas in many Asian
                            countries since 1995. His specific expertise is in strategic planning and corporate
                            restructuring. He accumulated his experience during his nine years at BCG where he
                            was the first South East Asian to be elected partner in 2001. He has been involved
                            on topics ranging from strategy, operational effectiveness, to organisational
                            improvement primarily in financial services, industrial goods, and consumer product
                            industries. His recent focus has been on growth strategies and restructuring of
                            large companies, particularly family owned conglomerates in Thailand. Mr. Pramoad
                            serves on the boards of a number of private and public companies including Sermsuk
                            Public Company Limited and Thai Summit Harness Public Company Limited.s.</p>
                        <p>Mr. Pramoad Phornprapha was appointed to the Univanich Board as an Audit
                            Committee/Independent Director on 30th September 2017</p>
                        <h5>IOD Program</h5>
                        <ul>
                            <li>• Directors Certification Program, DCP 55/2005,
                                Thai Institute of Directors (IOD)</li>
                            <li>• Leader Program, CMA10, Capital Market Academy</li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--team single section end-->

@endsection
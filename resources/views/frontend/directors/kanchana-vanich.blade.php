@extends('frontend.layouts.master')
@section('title', 'Kanchana Vanich')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Kanchana Vanich",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Board of Directors',
'url' => 'directors'
],

[
'item_title' => 'Kanchana Vanich'
],
]
]
)

<section class="team-single-section ptb-100">
    <div class="container">
        <div class="row align-items-top">
            <div class="col-md-12 col-sm-12 col-lg-5">
                <div class="team-single-img">
                    <img src="{{ asset('images/directors/dir-3.jpg') }} " alt="board of directors member"
                        class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-7">
                <div class="team-single-text">
                    <div class="team-name mb-4">
                        <h4 class="mb-1">Mrs. Kanchana Vanich</h4>
                        <span>Director</span>
                    </div>
                    <div class="text-content mt-20">
                        <p></p><strong>Age:</strong> 61</p>
                        <strong>Education:</strong>
                        <p>A degree in finance from the University of Wisconsin, in the United States.</p>
                        <strong>Experience:</strong>
                        <p>Ms. Kanchana Vanich has been a Director in the Univanich business since 1983 and is also a
                            director of other companies in the Vanich group, including Aikchol Hospital Public Company
                            Limited.</p>
                        <h5>IOD Program</h5>
                        <ul>
                            <li>• Director Accreditation Program (DAP) #36/2005</li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--team single section end-->

@endsection
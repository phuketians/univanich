@extends('frontend.layouts.master')
@section('title', 'Phortchana Manoch')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Phortchana Manoch",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Board of Directors',
'url' => 'directors'
],

[
'item_title' => 'Phortchana Manoch'
],
]
]
)

<section class="team-single-section ptb-100">
    <div class="container">
        <div class="row align-items-top">
            <div class="col-md-12 col-sm-12 col-lg-5">
                <div class="team-single-img">
                    <img src="{{ asset('images/directors/dir-2.jpg') }} " alt="board of directors member"
                        class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-7">
                <div class="team-single-text">
                    <div class="team-name mb-4">
                        <h4 class="mb-1">Mrs. Phortchana Manoch</h4>
                        <span>Director</span>
                    </div>
                    <div class="text-content mt-20">
                        <p></p><strong>Age:</strong> 64</p>
                        <strong>Education:</strong>
                        <p>Bachelor of Laws from Dhurakij Pundit University. Bachelor of Science in Business
                            Administration from the University of Wisconsin, Green Bay, the United States. Master's
                            degree in Hospital Administration from the University of Wisconsin, Green Bay, the United
                            States.</p>
                        <strong>Experience:</strong>
                        <p>Currently Chief Executive of Aikchol Hospital Public Co., Ltd., a group of private hospitals
                            in Chonburi Province. Mrs Manoch has been a director of the Univanich business for more than
                            fifteen years and is also a director of other companies in Vanich group.</p>
                            <h5>IOD Program</h5>
                        <ul>
                            <li>• Director Certification Program (DCP) #25/2005</li>
                            <li>• Financial Statements for Directors (FSD) #13/2011</li>
                            <li>• Good Governance for Medical Executive Course - Class 1 from King Prajadhipok's Institute</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--team single section end-->

@endsection
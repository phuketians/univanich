<section class="hero-section top-background ptb-100 gradient-overlay">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-7">
                <div class="page-header-content text-white text-center pt-sm-5 pt-md-5 pt-lg-0">
                    <h1 class="text-white mb-0">{{ $page_title ??  'Univanish' }}</h1>
                    <div class="custom-breadcrumb">
                        @if( isset( $b_items ) && is_array( $b_items) )
                        <ol class="breadcrumb d-inline-block bg-transparent list-inline py-0">
                            @foreach( $b_items as $item ) 

                                @if( isset( $item['url'] ) )
                                    <li class="list-inline-item breadcrumb-item">
                                        <a href="{{ route( $item['url'] ) }}">{{ $item['item_title'] }}</a>
                                    </li>
                                @else
                                <li class="list-inline-item breadcrumb-item active">{{ $item['item_title'] }}</li>
                                @endif
                                
                            @endforeach
                        </ol>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
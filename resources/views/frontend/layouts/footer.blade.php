<!--footer section start-->
<footer class="footer-section">  
    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg py-3">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12">
                    <p class="copyright-text pb-0 mb-0"><center>Copyrights © 2021. All
                        rights reserved by Univanich Palm Oil Public Co. Ltd. | Designed by <a href="https://phuketians.com">Phuketians</center></p>                    
                </div>
                
            </div>
        </div>
    </div>
    <!--footer copyright end-->
</footer>
<!--footer section end-->
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--title-->
    <title>Univanich || @yield('title')</title>

    <!--favicon icon-->
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans:400,600&display=swap"
        rel="stylesheet">

    <link href="{{ asset('frontend/css/app.css') }}" rel="stylesheet">
</head>

<body>
    @include('frontend.layouts.header')

    <div class="main" style="height:94vh">

        @yield('content')

        @include('frontend.layouts.footer')

    </div>

    <script src="{{asset('frontend/js/app.js')}}"></script>

     <script>
        var premiumStockMarketWidgets = {
            pluginUrl: "{{asset('stock')}}",
            ajaxUrl: "{{asset('stock/ajax.php')}}",
         }
    </script>
    <script src="{{asset('stock/assets/dist/app.js')}}"></script>


</body>

</html>

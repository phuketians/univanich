<!--loader start-->
<div id="preloader">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!--loader end-->

<!--header section start-->
<header class="header">
    <!--start navbar-->
    <nav class="navbar navbar-expand-lg fixed-top white-bg">
        <div class="container position-relative">

            <div class="header-left logo">
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="{{asset('images/logo-white.png')}}" alt="logo" class="img-fluid" />
                </a>
            </div>

            <div class="header-right">

                <div class="main-title float-left">
                     <stock-market-widget type="card" template="basic2" assets="UVAN.BK" api="yf"></stock-market-widget>
                </div>


                <div class="navbar-header float-right">
                    <button data-target="#top-nav" data-toggle="collapse" type="button" class="navbar-toggle">
                        <i class="fa fa-bars"></i>
                    </button>

                </div>

            </div>           
        </div>

        <div class="MenuWrap">
            <div class="container">
                <ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('company-profile') }}">Company Profile</a>
                        <ul>
                            <li><a href="{{ route('chairman-report') }}">Message from Chairman</a></li>
                            <li><a href="{{ route('principles-code') }}">Code of Business Principles</a></li>
                            <li><a href="{{ route('company-profile') }}">Company Profile</a></li>
                            <li><a href="{{ route('corporate') }}">Corporate Governance</a></li>                            
                            <li><a href="{{ route('board-of-directors') }}">Board of Directors</a></li>
                            <li><a href="{{ route('management-team') }}">Management Team</a></li>
                            <li><a href="{{ route('timeline') }}">Timeline</a></li>
                            <li><a href="{{ route('awards') }}">Awards</a></li>                            
                        </ul>
                    </li>
                    <li><a href="#">Operations</a>
                        <ul>
                            <li><a href="{{ route('estates') }}">Estates</a></li>
                            <li><a href="{{ route('factories') }}">Factories</a></li>
                            <li><a href="{{ route('nurseries') }}">Nurseries</a></li>
                        </ul>

                        <a href="#">Products</a>

                        <ul>
                            <li><a href="{{ route('cpo-pko') }}">CPO & PKO</a></li>
                            <li><a href="{{ route('seed-seedlings') }}">Seeds & Seedlings</a></li>
                            <li><a href="{{ route('by-products') }}">Byproducts</a></li>
                        </ul>
                    </li>

                    <li><a href="{{ route('investor-relations') }}">Investor Relations</a>
                        <ul>
                            <li><a href="{{ route('financial-information') }}">Financial Information</a></li>
                            <li><a href="{{ route('shareholder') }}">Shareholder Information</a></li>
                            <li><a href="{{ route('publications') }}">Publications and Download</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('sustainability') }}">Sustainability</a>
                        <ul>
                            <li><a href="{{ route('social') }}">Social Sustainability</a></li>
                            <li><a href="{{ route('environmental') }}">Environmental Sustainability</a></li>                            
                            <li><a href="{{ route('economic') }}">Economic Sustainability</a></li>
                            <li><a href="{{ route('scientific') }}">Scientific Sustainability</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('media') }}">Media</a>
                        <ul>
                            <li><a href="{{ route('media') }}">News</a></li>    
                        </ul>

                        <a href="{{ route('careers') }}">Careers</a>
                        <ul>
                            <li><a href="{{ route('careers') }}">Careers Center</a></li>
                        </ul>

                        <a href="{{ route('contact') }}">Contact Us</a>

                        <ul>
                            <li><a href="{{ route('whistleblowing') }}">Whistleblowing</a></li>
                            <li><a href="{{ route('feedback-inquiries') }}">Feedback and Inquiries</a></li>
                        </ul>

                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!--header section end-->

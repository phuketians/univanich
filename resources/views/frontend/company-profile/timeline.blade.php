@extends('frontend.layouts.master')
@section('title', 'Timeline')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Timeline",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Company Profile',
                            'url' => 'company-profile'
                        ],

                        [
                            'item_title' => 'Timeline'
                        ],
                    ]
            ]
        )

<section class="col_wrap ptb-100">

    <div class="container">
        <div class="row">

            <div class="col-md-12 mt-4">
                <div class="main-timeline">
                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                                The first Oil Palm Plantation was established in Thailand, pioneered by Mr. Chean Vanich.
                            </p>
                            <span class="timeline-year">1969</span>
                        </div>
                    </div>
                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            The first Palm Oil Crushing Factory in Thailand was commissioned under Thai Oil Palm Industry and Estate Company Limited, Topi Crushing Factory (10 ton per hour) at Plai Phraya district, led by Mr. Chean Vanich.
                            </p>
                            <span class="timeline-year">1974</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Siam Oil Palm Crushing factory was commissioned at 10 ton per hour.
                            </p>
                            <span class="timeline-year">1980</span>
                        </div>
                    </div>


                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Mr. Chean Vanich’s plantations and crushing mills entered a 15-year
                    joint venture with Unilever PLC. Unilever held a 51% shareholding in Univanich, and the business was
                    managed through Unilever’s International Plantations and Plant Science Group. This enabled the
                    business to build capacity through modern agriculture technologies, oil palm research, environmental
                    best practices and professional staff training. 
                            </p>
                            <span class="timeline-year">1983</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Replanting commenced with improved genetics from Univanich Research Centre
                    and Combined Breeding Program from leading overseas programs.Offering improved yield and
                    performance.
                            </p>
                            <span class="timeline-year">1992</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Univanich Palm Oil Public Company Limited was established on 26th December
                    1995 as an amalgamation of three of Thailand’s pioneer palm oil companies owned by Mr. Chean Vanich.
                    The three companies were Thai Oil Palm Industry and Estate Company Limited, Siam Palm Oil and
                    Refinery Industry Company Limited, and Hup Huat Palm Oil Industry Company Limited.
                            </p>
                            <span class="timeline-year">1995</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Unilever’s 51% stake sold to Thai partners, making Univanich a 100% Thai company.
                            </p>
                            <span class="timeline-year">1998</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Topi Palm Oil Crushing Factory expanded to 60 ton per hour and a Kernel
                    Crushing Facility was added with a capacity of 75 ton per day.
                            </p>
                            <span class="timeline-year">2001</span>
                        </div>
                    </div>


                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            The Company was listed on the Stock Exchange of Thailand on 25th November
                    under the abbreviation UVAN.
                            </p>
                            <span class="timeline-year">2003</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Lamthap Palm Oil Crushing Factory was commissioned at 45 tph. This was to be Univanich’s 3rd factory.
                            </p>
                            <span class="timeline-year">2004</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Lamthap Kernel Crushing plant commissioned with a capacity of 75 ton per day.
                            </p>
                            <span class="timeline-year">2005</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Univanich commissioned Thailand’s first Tissue Culture Laboratory for Oil
                    palms, and commenced research into the production of clonal seeds from elite oil palm clones.
                            </p>
                            <span class="timeline-year">2006</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                                Completion of Biogas Reactor and commissioning of 952 Kw Gas engine at Siam Palm Oil Factory.
                            </p>   
                            <p class="description">
                                Completion of Biogas Reactor and commissioning of 2 gas engine units (952KW each) at Lamthap Palm Oil Factory.</p>
                            </p>
                            <span class="timeline-year">2008</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Completion of Biogas Reactor and commissioning of 3 gas engine units (952KW each) at Topi Palm Oil Factory.
                            </p>
                            <span class="timeline-year">2009</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Univanich expanded its business to the Philippines by establishing a 51%
                    owned joint venture company named Univanich Carmen Palm Oil Corporation (UCPOC). UCPOC has
                    constructed a new palm oil crushing mill in North Cotabato province of Mindanao. The new palm oil
                    mill started processing in July 2014, at a capacity of 30 ton per hour.
                            </p>
                            <p class="description">
                            Univanich became Thailand’s first RSPO-certified Palm Oil exporter, with first export going to premium markets in Europe.
                            </p>
                            <span class="timeline-year">2013</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            The first commercial plantings of Univanich clonal seed from elite parental clones.
                            </p>
                            <span class="timeline-year">2016</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Acquisition of Chok Vallapa Palm Oil Crushing Factory with capacity of 60 ton per hour.
                            </p>

                            <p class="description">
                            Pabon Palm Oil Crushing Factory was commissioned at 30 ton per hour.
                            </p>
                            <span class="timeline-year">2017</span>
                        </div>
                    </div>

                    <div class="timeline">
                        <div class="timeline-content">
                            <p class="description">
                            Expansion of Topi and Lamthap Kernel Crushing facilities, increasing
                    Univanich capacity to 345 ton per day of Kernel Crushing.
                            </p>
                            <p class="description">
                            Completion of Biogas Reactor at Pabon Palm Oil Crushing Factory.
                            </p>
                            <p class="description">
                            Addition of 2MW Gas engine to Siam Palm Oil Factory.
                            </p>
                            <span class="timeline-year">2019</span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
        <div class="action-btns mt-4" align="center">
                            <a href="/images/univanich-milestones-development.pdf" target="_blank" class="btn primary-solid-btn mr-2">Univanich Milestones of Development</a>
                        </div>
    </div><!-- /.container -->
</section><!-- /.col_wrap -->

@endsection

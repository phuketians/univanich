@extends('frontend.layouts.master')
@section('title', 'Investor Relations')
@section('content')

    
<!--hero section start-->
    <section class="hero-section ptb-100 gradient-overlay"
             style="background: url('/images/invr-bg01.jpg')no-repeat center center / cover">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-7">
                    <div class="page-header-content text-white text-center pt-sm-5 pt-md-5 pt-lg-0">
                        <h1 class="text-white mb-0"></h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--hero section end-->


<!--our work or portfolio section start-->
<section class="our-portfolio-section ptb-100" style="min-height: 600px">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-heading text-center mb-5">
                    <h2></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portfolio-container" id="MixItUp">
                    <div class="mix portfolio-item branding" data-ref="mixitup-target">
                        <div class="portfolio-wrapper">
                            <a href="{{ route('financial-information') }}">
                                <div class="content-overlay"></div>

                                <img class="img-fluid" src=" {{ asset( 'images/portfolios/3.jpg' ) }} " alt="portfolio" />

                                <div class="content-details fadeIn-bottom text-white">

                                    <h5 class="text-white mb-1">Financial Information</h5>

                                    <p class="DetailsButton">Details</p>

                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="mix portfolio-item other animation" data-ref="mixitup-target">
                        <div class="portfolio-wrapper">
                            <a href="{{ route('shareholder') }}">
                                <div class="content-overlay"></div>
                                <img class="img-fluid" src=" {{ asset('images/portfolios/2.jpg') }} " alt="portfolio" />
                                <div class="content-details fadeIn-bottom text-white">
                                    <h5 class="text-white mb-1"> Shareholder Information</h5>

                                    <p class="DetailsButton">Details</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="mix portfolio-item animation" data-ref="mixitup-target">
                        <div class="portfolio-wrapper">
                            <a href="{{ route('publications') }}">
                                <div class="content-overlay"></div>
                                <img class="img-fluid" src="{{ asset('images/portfolios/4.jpg') }}" alt="portfolio" />
                                <div class="content-details fadeIn-bottom text-white">
                                    <h5 class="text-white mb-1">Publications and Download</h5>
                                    <p class="DetailsButton">Details</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

@endsection
@extends('frontend.layouts.master')
@section('title', 'Financial Information')
@section('content')


@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Financial Information",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Investor Relations',
                            'url' => 'investor-relations'
                        ],

                        [
                            'item_title' => 'Financial Information'
                        ],
                    ]
            ]
        )


<section class="col_wrap ptb-100" style="min-height: 630px">
    <div class="container">
        <div class="row">  
            <div class="col-md-12">

                <stock-market-widget type="table-quotes" template="basic2" color="#5679FF" assets="UVAN.BK" fields="logo_name,price,change_abs,change_pct,market_cap" display_header="true" display_chart="true" display_currency_symbol="true" pagination="true" search="false" rows_per_page="5" sort_field="logo_name" sort_direction="asc" alignment="left" api="yf"></stock-market-widget>
            </div>
        </div>
                <div class="row justify-content-around align-items-top ptb-100">
                    <h2><center>Downloads of Financial Statements</center></h2>
                    <div class="col-md-12 col-lg-6 ptb-10">
                        <table class="table responsive table-hover mb-4">
                            <tbody>
                                <tr>
                                    <td>Year 2020</td>
                                    <td><a href="/finance/20201110-uvan-fs-3q2020-en.pdf"><span class="pl-4">- Full
                                                Financial Statement Q3 2020</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/20200814-uvan-fs-2q2020-en.pdf"><span class="pl-4">- Full
                                                Financial Statement Q2 2020</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/20200515-uvan-fs-1q2020-en.pdf"><span class="pl-4">- Full
                                                Financial Statement Q1 2020</span></a></td>
                                </tr>
                                    <tr>
                                        <th>Year 2019</th>
                                        <th><a href="/finance/20200228-uvan-fs-fy2019-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Yearly 2019</span></a></th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20191114-uvan-fs-3q2019-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Q3 2019</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20190808-uvan-fs-2q2019-en-01.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Q2 2019</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20190508-uvan-fs-1q2019-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Q1 2019</span></a></td>
                                    </tr>
                                    <tr>
                                        <td>Year 2018</td>
                                        <td><a href="/finance/20190222-uvan-fs-fy2018-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Yearly 2018</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20181109-uvan-fs-3q2018-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Q3 2018</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20180814-uvan-fs-2q2018-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Q2 2018</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20180511-uvan-fs-1q2018-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Q1 2018</span></a></td>
                                    </tr>
                                    <tr>
                                        <td>Year 2017</td>
                                        <td><a href="/finance/20180223-uvan-fs-fy2017-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Yearly 2017</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20171110-uvan-fs-3q2017-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Q3 2017</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20170811-uvan-fs-2q2017-en.pdf" target="_blank"><span
                                                    class="pl-4">- Full Financial Statement Q2 2017</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20170512-uvan-fs-1q2017-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Q1 2017</span></a></td>
                                    </tr>
                                    <tr>
                                        <td>Year 2016</td>
                                        <td><a href="/finance/20170224-uvan-fs-fy2016-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Yearly 2016</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20161114-uvan-fs-3q2016-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Q3 2016</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20160805-uvan-fs-2q2016-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Q2 2016</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20160513-uvan-fs-1q2016-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Q1 2016</span></a></td>
                                    </tr>
                                    <tr>
                                        <td>Year 2015</td>
                                        <td><a href="/finance/20160226-uvan-fs-fy2015-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Yearly 2015</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20151113-uvan-fs-3q2015-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Q3 2015</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20150814-uvan-fs-2q2015-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Q2 2015</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20150515-uvan-fs-1q2015-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Q1 2015</span></a></td>
                                    </tr>
                                    <tr>
                                        <td>Year 2014</td>
                                        <td><a href="/finance/20150227-uvan-fsfy2014-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Yearly 2014</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20141114-uvan-fs3q2014-en.zip"><span class="pl-4">- Full
                                                    Financial Statement Q3 2014</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20140814-uvan-fs2q2014-en-01.zip"><span class="pl-4">-
                                                    Full Financial Statement Q2 2014</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20140509-UVAN-FS1Q2014-EN.zip"><span class="pl-4">- Full
                                                    Financial Statement Q1 2014</span></a></td>
                                    </tr>
                                    <tr>
                                        <td>Year 2013</td>
                                        <td><a href="/finance/20140228-UVAN-FSFY2013-EN.zip"><span class="pl-4">- Full
                                                    Financial Statement Yearly 2013</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20131112-UVAN-FS3Q2013-EN-01.zip"><span class="pl-4">- Full
                                                    Financial Statement Q3 2013</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20130813-UVAN-FS2Q2013-EN.zip"><span class="pl-4">- Full
                                                    Financial Statement Q2 2013</span></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="/finance/20130515-UVAN-FS1Q2013-EN.zip"><span class="pl-4">- Full
                                                    Financial Statement Q1 2013</span></a></td>
                                    </tr>
                                </thead>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 col-lg-6 ptb-10">
                        <table class="table responsive table-hover mb-4">
                            <thead>
                            <tbody>
                                <tr>
                                    <td>Year 2012</td>
                                    <td><a href="/finance/20130301-UVAN-FSFY2012-EN.zip"><span class="pl-4">- Full
                                                Financial Statement Yearly 2012</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/20121109-UVAN-FS3Q2012-EN.zip"><span class="pl-4">- Full
                                                Financial Statement Q3 2012</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/20120810-UVAN-FS2Q2012-EN.zip"><span class="pl-4">- Full
                                                Financial Statement Q2 2012</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/20120511-UVAN-FS1Q2012-EN.zip"><span class="pl-4">- Full
                                                Financial Statement Q1 2012</span></a></td>
                                </tr>
                                <tr>
                                    <td>Year 2011</td>
                                    <td><a href="/finance/20120224-UVAN-FSFY2011-EN.zip"><span class="pl-4">- Full
                                                Financial Statement Yearly 2011</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/20111111-UVAN-FS3Q2011-EN.zip"><span class="pl-4">- Full
                                                Financial Statement Q3 2011</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/q211_en.zip"><span class="pl-4">- Full Financial Statement Q2
                                                2011</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/q111_en.zip"><span class="pl-4">- Full Financial Statement Q1
                                                2011</span></a></td>
                                </tr>
                                <tr>
                                    <td>Year 2010</td>
                                    <td><a href="/finance/fy10_en.zip"><span class="pl-4">- Full Financial Statement
                                                Yearly 2010</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/q310_en.zip"><span class="pl-4">- Full Financial Statement Q3
                                                2010</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/q210_en.zip"><span class="pl-4">- Full Financial Statement Q2
                                                2010</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/q110_en.zip"><span class="pl-4">- Full Financial Statement Q1
                                                2010</span></a></td>
                                </tr>
                                <tr>
                                    <td>Year 2009</td>
                                    <td><a href="/finance/fy09_en.zip"><span class="pl-4">- Full Financial Statement
                                                Yearly 2009</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/q309_en.zip"><span class="pl-4">- Full Financial Statement Q3
                                                2009</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/q209_en.zip"><span class="pl-4">- Full Financial Statement Q2
                                                2009</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/uvan_2009q1_en.zip"><span class="pl-4">- Full Financial
                                                Statement Q1 2009</span></a></td>
                                </tr>
                                <tr>
                                    <td>Year 2008</td>
                                    <td><a href="/finance/Q408_en.zip"><span class="pl-4">- Full Financial Statement
                                                Yearly 2008</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/Q308_en.zip"><span class="pl-4">- Full Financial Statement Q3
                                                2008</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/Q208_en.zip"><span class="pl-4">- Full Financial Statement Q2
                                                2008</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/Q108_en.zip"><span class="pl-4">- Full Financial Statement Q1
                                                2008</span></a></td>
                                </tr>
                                <tr>
                                    <td>Year 2007</td>
                                    <td><a href="/finance/Q407_en.zip"><span class="pl-4">- Full Financial Statement
                                                Yearly 2007</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/Q307_en.zip"><span class="pl-4">- Full Financial Statement Q3
                                                2007</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/Q207_en.zip"><span class="pl-4">- Full Financial Statement Q2
                                                2007</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/Q107_en.zip"><span class="pl-4">- Full Financial Statement Q1
                                                2007</span></a></td>
                                </tr>
                                <tr>
                                    <td>Year 2006</td>
                                    <td><a href="/finance/Q406_en.zip"><span class="pl-4">- Full Financial Statement
                                                Yearly 2006</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/Q306_en.zip"><span class="pl-4">- Full Financial Statement Q3
                                                2006</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/Q206_en.zip"><span class="pl-4">- Full Financial Statement Q2
                                                2006</span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href="/finance/Q106_en.zip"><span class="pl-4">- Full Financial Statement Q1
                                                2006</span></a></td>
                                </tr>
                                </thead>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

@endsection
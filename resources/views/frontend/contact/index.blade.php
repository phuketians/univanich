@extends('frontend.layouts.master')
@section('title', 'Contact Us')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Contact Us",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Contact Us',
                            'url' => 'contact'
                        ],

                        [
                            'item_title' => 'Contact'
                        ],
                    ]
            ]
        )
<!--contact us promo start-->
        <section class="contact-us-promo pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card single-promo-card single-promo-hover text-center shadow-sm">
                            <div class="card-body py-5">
                                <div class="pb-2">
                                    <span class="ti-mobile icon-sm color-secondary"></span>
                                </div>
                                <div>
                                    <h5 class="mb-0">Call Us</h5>
                                    <a href="tel:+6675681116" class="link-color">+66 (0) 75681116</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card single-promo-card single-promo-hover text-center shadow-sm">
                            <div class="card-body py-5">
                                <div class="pb-2">
                                    <span class="ti-location-pin icon-sm color-secondary"></span>
                                </div>
                                <div>
                                    <h5 class="mb-0">Visit Us</h5>
                                    <a href="https://bit.ly/3n9ZBub" target="_blank" class="link-color">Krabi, Thailand</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card single-promo-card single-promo-hover text-center shadow-sm">
                            <div class="card-body py-5">
                                <div class="pb-2">
                                    <span class="ti-email icon-sm color-secondary"></span>
                                </div>
                                <div>
                                    <h5 class="mb-0">Mail Us</h5>
                                    <a href="mailto:info@univanich.com" class="link-color">info@univanich.com</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--contact us promo end-->

        <!--contact us section start-->
        <section class="contact-us-section ptb-100">
            <div class="container contact">
                <div class="col-12 pb-3 message-box d-none">
                    <div class="alert alert-danger"></div>
                </div>
                <div class="row justify-content-around">
                    <div class="col-md-6">
                        <div class="contact-us-form gray-light-bg rounded p-5">
                            <h4>Ready to get started?</h4>
                            <form action="" method="POST" id="contactForm" class="contact-us-form">
                                <div class="form-row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Enter name"
                                                required="required">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email"
                                                placeholder="Enter email" required="required">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea name="message" id="message" class="form-control" rows="7"
                                                cols="25" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 mt-3">
                                        <button type="submit" class="btn secondary-solid-btn" id="btnContactUs">
                                            Send Message
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="contact-us-content">
                            <h2>Univanich Palm Oil Public Company Ltd.</h2>
                            <p class="lead">Univanich Palm Oil PCL has been a pioneer of the oil palm industry in
                                Thailand since the company’s first plantations were established back in 1969. Today,
                                Univanich is Thailand’s leading producer of crude palm oils and high quality oil palm
                                seeds.</p>

                            <a href="https://bit.ly/3n9ZBub" target="_blank" class="btn btn primary-solid-btn mr-2">Get Directions <span
                                    class="ti-arrow-right pl-2"></span></a>

                            <hr class="my-5">

                            <h5>Our Headquarters</h5>
                            <address>
                                258 Aoluk-Laemsak Road, P.O.BOX 8-9,<br>
                                Krabi 81110, Thailand
                            </address>
                            <br>
                            <span>Phone: +66 (0) 75681116 | Fax:+66 (0) 681124</span> <br>
                            <span>Email: <a href="mailto:info@univanich.com"
                                    class="link-color">info@univanich.com</a></span>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--contact us section end-->

        <!--google map block start-->
        <div class="google-map">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10090.984306909302!2d98.72521840584703!3d8.382339262564496!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3051a75f0c8520c9%3A0x1964f0da5d7b0ab0!2sUnivanich%20Palm%20Oil%20Public%20Company%20Limited%20(HQ)!5e0!3m2!1sen!2sth!4v1607671049500!5m2!1sen!2sth"
                height="100%" style="border:0;" allowfullscreen=""></iframe>
        </div>
        <!--google map block end-->

@endsection
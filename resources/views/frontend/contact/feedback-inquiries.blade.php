@extends('frontend.layouts.master')
@section('title', 'Feedback and Inquiries')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Feedback and Inquiries",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Contact Us',
                            'url' => 'contact'
                        ],

                        [
                            'item_title' => 'Feedback and Inquiries'
                        ],
                    ]
            ]
        )
<!--contact us section start-->
        <section class="contact-us-section ptb-100">
            <div class="container contact">
                <div class="col-12 pb-3 message-box d-none">
                    <div class="alert alert-danger"></div>
                </div>
                <div class="row justify-content-around">
                    <div class="col-md-6">
                        <div class="contact-us-form gray-light-bg rounded p-5">
                            <h4>Feedback and Inquiries</h4>
                            <p>In order to ensure a prompt response to your inquiry, please fill out the below Inquiry Form. Once all fields are completed, simply click on the submit button.</p>
                            <form action="" method="POST" id="contactForm" class="contact-us-form">
                                <div class="form-row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Full Name"
                                                required="required">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="address" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="city/state" placeholder="City/State">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="zip code" placeholder="Zip Code">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="occupation" placeholder="Occupation">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email"
                                                placeholder="Email" required="required">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea name="message" id="message" class="form-control" rows="7"
                                                cols="25" placeholder="Information Required"></textarea required="required">
                                        </div>
                                    </div>
                                    <div>
                                        <h5>How did you hear about us?</h5>
                                        <input type="radio" id="opt1" name="option" value="browsingthenet">
                                        <label for="opt1">Browsing the Net</label><br>
                                        <input type="radio" id="opt2" name="option" value="wordofmouth">
                                        <label for="opt2">Word of Mouth</label><br>
                                        <input type="radio" id="opt3" name="option" value="searchengines">
                                        <label for="opt3">Search Engines</label><br>
                                        <input type="radio" id="opt4" name="option" value="newspaperads">
                                        <label for="opt4">Newspaper Ads</label>
                                    </div>
                                    <div class="col-sm-12 mt-3">
                                        <button type="submit" class="btn secondary-solid-btn" id="btnContactUs">
                                            SUBMIT
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="contact-us-content">
                            <h2>Univanich Palm Oil Public Company Ltd.</h2>
                            <p class="lead">Univanich Palm Oil PCL has been a pioneer of the oil palm industry in
                                Thailand since the company’s first plantations were established back in 1969. Today,
                                Univanich is Thailand’s leading producer of crude palm oils and high quality oil palm
                                seeds.</p>

                            <a href="https://bit.ly/3n9ZBub" target="_blank" class="btn btn primary-solid-btn mr-2">Get Directions <span
                                    class="ti-arrow-right pl-2"></span></a>

                            <hr class="my-5">

                            <h5>Our Headquarters</h5>
                            <address>
                                258 Aoluk-Laemsak Road, P.O.BOX 8-9,<br>
                                Krabi 81110, Thailand
                            </address>
                            <br>
                            <span>Phone: +66 (0) 75681116 | Fax:+66 (0) 681124</span> <br>
                            <span>Email: <a href="mailto:info@univanich.com"
                                    class="link-color">info@univanich.com</a></span>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--contact us section end-->

@endsection
@extends('frontend.layouts.master')
@section('title', 'Harry Brock')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Harry Brock",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Management Team',
'url' => 'management-team'
],

[
'item_title' => 'Harry Brock'
],
]
]
)

<section class="team-single-section ptb-100">
    <div class="container">
        <div class="row align-items-top">
            <div class="col-md-12 col-sm-12 col-lg-5">
                <div class="team-single-img">
                    <img src="{{ asset('images/team/harry.jpg') }} " alt="management team member"
                        class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-6">
                <div class="team-single-text">
                    <div class="team-name mb-4">
                        <h4 class="mb-1">Mr. Harry Brock</h4>
                        <span>General Manager</span>
                    </div>
                    <div class="text-content mt-20">
                        <p></p><strong>Age:</strong> 45</p>
                        <strong>Education:</strong>
                        <p>Bachelor of Science, Biological Sciences at Birmingham University, UK Masters in Business
                            Administration (MBA) (with Distinction), Bath University, UK.</p>
                        <strong>Experience:</strong>
                        <p>From 1998 to 2018 Harry was employed by New Britain Palm Oil Ltd, Papua New Guinea (wholly
                            owned subsidiary of Sime Darby Plantations), holding various management positions. From 2011
                            – 2018, Harry was General Manager of New Britain Palm Oil Ltd, West New Britain Operations,
                            and in 2018, joined Sime Darby Plantations Malaysia, as Regional Chief Executive Officer for
                            the Central West Region. Harry joined Univanich in September 2019 as General Manager
                            responsible for management of all Univanich’s operations in Thailand.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--team single section end-->

@endsection
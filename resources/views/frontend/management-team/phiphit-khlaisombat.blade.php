@extends('frontend.layouts.master')
@section('title', 'Phiphit Khlaisombat')
@section('content')

@include('frontend.layouts.breadcrumb',
[
'page_title' => "Phiphit Khlaisombat",
'b_items' => [
[
'item_title' => 'Home',
'url' => 'home'
],

[
'item_title' => 'Management Team',
'url' => 'management-team'
],

[
'item_title' => 'Phiphit Khlaisombat'
],
]
]
)

<section class="team-single-section ptb-100">
    <div class="container">
        <div class="row align-items-top">
            <div class="col-md-12 col-sm-12 col-lg-5">
                <div class="team-single-img">
                    <img src="{{ asset('images/team/phiphit.jpg') }} " alt="board of directors member"
                        class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-6">
                <div class="team-single-text">
                    <div class="team-name mb-4">
                        <h4 class="mb-1">Mr. Phiphit Khlaisombat</h4>
                        <span>Chief Engineer</span>
                    </div>
                    <div class="text-content mt-20">
                        <p></p><strong>Age:</strong> 49</p>
                        <strong>Education:</strong>
                        <p>BE in Industrial/Production Engineering, King Mongkut’s University of Technology Thonburi.
                        </p>
                        <strong>Experience:</strong>
                        <p>Before joining Univanich, Phiphit Khlaisombat worked as a Project Manager in Berli Jucker
                            Public Company Limited. He joined Univanich in 2005 as a Factory Engineer and in 2008 was
                            appointed as Factories Manager responsible for the operation of the company’s three palm oil
                            crushing mills in Thailand. In January 2016 he was promoted to Chief Engineer responsible
                            for factory operations and new capital projects throughout Univanich group.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--team single section end-->

@endsection
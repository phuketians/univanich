@extends('frontend.layouts.master')
@section('title', 'Nattapong Dachanabhirom')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Nattapong Dachanabhirom",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Management Team',
                            'url' => 'management-team'
                        ],

                        [
                            'item_title' => 'Nattapong Dachanabhirom'
                        ],
                    ]
            ]
        )
        
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="{{ asset('images/team/nattapong.jpg') }} " alt="board of directors member" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">Mr. Nattapong Dachanabhirom</h4>
                                <span>Chief Financial Officer</span>
                            </div>
                            <div class="text-content mt-20">
                                <p></p><strong>Age:</strong> 50</p>
                                <strong>Education:</strong>
                                <p>Master of Business Administration, Prince of Songkla University. B.A. (Accounting) The University of the Thai Chamber of Commerce. Certified Public Accountant (Thailand).</p>
                                <strong>Experience:</strong>
                                <p>Mr. Nattapong Dachanabhirom worked as an Audit Supervisor with Coopers & Lybrand (TH) Company Limited for 5 years from 1992. In 1997 he joined Univanich as Financial Controller and was promoted to Accounting Manager in 2000, responsible for the company’s accounting and finance operations. In January 2016 he was promoted to be Chief Financial Officer responsible for financial operations and new business development throughout the Univanich Group.</p>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->
    
@endsection
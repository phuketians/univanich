@extends('frontend.layouts.master')
@section('title', 'Palat Tittinutchanon')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Palat Tittinutchanon",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Management Team',
                            'url' => 'management-team'
                        ],

                        [
                            'item_title' => 'Palat Tittinutchanon'
                        ],
                    ]
            ]
        )
        
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="{{ asset('images/team/palat.jpg') }} " alt="board of directors member" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">Mr. Palat Tittinutchanon</h4>
                                <span>Plantations Director, Head of Oil Palm Research</span>
                            </div>
                            <div class="text-content mt-20">
                                <p></p><strong>Age:</strong> 72</p>
                                <strong>Education:</strong>
                                <p>Ph.D. (Soil and Plant Science) University of Florida, U.S.A., a Master's degree from North Carolina State University, USA. and a Bachelor degree from Kasetsart University in Thailand.</p>
                                <strong>Experience:</strong>
                                <p>Before joining Univanich in 1988, Palat Tittinutchanon was Senior Oil Palm Agronomist in Thailand's Department of Agriculture. In 1988 he was appointed Research Officer responsible for the Univanich R&D programme. From 1994 he was seconded overseas by Unilever for two years as Estate Manager in Pamol Plantations Sdn Bhd in Malaysia. Upon his return to Thailand in 1996, he was appointed as Plantations Manager, responsible for operations in Univanich oil palm estates. In April 2012, Dr Palat was appointed to the Board as Plantations Director and Head of Oil Palm Research.</p>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->
    
@endsection
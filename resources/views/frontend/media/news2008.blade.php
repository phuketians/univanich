@extends('frontend.layouts.master')
@section('title', '2008 News Highlights')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "2008 News Highlights",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Media',
                            'url' => 'media'
                        ],

                        [
                            'item_title' => '2008 News Highlights'
                        ],
                    ]
            ]
        )
<!--blog section start-->
    <section class="our-blog-section ptb-100 gray-light-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading mb-5">
                        <h2><center>UVAN News</center></h2>
                        <h5><center>
                            News Highlights 2008</center></h5>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Indonesia approves Univanich oil palm seeds</span>
                            <img src="/images/blog/indonesia-news.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title">Univanich Carmen factory under construction in Philippines</h3>
                            <p class="card-text">Dr Palat Tittinutchanon (centre) explains details of the Univanich oil palm breeding programme to Mr Achmad Mangga Barani, Director General of Estate Crops, Indonesia’s Ministry of Agriculture, (left) and Mr Nelson Manurung, President Director of PT Agricinal, (right).  The Director General led a review team of oil palm breeders and senior officials to the Univanich seed gardens on 13th August 2008.</p>
                            <h5>Indonesia approves Univanich oil palm seeds </h5>
                            <p class="card-text">Following the visit to Southern Thailand by experts from Indonesia’s Ministry of Agriculture, official approval has been granted to Univanich Palm Oil PCL, of Krabi, to export hybrid oil palm seeds to growers in Indonesia.</p>
                            <p class="card-text">Univanich Managing Director, John Clendon, commented that Indonesia leads the world in oil palm cultivation with more than 31.2 million rai (five million hectares) in production.  Until recently the Indonesian market has been closed to outside seed producers since Indonesia already has many of the industry’s best breeding programmes which are reported to achieve the worlds highest oil palm yields.  The Indonesian Government has been careful not to allow import of seeds from regions where there is a risk from oil palm diseases or where the quality of the breeding programmes is not up to the highest international standards.</p>
                            <p class="card-text">According to John Clendon, this official approval for Univanich seeds to be sold in Indonesia is further recognition that the company’s research is producing very high quality planting material.  He noted that there has been much overseas interest in Univanich hybrid oil palms which are now growing in more than seven countries.  To enhance the future scope of this breeding programme Univanich has also constructed a tissue culture laboratory to produce Thailand’s first oil palm clones.  Mr Clendon said that the company has been investing in this tissue culture technology since 2005, aimed at cloning elite mother palms and to increase capacity for production of high quality seeds.</p>
                            <p class="card-text">The company’s Head of Oil Palm Research, Dr Palat Tittinutchanon, said that for 25 years the Univanich Oil Palm Research Centre has been importing pedigree palms from other research centres around the world.  In the company’s breeding programme these palms have been used to produce new varieties of hybrid oil palms.  Dr Palat reported that the company has planted more than 4,000 rai of research trials in the Univanich estates in Krabi, to identify the highest yielding hybrid crosses which are best adapted for Thailand’s dry climate and growing conditions.   In 2009, the company will produce 10 million oil palm seeds for export to major customers in South America, India and now in Indonesia, as well as to supply Thailand’s oil palm farmers with high quality seedlings.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Univanich Palm Oil PCL receives 2008 Best Performance Award</span>
                            <img src="/images/blog/setaward08.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title">Loading Thailand’s first cargo of CPSO at the Chean Vanich export jetty in Krabi</h3>
                            <p class="card-text">Mr John Clendon, Managing Director of Univanich Palm Oil PCL accepts the Best Performance trophy from Deputy Prime Minister Dr Olarn Chaiprawat at the 2008 SET Awards. Mr Clendon was also one of four finalists for Thailand’s CEO of the year award.</p>
                            <p>Since the company’s public listing in 2003 this is the second time that Univanich has been recognized with this top award. The finalists were selected from 470 public companies listed on Thailand’s main board exchange and judging was carried out in three rounds by the TRIS Rating Agency and SET Analysts. Performance criteria in the final selection included Return on Assets (ROA), Return on Equity (ROE), Earnings Growth and Return to Investors over a three year period.</p>
                        </div>
                    </div>
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">New Year Party 2009</span>
                            <img src="/images/blog/setaward08-kav.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">At a New Year party held at the Univanich staff Club, Chairman Mr Apirag Vanich congratulated the company’s management and staff on this recognition of their outstanding achievements in 2008.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">A new record for Univanich Palm Oil Exports</span>
                            <img src="/images/blog/cpo-export.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>A Vessel loading Univanich crude palm oil at Krabi, for delivery to Europe</h5>
                            <p class="card-text">During 2008 Univanich opened up new overseas markets in Europe and Asia, exporting a record 151,000 tonnes of crude palm oils. The company has become Thailand’s leading exporter of palm oil.</p>
                        </div>
                    </div>
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">International Publication of Univanich Research Papers</span>
                            <img src="/images/blog/the-planter.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>The Planter magazine, August 2008 edition with cover page from Univanich’s estates</h5>
                            <p class="card-text">The August 2008 edition of international publication “The Planter” featured two research papers on the Univanich oil palm breeding programme, and on the company’s irrigation trials reviewed after 15 years of recording.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Renovation of Siam Factory</span>
                            <img src="/images/blog/new-boiler.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>The new boiler installed at Siam Factory will improve efficiency and protect the environment</h5>
                            <img src="/images/blog/kernel-hopper.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <h5>During 2008, new palm kernel hoppers were installed at the Siam Factory to reduce handling cost of PK sent to Topi Factory for crushing</h5>
                            <p>During 2008, a new steam boiler, costing Baht 34 million, was installed at the Univanich Siam Factory to improve efficiency and protect the environment by reducing smoke emissions.</p>
                        </div>
                    </div>
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Prospecting for New Land</span>
                            <img src="/images/blog/new-land.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>Univanich managers inspecting new land which was formerly used for rice growing</h5>
                            <p class="card-text">During 2008, Univanich purchased 88.16 ha (551 rai) at a new location approximately 200km from the company’s HQ in Aoluk. It is planned to increase this area to 240 ha (1500 rai) during 2009, for future development of the company’s fourth site.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">2008 Oil Palm Replanting Programme</span>
                            <img src="/images/blog/rp2008.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>2008 replanting in Chean Vanich Estate</h5>
                            <p class="card-text">During 2008, the company replanted another 224 hectares (1401 rai). Since starting the annual replanting programme in 1992, 63% of the company’s estates have been replanted with a new generation of high yielding hybrid palms.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">2008 Oil Palm Irrigation Project</span>
                            <img src="/images/blog/new-irrigation.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>Installing new irrigation systems in a 2008 oil palm breeding trial</h5>
                            <p class="card-text">Each year Univanich installs new trials to test the latest generation of hybrid seedlings in both irrigated and non-irrigated conditions. This programme is designed to identify yield responses of different hybrids under both high and low rainfall conditions.</p>
                        </div>
                    </div>
                </div>
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-12">
                    <div class="action-btns mt-4" align="center">
                            <a href="/media/news2007" class="btn btn primary-solid-btn mr-2">2007 News Highlights</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--blog section end-->

@endsection
@extends('frontend.layouts.master')
@section('title', 'News')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "News",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Media',
                            'url' => 'media'
                        ],

                        [
                            'item_title' => 'News'
                        ],
                    ]
            ]
        )
<!--blog section start-->
    <section class="our-blog-section ptb-100 gray-light-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading mb-5">
                        <h2><center>UVAN News</center></h2>
                        <p class="lead"><center>
                            News Highlights 2013</center>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">New factory in Philippines</span>
                            <img src="/images/blog/carmen-factory.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title">Univanich Carmen factory under construction in Philippines</h3>
                            <p class="card-text">Construction of the new palm oil factory in Carmen Municipality of North Cotabato Province will be completed as planned in the second quarter of 2014. The Univanich Carmen Palm Oil Corporation (UCPOC) is a 51 : 49 jointventure with local investors and oil palm growers. It is the first palm oil crushing mill in North Cotabato where many small farmers are welcoming this new opportunity to increase the income from their family farms.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Thailand’s first export of Certified Sustainable Palm Oil (CSPO)</span>
                            <img src="/images/blog/cspo-img.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title">Loading Thailand’s first cargo of CPSO at the Chean Vanich export jetty in Krabi</h3>
                            <p class="card-text">Following the certification of Univanich plantations and crushing mills by the Round Table on Sustainable Palm Oil (RSPO), the company achieved another milestone by exporting Thailand’s first cargo of certified sustainable palm oil to premium markets in Europe.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">International presentation of Univanich research results</span>
                            <img src="/images/blog/int-presentation.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">At the 2013 Malaysian International Palm Oil Congress (PIPOC) the results of an important Univanich planting density trial was presented by Univanich consultant Dr RHV Corley, as a lead paper to more than 2,000 delegates. The trial was conducted over 16 years and the scientific paper was co-authored by Dr Corley and Dr Palat Tittinutchanon, our company’s Plantations Director and Head of Oil Palm Research.</p>
                            <img src="/images/blog/int-presentation2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p>During 2013, Mr John Clendon was invited to present Univanich research papers to the International Palm Produce Conference (IPPC) in Nigeria, and the Palm Oil Africa Summit in Gabon. West Africa is becoming an important market for Univanich seed exports.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">CSR Activities</span>
                            <img src="/images/blog/csr1.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">Dr Palat Tittinutchanon was the first patient to test a new dental clinic donated by Univanich to Plaipraya Hospital.</p>
                            <img src="/images/blog/csr2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p>On behalf of Univanich, Mr John Clendon presented two portable oxygen generators to Aoluk Hospital.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Awards</span>
                            <img src="/images/blog/awards01.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">At the 2013 Thailand Excellence Awards, Univanich was honoured as the Palm Oil Plantation Company of the Year, for a company which has pushed the boundaries of excellence and demonstrated outstanding performance in the Thailand market.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-12">
                    <div class="action-btns mt-4" align="center">
                            <a href="/media/news2008" class="btn btn primary-solid-btn mr-2">2008 News Highlights</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--blog section end-->

@endsection
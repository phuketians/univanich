@extends('frontend.layouts.master')
@section('title', '2007 News Highlights')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "2007 News Highlights",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Media',
                            'url' => 'media'
                        ],

                        [
                            'item_title' => '2007 News Highlights'
                        ],
                    ]
            ]
        )
<!--blog section start-->
    <section class="our-blog-section ptb-100 gray-light-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading mb-5">
                        <h2><center>UVAN News</center></h2>
                        <h5><center>
                            News Highlights 2007</center></h5>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Completion of Two Environmental Energy Projects</span>
                            <img src="/images/blog/biogas-siam.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title">Siam CIGAR</h3>
                            <p class="card-text">Waste water from the company’s Siam and Lamthap factories is now treated in covered in-ground anaerobic reactors (CIGARs) to digest the high organic content in the waste. This innovative digester technology to capture Methane biogas was designed for Univanich by Waste Solutions Ltd, an international firm of consulting engineers specialising in biogas technology.</p>
                            <img src="/images/blog/siam_biogas1.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p class="card-text">Methane biogas is normally a very damaging greenhouse gas but at the Siam and Lamthap factories it no longer escapes to the atmosphere but is used as a fuel. Two large biogas engines were imported from the Guascor company in Spain, each capable of generating up to 950kw electricity for supply to the provincial grid.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Reduction of Greenhouse Gas Emissions to create a new energy source</span>
                            <img src="/images/blog/ltcigar.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title">Lamthap CIGAR</h3>
                            <p class="card-text">The covered anaerobic digesters can hold up to 40,000 cubic metres of palm oil mill effluent (POME). The methane emissions are captured by heavy duty plastic sheeting and piped to the biogas power plant. After treatment in the digesters, water is returned to the existing pond system to be used for irrigation of the company’s plantations and nurseries.</p>
                            <img src="/images/blog/opening-biogas.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <h3 class="h5 mb-2 card-title">Siam Biogas Opening</h3>
                            <p>On 16th February 2008, Univanich Chairman Mr Apirag Vanich officially opened the completed biogas projects. In addition to the renewable energy source, the Siam and Lamthap projects are expected to produce revenue from Certified Emission Reductions (CERs) as a Clean Development Mechanism (CDM) incentive under the United Nations protocol on climate change.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Expanding Seed Exports</span>
                            <img src="/images/blog/seed-export.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">Germinated oil palm seeds are carefully packaged in cartons of 4,000 seeds each, for dispatch to domestic and export customers. During the past 30 years Univanich has collected and tested pedigree breeding material from research centres around the world. The company now has the most advanced oil palm breeding programme in Thailand, exporting to growers in seven countries.</p>
                            <img src="/images/blog/seed-export2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p class="card-text">Univanich germinated seeds are dispatched by airfreight from Phuket International Airport. The superior performance of this planting material, particularly in low rainfall conditions, has led to increasing export demand. New generations of hybrid crosses are being continuously tested in progeny trials, with an objective of achieving an annual seed production of 10 million seeds by 2010.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Expanding Oil Palm Research Centre</span>
                            <img src="/images/blog/tc-lab.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">At the Univanich Oil Palm Research Centre (OPRC) the tissue culture laboratory is expanding operations with more than 60,000 cultures under management. In the picture above the company’s Tissue Culturist Jeerawan Saengsawang displays cultures which have already become embryogenic, forming shoots of the next generation of oil palm clones.</p>
                            <img src="/images/blog/visitor.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <h5>Minister in Bunch Analysis Lab</h5>
                            <p>Minister of Agriculture and Cooperatives, Dr Teera Sutabutr, together with a group of senior officials recently visited the Univanich seed production unit. Pictured above, the minister and his group visit the bunch analysis laboratory where fruit from new progenies in the company’s breeding programme is carefully analysed for bunch composition, including high oil content.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Univanich Field Day</span>
                            <img src="/images/blog/field-day1.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">The Deputy Governor of Krabi Province, Mr Tanwakom Kemasiri, officially opened the first Univanich national field day on 15th December 2007. In his opening speech Khun Tanwakom commented that this initiative to share the company’s research with Thailand’s oil palm farmers would encourage the efficient growth of the country’s expanding oil palm industry.</p>
                            <img src="/images/blog/field-day2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p>More than 1,000 oil palm growers from around southern Thailand attended the Univanich field day. Managers at five estate sites and at the OPRC presented information and answered farmer questions about planting techniques, irrigation requirements, nursery management, and about the importance of advanced oil palm breeding to achieve higher crop yields.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Installation of New Irrigation Systems</span>
                            <img src="/images/blog/irrigation2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">Each year Univanich installs new irrigation systems to test the latest generation of hybrid seedlings in both irrigated and non-irrigated field trials. This programme is designed to identify yield responses of different progenies under both high and low rainfall conditions.</p>
                        </div>
                    </div>
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">New Employee Houses</span>
                            <img src="/images/blog/worker-houses.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">During 2007, an additional 10 new houses were built for plantations workers and their families. Since the company’s employees are now provided with this high standard of estate housing, there has been low labour turn-over and the company has been fortunate to retain a skilled and stable workforce.</p>
                        </div>
                    </div>
                </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Univanich research papers presented to overseas conferences</span>
                            <img src="/images/blog/india2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">Univanich Managing Director, Mr John Clendon, was invited to present a paper titled “A Review of 15 years of oil palm irrigation research in Southern Thailand” to the Indian National Conference on Oil Palm held in Andhra Pradesh. Since the Government of India is encouraging the expansion of oil palm cultivation there was much interest in the Univanich research carried-out in low rainfall conditions similar to those in India’s oil palm regions.</p>
                            <img src="/images/blog/booth-india7.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p class="card-text">At the same conference in India, research consultant, Dr V Rao, presented a paper titled “The Univanich Oil Palm Breeding Programme and Progeny Trial Results from Thailand”. Pictured above at the conference trade booth are Ms Pritsana Kusakoon (Supervisor, Seed Exports), Dr V Rao, and Dr Palat Tittinutchanon (Plantations Controller and Head of Oil Palm Research).</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">International seeds marketing</span>
                            <img src="/images/blog/kltradebooth.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">The recent Price Outlook Conference in Malaysia was attended by more than 1,800 international palm oil producers and consumers. Univanich was represented at a trade booth to promote awareness of the company’s high yielding oil palm seeds, and to meet customers from around the world.</p>
                        </div>
                    </div>
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">A bright light on the horizon</span>
                            <img src="/images/blog/flair.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">At Univanich, surplus methane biogas emitted naturally from the organic waste of palm oil factories, is now destroyed in a flare and no longer escapes to the atmosphere as a damaging greenhouse gas.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Expanding Seed Exports to South America</span>
                            <img src="/images/blog/seed-export3.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">Following the success of earlier trial plantings of Univanich seeds in South America , oil palm growers there have recently increased their orders for the latest generation of Univanich hybrid seeds.Several large consignments of seeds have recently been airfreighted from Phuket International Airport , mainly to Colombia , which is South America 's leading palm oil producer and exporter.</p>
                            <p class="card-text">With interest in Univanich DxP hybrid seeds continuing to grow in overseas markets, the company has expanded production facilities and introduced ISO 9001 auditing procedures to maintain high quality standards in the seed production unit.</p>
                        </div>
                    </div>
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Investment in Environmental Energy Projects</span>
                            <img src="/images/blog/energy-project1.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">Univanich has started construction of large CIGAR ponds (Covered In-Ground Anaerobic Reactors) at two of the company's palm oil crushing mills. The covered ponds will collect Methane biogas emissions from the factory's waste water. Methane is a damaging greenhouse gas generated naturally from the action of bacteria on organic material found in waste water ponds. By preventing the discharge of Methane to the atmosphere these projects are intended to generate income from Carbon Credits (Certified Emission Reductions) under the United Nations Protocol on Climate Change.</p>
                            <img src="/images/blog/energy-project2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p>The Methane biogas collected in these reactors will be used to generate electricity for sale to the Provincial Electricity Authority under the recently announced incentives for Very Small Power Producers (VSPP Scheme)</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Minister of Agriculture and Senior Officials visit Univanich</span>
                            <img src="/images/blog/minister_agr1.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">On 15th July 2007, Thailand 's Minister of Agriculture and Co-operatives, Dr Teera Sutabutr, visited Univanich Palm Oil PCL, accompanied by senior ministry officials. Also accompanying the Minister was the Krabi Governor Mr Siva Sirisaowalak. In this picture Estate Manager, Mr Amorn Chaisalee, presents the results of an irrigation trial in the company's Chean Vanich Estate.</p>
                            <img src="/images/blog/minister_agr2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p>During his tour of the company's Bunch Analysis Laboratory the Minister commented that both the Univanich Company and his Ministry shared the same objective of working to raise the yield of oil palms grown by Thailand 's farmers. The investment in bunch analysis was described as an essential tool for raising oil yields from the new generations of palms.</p>
                            <img src="/images/blog/minister_agr3.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p>he Minister congratulated Univanich Managing Director, Mr John Clendon, on the achievements of the company's Research Centre and on the international success of the oil palm breeding programme.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Public Consultation for United Nations CDM (Clean Development Mechanism)</span>
                            <img src="/images/blog/cdm1.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">Univanich has been holding public meetings to explain the company's objective to build biogas power projects and to register for Certified Emission Reductions (CER's or Carbon Credits) under the United Nations Protocol on Climate Change. In the picture above, the Univanich project team led by Factories Manager Somphon Tantitham, presents the company's plans to the Lamthap Communitiy.</p>
                            <img src="/images/blog/cdm2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p class="card-text">Members of the Community and other interested stakeholders were encouraged to raise questions at the consultation meeting. A detailed survey after the meeting revealed strong community support for the proposed project which was generally seen to help the environment by reducing greenhouse gas emissions and by generating electricity from a renewable local resource.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Progress in Oil Palm Tissue Culture</span>
                            <img src="/images/blog/tc-lab3.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">Dr Palat Tittinutchanon, Head of Oil Palm Research, explains the various stages of clone development to visitors to the company’s Tissue Culture Laboratory. The Univanich laboratory was constructed in 2005 to support the company’s expanded oil palm breeding activities. Initial objectives are to clone elite parent palms from the company’s hybrid breeding programme.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-12">
                    <div class="action-btns mt-4" align="center">
                            <a href="/media/news2006" class="btn btn primary-solid-btn mr-2">2006-2004 News Highlights</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--blog section end-->

@endsection
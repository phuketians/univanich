<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="BizBite corporate business template or agency and marketing template helps you easily create websites for your business.">
    <meta name="author" content="ThemeTags">


    <!--title-->    
    <title>News || Univanich</title>

    <!--favicon icon-->
    <link rel="icon" href="img/favicon.png" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans:400,600&display=swap" rel="stylesheet">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!--Magnific popup css-->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!--Themify icon css-->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!--Fontawesome icon css-->
    <link rel="stylesheet" href="css/all.min.css">
    <!--animated css-->
    <link rel="stylesheet" href="css/animate.min.css">
    <!--ytplayer css-->
    <link rel="stylesheet" href="css/jquery.mb.YTPlayer.min.css">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!--custom css-->
    <link rel="stylesheet" href="css/style.css">
    <!--responsive css-->
    <link rel="stylesheet" href="css/responsive.css">

</head>
<body>

<!--loader start-->
<div id="preloader">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!--loader end-->

<!--header section start-->
<header class="header">    
    <!--start navbar-->
    <nav class="navbar navbar-expand-lg fixed-top white-bg">
        <div class="container position-relative">
            <a class="navbar-brand" href="home.html">
                <img src="img/logo-white.png" alt="logo" class="img-fluid"/>
            </a>

            <div class="main-title">
                <div class="title"> Univanich Plam Oil Public Co., Ltd.</div>
                <div class="sub-title">
                    <span>Univanich | 45.50 (+2.25 | 5.70%)</span>                    
                    <span>
                        <a href="#">TH</a> |
                        <a href="#">EN</a>
                    </span> 
                </div>                
            </div>             

            <div class="navbar-header">

                <a class="navbar-brand text-uppercase" href="#">open menu</a>

                <button data-target="#top-nav" data-toggle="collapse" type="button" class="navbar-toggle">
                    <i class="fa fa-bars"></i>
                </button>
                
            </div>            
        </div>

        <div class="MenuWrap">
            <div class="container">
                <ul>
                    <li><a href="home.html">Home</a></li>
                    <li><a href="company_profile.html">Company Profile</a>
                        <ul>
                            <li><a href="chairman_report.html">Message from CEO and/or? Chairman</a></li>
                            <li><a href="principles-code.html">Code of Business Principles</a></li>
                            <li><a href="company_profile.html">Company Profile/Who we are</a></li>
                            <li><a href="timeline.html">Timeline</a></li>
                            <li><a href="#">Board of Directors</a></li>
                            <li><a href="#">Management Team</a></li>
                            <li><a href="#">Awards</a></li>                            
                        </ul>
                    </li>
                    <li><a href="#">Operations</a>
                        <ul>
                            <li><a href="estates.html">Estates</a></li>
                            <li><a href="factories.html">Factories</a></li>
                            <li><a href="#">Biogas</a></li>
                            <li><a href="nurseries.html">Nurseries</a></li>
                        </ul>

                        <a href="#">Products</a>

                        <ul>
                            <li><a href="#">CPO & PKO</a></li>
                            <li><a href="#">Seeds & Seedlings</a></li>
                            <li><a href="#">Byproducts</a></li>
                            <li><a href="#">Biogas</a></li>                        
                        </ul>
                    </li>

                    <li><a href="#">Investor Relations</a>
                        <ul>
                            <li><a href="company_profile.html">Company Profile</a></li>
                            <li><a href="financial_information.html">Financial Information</a></li>
                            <li><a href="#">Stock Information (stock quotes, embed like BGP)</a></li>
                            <li><a href="shareholder.html">Shareholder Information</a></li>
                            <li><a href="#">Newsroom</a></li>
                            <li><a href="awards.html">Awards</a></li>
                            <li><a href="#">Publications and Download</a>
                                <ul>
                                    <li><a href="#">Annual report</a></li>
                                    <li><a href="#">Company factsheet</a></li>
                                    <li><a href="#">Brochure</a></li>
                                </ul>
                            </li>                            
                        </ul>
                    </li>
                    <li><a href="#">Sustainability</a>
                        <ul>
                            <li><a href="#">CSR (Social Sustainability)</a></li>
                            <li><a href="#">RSPO (Environmental Sustainability)</a></li>
                            <li><a href="#">Corporate Governance</a></li>
                            <li><a href="#">Environment (Environmental Sustainability)</a></li>
                        </ul>                            
                    </li>
                    <li><a href="#">Media</a>
                        <ul>
                            <li><a href="#">News</a></li>
                            <li><a href="#">Downloads</a>
                                <ul>
                                    <li><a href="#">Fact sheets/brochures/etc</a></li>
                                </ul>
                            </li>
                        </ul>    

                        <a href="#">Careers</a>
                        <ul>
                            <li><a href="#">Careers Center</a></li>
                        </ul>

                        <a href="#">Contact Us</a>

                        <ul>
                            <li><a href="#">Whistleblowing</a></li>
                            <li><a href="#">Feedback and Inquiries</a></li>
                        </ul> 

                    </li> 
                </ul>
            </div>
        </div>        
    </nav>
</header>
<!--header section end-->

<!--body content wrap start-->
<div class="main">

    <!--header section start-->
    <section class="hero-section ptb-100 gradient-overlay"
             style="background: url('img/header-bg-5.jpg')no-repeat center center / cover">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-7">
                    <div class="page-header-content text-white text-center pt-sm-5 pt-md-5 pt-lg-0">
                        <h1 class="text-white mb-0">News</h1>
                        <div class="custom-breadcrumb">
                            <ol class="breadcrumb d-inline-block bg-transparent list-inline py-0">
                                <li class="list-inline-item breadcrumb-item"><a href="home.html">Home</a></li>
                                <li class="list-inline-item breadcrumb-item active">News</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--header section end-->

    <!--blog section start-->
    <section class="our-blog-section ptb-100 gray-light-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="section-heading mb-5">
                        <h2>Our Latest News</h2>
                        <p class="lead">
                            Enthusiastically drive revolutionary opportunities before emerging leadership. Phosfluorescently cultivate emerging alignments time methods of empowerment.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <a href="blog-details.html"><span class="category position-absolute">Business</span></a>
                            <a href="blog-details.html"><img src="img/blog/1.jpg" class="card-img-top position-relative img-fluid" alt="blog"></a>
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title"><a href="blog-details.html">Appropriately productize fully</a></h3>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk.</p>
                        </div>
                        <div class="card-footer border-0 d-flex align-items-center justify-content-between">
                            <div class="author-meta d-flex align-items-center">
                                <span class="fa fa-user mr-2 p-3 bg-white rounded-circle border"></span>
                                <div class="author-content">
                                    <a href="blog-details.html" class="d-block">ThemeTags</a>
                                    <small>May 26, 2020</small>
                                </div>
                            </div>
                            <div class="author-like">
                                <a href="blog-details.html"><span class="fa fa-share-alt"></span> 50</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <a href="blog-details.html"><span class="category position-absolute">SEO, Analytics</span></a>
                            <a href="blog-details.html"><img src="img/blog/2.jpg" class="card-img-top position-relative img-fluid" alt="blog"></a>
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title"><a href="blog-details.html">Quickly formulate backend</a></h3>
                            <p class="card-text">Synergistically engage effective ROI after customer directed partnerships.</p>
                        </div>
                        <div class="card-footer border-0 d-flex align-items-center justify-content-between">
                            <div class="author-meta d-flex align-items-center">
                                <span class="fa fa-user mr-2 p-3 bg-white rounded-circle border"></span>
                                <div class="author-content">
                                    <a href="blog-details.html" class="d-block">ThemeTags</a>
                                    <small>May 28, 2020</small>
                                </div>
                            </div>
                            <div class="author-like">
                                <a href="blog-details.html"><span class="fa fa-share-alt"></span> 30</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <a href="blog-details.html"><span class="category position-absolute">Marketing</span></a>
                            <a href="blog-details.html"><img src="img/blog/3.jpg" class="card-img-top position-relative img-fluid" alt="blog"></a>
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title"><a href="blog-details.html">Objectively extend extensive</a></h3>
                            <p class="card-text">Holisticly mesh open-source leadership rather than proactive users.</p>
                        </div>
                        <div class="card-footer border-0 d-flex align-items-center justify-content-between">
                            <div class="author-meta d-flex align-items-center">
                                <span class="fa fa-user mr-2 p-3 bg-white rounded-circle border"></span>
                                <div class="author-content">
                                    <a href="blog-details.html" class="d-block">ThemeTags</a>
                                    <small>May 30, 2020</small>
                                </div>
                            </div>
                            <div class="author-like">
                                <a href="blog-details.html"><span class="fa fa-share-alt"></span> 55</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <a href="blog-details.html"><span class="category position-absolute">Marketing</span></a>
                            <a href="blog-details.html"><img src="img/blog/4.jpg" class="card-img-top position-relative img-fluid" alt="blog"></a>
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title"><a href="blog-details.html">Intrinsicly provide access</a></h3>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk.</p>
                        </div>
                        <div class="card-footer border-0 d-flex align-items-center justify-content-between">
                            <div class="author-meta d-flex align-items-center">
                                <span class="fa fa-user mr-2 p-3 bg-white rounded-circle border"></span>
                                <div class="author-content">
                                    <a href="blog-details.html" class="d-block">ThemeTags</a>
                                    <small>May 20, 2020</small>
                                </div>
                            </div>
                            <div class="author-like">
                                <a href="blog-details.html"><span class="fa fa-share-alt"></span> 20</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <a href="blog-details.html"><span class="category position-absolute">Technology</span></a>
                            <a href="blog-details.html"><img src="img/blog/5.jpg" class="card-img-top position-relative img-fluid" alt="blog"></a>
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title"><a href="blog-details.html">Foster exceptional niches</a></h3>
                            <p class="card-text">Synergistically engage effective ROI after customer directed partnerships.</p>
                        </div>
                        <div class="card-footer border-0 d-flex align-items-center justify-content-between">
                            <div class="author-meta d-flex align-items-center">
                                <span class="fa fa-user mr-2 p-3 bg-white rounded-circle border"></span>
                                <div class="author-content">
                                    <a href="blog-details.html" class="d-block">ThemeTags</a>
                                    <small>Mar 28, 2020</small>
                                </div>
                            </div>
                            <div class="author-like">
                                <a href="blog-details.html"><span class="fa fa-share-alt"></span> 10</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <a href="blog-details.html"><span class="category position-absolute">Business & Marketing</span></a>
                            <a href="blog-details.html"><img src="img/blog/6.jpg" class="card-img-top position-relative img-fluid" alt="blog"></a>
                        </div>
                        <div class="card-body">
                            <h3 class="h5 mb-2 card-title"><a href="blog-details.html">Object communicate business</a></h3>
                            <p class="card-text">Holisticly mesh open-source leadership rather than proactive users.</p>
                        </div>
                        <div class="card-footer border-0 d-flex align-items-center justify-content-between">
                            <div class="author-meta d-flex align-items-center">
                                <span class="fa fa-user mr-2 p-3 bg-white rounded-circle border"></span>
                                <div class="author-content">
                                    <a href="blog-details.html" class="d-block">ThemeTags</a>
                                    <small>Apr 30, 2020</small>
                                </div>
                            </div>
                            <div class="author-like">
                                <a href="blog-details.html"><span class="fa fa-share-alt"></span> 25</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--pagination start-->
            <div class="row">
                <div class="col-md-12">
                    <nav class="custom-pagination-nav mt-4">
                        <ul class="pagination justify-content-center">
                            <li class="page-item"><a class="page-link" href="#"><span class="ti-angle-left"></span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#"><span class="ti-angle-right"></span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!--pagination end-->

        </div>
    </section>
    <!--blog section end-->

</div>
<!--body content wrap end-->

<!--footer section start-->
<footer class="footer-section">  
    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg py-3">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12">
                    <p class="copyright-text pb-0 mb-0">Copyrights © 2020. All
                        rights reserved by Univanich Plam Oil Public Co. Ltd.                        
                </div>
                
            </div>
        </div>
    </div>
    <!--footer copyright end-->
</footer>
<!--footer section end-->

<!--bottom to top button start-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="ti-angle-up"></span>
</button>
<!--bottom to top button end-->


<!--jQuery-->
<script src="js/jquery-3.4.1.min.js"></script>
<!--Popper js-->
<script src="js/popper.min.js"></script>
<!--Bootstrap js-->
<script src="js/bootstrap.min.js"></script>
<!--Magnific popup js-->
<script src="js/jquery.magnific-popup.min.js"></script>
<!--jquery easing js-->
<script src="js/jquery.easing.min.js"></script>
<!--jquery ytplayer js-->
<script src="js/jquery.mb.YTPlayer.min.js"></script>
<!--Isotope filter js-->
<script src="js/mixitup.min.js"></script>
<!--wow js-->
<script src="js/wow.min.js"></script>
<!--owl carousel js-->
<script src="js/owl.carousel.min.js"></script>
<!--countdown js-->
<script src="js/jquery.countdown.min.js"></script>
<!--custom js-->
<script src="js/scripts.js"></script>
</body>
</html>
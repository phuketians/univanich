<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Univanich || 500 Error</title>

    <!--favicon icon-->
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans:400,600&display=swap"
        rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('backend/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">


    <div class="error-box text-center animated fadeInDown">
        <h1>500</h1>
        <h3 class="font-bold-error">Internal Server Error</h3>

        <div class="error-desc">
            The server encountered something unexpected that didn't allow it to complete the request. We apologize.
            <br />
            You can go back to main page:
            <br />
            <a href="{{route('home')}}" class="btn btn-primary m-t">Home</a>
        </div>
    </div>


    <script src="{{asset('backend/js/app.js')}}"></script>
</body>

</html>

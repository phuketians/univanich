const mix = require('laravel-mix');

/**
 * Frontend css mix
 */

mix.styles(
    [
        "resources/frontend/assets/css/bootstrap.min.css",
        "resources/frontend/assets/css/magnific-popup.css",
        "resources/frontend/assets/css/themify-icons.css",
        "resources/frontend/assets/css/all.min.css",
        "resources/frontend/assets/css/animate.min.css",
        "resources/frontend/assets/css/jquery.mb.YTPlayer.min.css",
        "resources/frontend/assets/css/owl.carousel.min.css",
        "resources/frontend/assets/css/owl.theme.default.min.css",
        "resources/frontend/assets/css/style.css",
        "resources/frontend/assets/css/responsive.css",
        
    ],
    "public/frontend/css/app.css"
);

/**
 * Frontend js mix
 */

mix.scripts(
    [
        "resources/frontend/assets/js/jquery-3.4.1.min.js",        
        "resources/frontend/assets/js/popper.min.js",        
        "resources/frontend/assets/js/bootstrap.min.js",        
        "resources/frontend/assets/js/jquery.magnific-popup.min.js",        
        "resources/frontend/assets/js/jquery.easing.min.js",        
        "resources/frontend/assets/js/jquery.mb.YTPlayer.min.js",        
        "resources/frontend/assets/js/mixitup.min.js",        
        "resources/frontend/assets/js/wow.min.js",        
        "resources/frontend/assets/js/owl.carousel.min.js",        
        "resources/frontend/assets/js/jquery.countdown.min.js",        
        "resources/frontend/assets/js/scripts.js",        
       
    ],
    "public/frontend/js/app.js"
);

/**
 * Backend css mix
 */

mix.styles(
    [
        "resources/backend/assets/css/bootstrap.min.css",
        "resources/backend/assets/css/animate.css",
        "resources/backend/assets/css/style.css",
        "resources/backend/assets/css/sweetalert.css",
    ],
    "public/backend/css/app.css"
);

/**
 * Backend js mix
 */

mix.scripts(
    [
        "resources/backend/assets/js/jquery-3.1.1.min.js",
        "resources/backend/assets/js/popper.min.js",
        "resources/backend/assets/js/bootstrap.js",
        "resources/backend/assets/js/sweetalert.min.js",
        "resources/backend/assets/js/plugins/metisMenu/jquery.metisMenu.js",
        "resources/backend/assets/js/plugins/slimscroll/jquery.slimscroll.min.js",
        "resources/backend/assets/js/inspinia.js",
        "resources/backend/assets/js/custom.js",
    ],
    "public/backend/js/app.js"
);




/*!
 * 
 * Premium Stock Market Widgets
 * -----------------------------
 * Version 3.2.1, built on Tuesday, December 15, 2020
 * Copyright (c) Financial apps and plugins <info@financialplugins.com>. All rights reserved
 * Demo: https://stockmarketwidgets.financialplugins.com/
 * Purchase (WordPress version): https://1.envato.market/az97R
 * Purchase (PHP version): https://1.envato.market/AQ17o
 * Like: https://www.facebook.com/financialplugins/
 * 
 */
(window.webpackJsonp1818=window.webpackJsonp1818||[]).push([[19],{1035:function(t,e,s){"use strict";s.r(e);var n=function(){var t=this.$createElement;return(this._self._c||t)("generic",this._b({tag:"component"},"component",this.$attrs,!1))};n._withStripped=!0;var i=s(267),a=function(){var t=this.$createElement,e=this._self._c||t;return e("div",{class:this.classes},[this.display?e("canvas",{staticClass:"smw-chart-container"}):e("div",{staticClass:"smw-flexbox smw-flexbox-align-items-center smw-flexbox-justify-content-center"},[e("i",{staticClass:"fas fa-spinner fa-spin"})])])};a._withStripped=!0;var o=s(261),r=s(1031),c={mixins:[o.a,r.a]},l=(s(801),s(0)),u=Object(l.a)(c,a,[],!1,null,"37e61a56",null);u.options.__file="assets/js/components/widgets/doughnut/template.vue";var p={components:{Generic:u.exports},mixins:[i.a]},h=Object(l.a)(p,n,[],!1,null,null,null);h.options.__file="assets/js/components/widgets/doughnut/type.vue";e.default=h.exports},413:function(t,e,s){var n=s(28),i=s(802);"string"==typeof(i=i.__esModule?i.default:i)&&(i=[[t.i,i,""]]);var a={insert:"head",singleton:!1};n(i,a);t.exports=i.locals||{}},801:function(t,e,s){"use strict";var n=s(413);s.n(n).a},802:function(t,e,s){"use strict";s.r(e);var n=s(16),i=s.n(n)()(!1);i.push([t.i,".smw-widget[data-v-37e61a56]{color:inherit;width:20em;height:15em}.smw-widget .smw-flexbox[data-v-37e61a56]{color:inherit;height:100%}\n",""]),e.default=i}}]);
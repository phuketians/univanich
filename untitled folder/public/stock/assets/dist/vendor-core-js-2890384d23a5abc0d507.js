/*!
 * 
 * Premium Stock Market Widgets
 * -----------------------------
 * Version 3.2.1, built on Tuesday, December 15, 2020
 * Copyright (c) Financial apps and plugins <info@financialplugins.com>. All rights reserved
 * Demo: https://stockmarketwidgets.financialplugins.com/
 * Purchase (WordPress version): https://1.envato.market/az97R
 * Purchase (PHP version): https://1.envato.market/AQ17o
 * Like: https://www.facebook.com/financialplugins/
 * 
 */
(window.webpackJsonp1818=window.webpackJsonp1818||[]).push([[45],{509:function(t,n,o){o(510),o(513),o(514),o(515),o(516),o(517),o(518),o(519),o(520),o(521),o(522),o(523),o(524),o(525),o(526),o(527),o(528),o(529),o(530),o(531),o(532),o(533),o(534),o(535),o(536),o(537),o(538),o(539),o(540),o(541),o(542),o(543),o(544),o(545),o(546),o(547),o(548),o(549),o(550),o(551),o(552),o(553),o(554),o(556),o(557),o(558),o(559),o(560),o(561),o(562),o(563),o(564),o(565),o(566),o(567),o(568),o(569),o(570),o(571),o(572),o(573),o(574),o(575),o(576),o(577),o(578),o(579),o(580),o(581),o(582),o(583),o(584),o(585),o(586),o(587),o(588),o(589),o(591),o(592),o(594),o(595),o(596),o(597),o(598),o(599),o(600),o(602),o(603),o(604),o(605),o(606),o(607),o(608),o(609),o(610),o(611),o(612),o(613),o(614),o(354),o(615),o(488),o(616),o(489),o(617),o(618),o(619),o(620),o(490),o(623),o(624),o(625),o(626),o(627),o(628),o(629),o(630),o(631),o(632),o(633),o(634),o(635),o(636),o(637),o(638),o(639),o(640),o(641),o(642),o(643),o(644),o(645),o(646),o(647),o(648),o(649),o(650),o(651),t.exports=o(275)},652:function(t,n,o){o(653),t.exports=o(275).Array.includes},654:function(t,n,o){o(655),t.exports=o(275).Array.flatMap},657:function(t,n,o){o(658),t.exports=o(275).String.padStart},659:function(t,n,o){o(660),t.exports=o(275).String.padEnd},661:function(t,n,o){o(662),t.exports=o(275).String.trimLeft},663:function(t,n,o){o(664),t.exports=o(275).String.trimRight},665:function(t,n,o){o(666),t.exports=o(335).f("asyncIterator")},667:function(t,n,o){o(668),t.exports=o(275).Object.getOwnPropertyDescriptors},669:function(t,n,o){o(670),t.exports=o(275).Object.values},671:function(t,n,o){o(672),t.exports=o(275).Object.entries},673:function(t,n,o){"use strict";o(490),o(674),t.exports=o(275).Promise.finally},675:function(t,n,o){o(676),o(677),o(678),t.exports=o(275)},680:function(t,n,o){o(681),t.exports=o(499).global}}]);
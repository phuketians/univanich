<?php

error_reporting(0);

// define plugin root folder to be used by other classess
define('SMW_ROOT_DIR', dirname(__FILE__));

// register autoload function
spl_autoload_register(function ($className) {
    if (strpos($className,'PremiumStockMarketWidgets')!==FALSE) {
        $classFileName = 'classes' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
        if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . $classFileName))
            require_once $classFileName;
    }
});

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-Control: max-age=0", false);
header("Pragma: no-cache");

// search
if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'smwAssetSearch') {
    print (new \PremiumStockMarketWidgets\AssetSearch($_REQUEST))->get();
// market data request
} else if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'smwGetMarketData') {
    print (new \PremiumStockMarketWidgets\MarketData($_REQUEST))->get();
}
<?php
$assetsLogoImages = array_slice(scandir('../assets/images/logo'), 2);

// convert the list to symbol => image associative array, e.g. ['AAPL' => 'AAPL.png']
$assetsLogoImages = array_combine(array_map(function ($image) {
    return substr($image, 0, strrpos($image, '.'));
}, $assetsLogoImages), $assetsLogoImages);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Premium Stock Market Widgets | Builder</title>

  <link rel="stylesheet" type="text/css" href="../assets/dist/main.css"/>
  <link rel="stylesheet" type="text/css" href="../assets/dist/builder.css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.10.0/styles/default.min.css">

  <style>
  h1 {
    font-size: 2em;
  }

  h2 {
    font-size: 1.5em;
    border-bottom: 1px solid darkgrey;
  }

  .container {
    width: 80%;
    max-width: 1200px;
    margin-left: auto;
    margin-right: auto;
  }

  .smw-parameter {
    margin-top: 1em;
  }

  input[type=text], input[type=number], select {
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;
    height: 28px;
    padding: 0 8px;
  }

  input[type=text]:read-only {
    background: #eee;
  }

  footer {
    margin-top: 3em;
    padding-top: 1em;
    border-top: 1px solid darkgrey;
  }
  </style>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<div class="container">
  <div>
    <h1>Premium Stock Market Widgets</h1>

    <h2>Widget Builder</h2>

    <div id="smw-widget-builder">
      <stock-market-widget-builder></stock-market-widget-builder>
    </div>
  </div>

  <footer>
    &copy; <a href="https://financialplugins.com/" target="_blank">FinancialPlugins.com</a>
  </footer>
</div>
<script>
var premiumStockMarketWidgets = {
  pluginUrl: '../',
  text: {
    website: 'Webseite',
    industry: 'Industrie',
    sector: 'Sektor'
  },
  assetsLogoImages: JSON.parse('<?php print json_encode($assetsLogoImages)?>')
}
</script>
<script src="../assets/dist/builder.js"></script>
</body>
</html>

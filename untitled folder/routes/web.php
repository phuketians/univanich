    <?php
    
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;
    
    Auth::routes();
    
    
    Route::get('/', 'HomeController@index')->name('home');
    
    Route::group( ['prefix' => 'company-profile' ],
    
        function () {
            Route::get('/', 'Front\CompanyController@index')->name('company-profile');
            Route::get('chairman-report', 'Front\CompanyController@chairmanReport')->name('chairman-report');
            Route::get('business-principles', 'Front\CompanyController@principlesCode')->name('principles-code');
            Route::get('corporate-governance', 'Front\CompanyController@corporateGovernance')->name('corporate-governance');        
            Route::get('timeline', 'Front\CompanyController@timeline')->name('timeline');
            Route::get('board-of-directors', 'Front\CompanyController@boardOfDirectors')->name('board-of-directors');
            Route::get('management-team', 'Front\CompanyController@managementTeam')->name('management-team');
			Route::get('awards', 'Front\CompanyController@awards')->name('awards');
            }
    );

    Route::group( ['prefix' => 'management-team' ],
    
        function () {
        Route::get('/', 'Front\ManagementController@index')->name('management-team');
        Route::get('thanaphol-leelangamwong', 'Front\ManagementController@thanapholLeelangamwong')->name('thanaphol-leelangamwong');
        Route::get('santi-suanyot', 'Front\ManagementController@santiSuanyot')->name('santisuanyot');
        Route::get('phiphit-khlaisombat', 'Front\ManagementController@phiphitKhlaisombat')->name('phiphit-khlaisombat');
        Route::get('nattapong-dachanabhirom', 'Front\ManagementController@nattapongDachanabhirom')->name('nattapong-dachanabhirom');
        Route::get('palat-tittinutchanon', 'Front\ManagementController@palatTittinutchanon')->name('palat-tittinutchanon');
        Route::get('harry-brock', 'Front\ManagementController@harryBrock')->name('harry-brock');
        Route::get('john-clendon', 'Front\ManagementController@johnClendon')->name('john-clendon');
            
        }
    );
    
    Route::group( ['prefix' => 'directors' ],
    
        function () {
        Route::get('/', 'Front\DirectorsController@index')->name('directors');
        Route::get('apirag-vanich', 'Front\DirectorsController@apiragVanich')->name('apirag-vanich');
        Route::get('phortchana-manoch', 'Front\DirectorsController@phortchanaManoch')->name('phortchana-manoch');
        Route::get('kanchana-vanich', 'Front\DirectorsController@kanchanaVanich')->name('kanchana-vanich');
        Route::get('palat-tittinutchanon', 'Front\DirectorsController@palatTittinutchanon')->name('palat-tittinutchanon');
        Route::get('chantip-vanich', 'Front\DirectorsController@chantipVanich')->name('chantip-vanich');
        Route::get('prote-sosothikul', 'Front\DirectorsController@proteSosothikul')->name('prote-sosothikul');
        Route::get('john-clendon', 'Front\DirectorsController@johnClendon')->name('john-clendon');
        Route::get('suchad-chiaranussati', 'Front\DirectorsController@suchadChiaranussati')->name('suchad-chiaranussati');
        Route::get('supapang-chanlongbutra', 'Front\DirectorsController@supapangChanlongbutra')->name('supapang-chanlongbutra');
        Route::get('pramoad-phornprapha', 'Front\DirectorsController@pramoadPhornprapha')->name('pramoad-phornprapha');
        }
    );
    
    Route::group( ['prefix' => 'operations' ],
        function () {
            Route::get('/', 'Front\OperationsController@index')->name('operations');
            Route::get('estates', 'Front\OperationsController@estates')->name('estates');
            Route::get('factories', 'Front\OperationsController@factories')->name('factories');
            Route::get('nurseries', 'Front\OperationsController@nurseries')->name('nurseries');
        }
    );
    
    Route::group( ['prefix' => 'estates' ],
        function () {
            Route::get('/', 'Front\EstatesController@index')->name('estates');
            Route::get('topi', 'Front\EstatesController@topi')->name('topi');
            Route::get('siampalm', 'Front\EstatesController@siampalm')->name('siampalm');
            Route::get('akapojana', 'Front\EstatesController@akapojana')->name('akapojana');
            Route::get('nanua', 'Front\EstatesController@nanua')->name('nanua');
            Route::get('chauat', 'Front\EstatesController@chauat')->name('chauat');
            Route::get('pabon', 'Front\EstatesController@pabon')->name('pabon');
            Route::get('wannee', 'Front\EstatesController@wannee')->name('wannee');
            Route::get('lamthap', 'Front\EstatesController@lamthap')->name('lamthap');
            Route::get('cheanvanich', 'Front\EstatesController@cheanvanich')->name('cheanvanich');
            Route::get('khlongtom', 'Front\EstatesController@khlongtom')->name('khlongtom');
        }
    );
    
    Route::group( ['prefix' => 'products' ],
        function () {
    
            Route::get('/', 'Front\ProductsController@index')->name('products');
            Route::get('cpo-pko', 'Front\ProductsController@cpoPko')->name('cpo-pko');
            Route::get('seed-seedlings', 'Front\ProductsController@seedSeedlings')->name('seed-seedlings');
            Route::get('seeds', 'Front\ProductsController@seeds')->name('seeds');
            Route::get('seedlings', 'Front\ProductsController@seedlings')->name('seedlings');
            Route::get('biogas', 'Front\ProductsController@biogas')->name('biogas');
            Route::get('by-products', 'Front\ProductsController@byProducts')->name('by-products');
    
        }
    );
    
    Route::group( ['prefix' => 'investor-relations' ],
        function () {
        
            Route::get('/', 'Front\InvestorRelationsController@index')->name('investor-relations');
            Route::get('financial-information', 'Front\InvestorRelationsController@financialInformation')->name('financial-information');
            Route::get('shareholder', 'Front\InvestorRelationsController@shareholder')->name('shareholder');
            Route::get('publications', 'Front\InvestorRelationsController@publications')->name('publications');
        }
    
    );
    
    Route::group( ['prefix' => 'sustainability' ],
        function () {
            Route::get('/', 'Front\SustainabilityController@index')->name('sustainability');
            Route::get('social-sustainability', 'Front\SustainabilityController@social')->name('social');
            Route::get('environmental-sustainability', 'Front\SustainabilityController@environmental')->name('environmental');
            Route::get('economic-sustainability', 'Front\SustainabilityController@economic')->name('economic');
            Route::get('scientific-sustainability', 'Front\SustainabilityController@scientific')->name('scientific');
            Route::get('corporate-governance', 'Front\SustainabilityController@corporate')->name('corporate');
        }
    );
    
    Route::group( ['prefix' => 'media' ],
        function () {
            Route::get('/', 'Front\MediaController@index')->name('media');
            Route::get('news2008', 'Front\MediaController@news2008')->name('news2008');
            Route::get('news2007', 'Front\MediaController@news2007')->name('news2007');
            Route::get('news2006', 'Front\MediaController@news2006')->name('news2006');
        }
    );
    
    Route::group( ['prefix' => 'careers' ],
        function () {
            Route::get('/', 'Front\CareersController@index')->name('careers');
        }
    );
    
    Route::group( ['prefix' => 'contact-us' ],
        function () {
            Route::get('/', 'Front\ContactController@index')->name('contact');
            Route::get('whistleblowing', 'Front\ContactController@whistleblowing')->name('whistleblowing');
            Route::get('feedback-inquiries', 'Front\ContactController@feedbackInquiries')->name('feedback-inquiries');
        }
    );
    
    
    
    
    
    
    
    Route::get('company-awards', 'HomeController@companyAwards')->name('company-awards');
    Route::get('operation-map', 'HomeController@operationMap')->name('operation-map');
    
    Route::get('projects', 'HomeController@projects')->name('projects');
    Route::get('project-details', 'HomeController@projectDetails')->name('project-details');
    
    Route::group(
        [
            'prefix' => 'admin',
            'middleware' => ['auth']
        ],
        function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
    
            Route::resource('regions', 'RegionController');
            Route::resource('status', 'ProjectStatusController');
            Route::resource('projects', 'ProjectController'); 
    
            Route::get('change-password', 'ChangePasswordController@index')->name('change.password');
            Route::post('change-password', 'ChangePasswordController@store')->name('post.change.password');
        }
    );

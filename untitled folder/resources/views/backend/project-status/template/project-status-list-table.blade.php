<div class="ibox float-e-margins">

    <div class="ibox-title">
        <div class="titleWrap">
            <h5>Project Status</h5>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="ibox-content">
                @if(\Session::has('success'))
                <div class="alert alert-success">
                    <ul class="list-style-none">
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
                @endif
                <form action="{{ route('status.store') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group @error('name') has-error @enderror">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" value="{{old('name')}}">
                                @error('name')
                                <div class="inline-errors">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="name" class="display-block">&nbsp;</label>
                                <input type="submit" class="btn btn-primary" value="Add">
                            </div>
                        </div>
                    </div>
                </form>
                <div class="clear"></div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox-content">
                @if(count($projectStatuses))
                <table class="table table-striped table-bordered table-hover tooltip-suggestion">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($projectStatuses as $key => $projectStatus)
                        <tr class="gradeX">
                            <td class="text-center">{!! ($key+1)+(20*($projectStatuses->currentPage()-1)) !!}</td>
                            <td>{{$projectStatus->name}}</td>
                            <td class="action-column tooltip-suggestion text-center">
                                <a class="btn btn-success btn-circle"
                                    href="{{ route('status.edit', $projectStatus->id) }}" data-toggle="tooltip"
                                    data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                <form id="DeleteProjectStatus-{{$projectStatus->id}}" method="POST"
                                    action="{{ route('status.destroy', $projectStatus->id) }}" class="action-form">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                    <a href="javascript:void(0)" onclick="deleteProjectStatus({{$projectStatus->id}})"
                                        class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top"
                                        title="Delete"><i class="fa fa-trash"></i></a>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                <div class="dataTables_paginate paging_simple_numbers">
                    {{$projectStatuses->links()}}
                </div>
                @else
                <div class="text-center">
                    <h4>No project status available</h4>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@extends('backend.layouts.master')
@section('title', 'Project Status')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Project Status</h2>
        <ol class="breadcrumb">            
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Project Status</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            @include('backend.project-status.template.project-status-list-table')
        </div>
    </div>
</div>

@endsection
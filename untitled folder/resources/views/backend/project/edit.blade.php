@extends('backend.layouts.master')
@section('title', 'Edit Project')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Edit Project</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('projects.index')}}">Project</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit Project</h5>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="ibox-content">
                            @if(\Session::has('success'))
                            <div class="alert alert-success">
                                <ul class="list-style-none">
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                            @endif
                            <form action="{{ route('projects.update', $project->id) }}" method="POST">
                                @csrf
                                {{ method_field('PUT') }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="region">Select Region *</label>
                                            <div class="form-group @error('region_id') has-error @enderror">
                                                {!! Form::select('region_id', $regions , $project->region_id,
                                                array('class' =>
                                                'form-control selectpicker','data-live-search' =>
                                                'true','placeholder'=>'Select Region')
                                                ); !!}
                                                @error('region_id')
                                                <div class="inline-errors">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="region">Select Status *</label>
                                            <div class="form-group @error('status_id') has-error @enderror">
                                                {!! Form::select('status_id', $projectStatuses , $project->status_id,
                                                array('class' =>
                                                'form-control selectpicker','data-live-search' =>
                                                'true','placeholder'=>'Select Status')
                                                ); !!}
                                                @error('status_id')
                                                <div class="inline-errors">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @error('title') has-error @enderror">
                                            <label for="name">Title *</label>
                                            <input type="text" name="title" class="form-control"
                                                value="{{$project->title}}">
                                            @error('title')
                                            <div class="inline-errors">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Sub Title</label>
                                            <input type="text" name="sub_title" class="form-control"
                                                value="{{$project->sub_title}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Plant Type</label>
                                            <input type="text" name="plant_type" class="form-control"
                                                value="{{$project->plant_type}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Electricity Capacity</label>
                                            <input type="text" name="plant_type" class="form-control"
                                                value="{{$project->plant_type}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Location</label>
                                            <input type="text" name="location" class="form-control"
                                                value="{{$project->location}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="name" class="display-block">&nbsp;</label>
                                            <input type="submit" class="btn btn-primary" value="Update">
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="ibox-content">
                            @if(count($projects))
                            <table class="table table-striped table-bordered table-hover tooltip-suggestion">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Region</th>
                                        <th>Status</th>
                                        <th>Title</th>
                                        <th>Sub Title</th>
                                        <th>Plant Type</th>
                                        <th>Electricity Capacity</th>
                                        <th>Location</th>
                                        <th class="text-center ProjectActionTh">Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($projects as $key=>$project)
                                    <tr class="gradeX">
                                        <td class="text-center">{!! ($key+1)+(20*($projects->currentPage()-1)) !!}</td>
                                        <td>{{$project->region->name}}</td>
                                        <td>{{$project->status->name}}</td>
                                        <td>{{$project->title}}</td>
                                        <td>{{$project->sub_title}}</td>
                                        <td>{{$project->plant_type}}</td>
                                        <td>{{$project->electricity_capacity}}</td>
                                        <td>{{$project->location}}</td>
                                        <td class="action-column tooltip-suggestion">
                                            <a class="btn btn-success btn-circle"
                                                href="{{ route('projects.edit', $project->id) }}" data-toggle="tooltip"
                                                data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                            <form id="DeleteProject-{{$project->id}}" method="POST"
                                                action="{{ route('projects.destroy', $project->id) }}"
                                                class="action-form">
                                                @csrf
                                                {{ method_field('DELETE') }}

                                                <a href="javascript:void(0)" onclick="deleteProject({{$project->id}})"
                                                    class="btn btn-danger btn-circle" data-toggle="tooltip"
                                                    data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                            <div class="dataTables_paginate paging_simple_numbers">
                                {{$projects->links()}}
                            </div>
                            @else
                            <div class="text-center">
                                <h4>No project available</h4>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

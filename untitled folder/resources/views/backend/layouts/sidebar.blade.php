@php
$route_name = Route::currentRouteName();
@endphp
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element text-center">
                    <img alt="image" class="rounded-circle" src="{{ asset('images/profile_small.png') }}">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold">Jhon Doe</span>
                        <span class="text-muted m-t-xs text-xs block">Adminstrator <b class="caret"></b></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item"
                                href="{{ route('change.password', app()->getLocale())}}">{{__('Change Password')}}</a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{__('Logout')}}</a>
                        </li>


                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </ul>
                </div>
                <div class="logo-element">
                    +
                </div>
            </li>
            <li class="@if($route_name=='dashboard') active @endif">
                <a href=" {{ route('dashboard') }}"><i class="fa fa-th-large"></i> <span
                        class="nav-label">Dashboard</span></a>
            </li>         
            <li class="@if($route_name=='regions.index' || $route_name=='regions.edit' || $route_name=='status.index' ||  $route_name=='status.edit' || $route_name=='projects.index' ||  $route_name=='projects.edit') active @endif">
                <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Projects</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="{{ route('regions.index') }}"><span
                                class="nav-label">Region</span></a>
                    </li class="">
                    <li>
                        <a href="{{ route('status.index') }}"><span class="nav-label">Project Status</span> </a>
                    </li>
                    <li>
                        <a href="{{ route('projects.index') }}"><span class="nav-label">Add Project</span> </a>
                    </li>
                </ul>
            </li>           
        </ul>

    </div>
</nav>

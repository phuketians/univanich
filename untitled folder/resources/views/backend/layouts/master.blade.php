<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Univanich || @yield('title')</title>

    <!--favicon icon-->
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans:400,600&display=swap"
        rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('backend/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
</head>

<body>
<div id="wrapper">
    @include('backend.layouts.sidebar')

    <div id="page-wrapper" class="gray-bg">

        @include('backend.layouts.header')

        @yield('content')

        @include('backend.layouts.footer')
    </div>

</div>

<script src="{{asset('backend/js/app.js')}}"></script>

</body>
</html>

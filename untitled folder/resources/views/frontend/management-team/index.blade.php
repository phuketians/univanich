@extends('frontend.layouts.master')
@section('title', 'Management Team')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Management Team",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'management-team'
                        ]
                    ]
            ]
        )


<section class="team-two-section ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/team/john.jpg') }} " alt="Mr. John Clendon" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr. John Clendon</h5>
                                <span>Managing Director</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/management-team/john-clendon" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr. John Clendon</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/team/harry.jpg') }} " alt="Mr. Harry Brock" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr. Harry Brock</h5>
                                <span>General Manager</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/management-team/harry-brock" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr. Harry Brock</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/team/palat.jpg') }} " alt="Dr. Palat Tittinutchanon" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Dr. Palat Tittinutchanon</h5>
                                <span>Plantations Director, <br>Head of Oil Palm Research</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/management-team/palat-tittinutchanon" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Dr. Palat Tittinutchanon</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/team/nattapong.jpg') }} " alt="Mr. Nattapong Dachanabhirom" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr. Nattapong Dachanabhirom</h5>
                                <span>Chief Financial Officer</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/management-team/nattapong-dachanabhirom" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr. Nattapong Dachanabhirom</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/team/phiphit.jpg') }} " alt="Mr. Phiphit Khlaisombat" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr. Phiphit Khlaisombat</h5>
                                <span>Chief Engineer</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/management-team/phiphit-khlaisombat" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr. Phiphit Khlaisombat</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/team/santi.jpg') }} " alt="Mr. Santi Suanyot" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr. Santi Suanyot</h5>
                                <span>Senior Plantations Manager</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/management-team/santi-suanyot" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr. Santi Suanyot</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/team/thanaphol.jpg') }} " alt="Mr.Thanaphol Leelangamwong" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr.Thanaphol Leelangamwong</h5>
                                <span>Senior Commercial Manager</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/management-team/thanaphol-leelangamwong" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr.Thanaphol Leelangamwong</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
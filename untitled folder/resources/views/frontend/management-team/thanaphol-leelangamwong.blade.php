@extends('frontend.layouts.master')
@section('title', 'Thanaphol Leelangamwong')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Thanaphol Leelangamwong",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Management Team',
                            'url' => 'management-team'
                        ],

                        [
                            'item_title' => 'Thanaphol Leelangamwong'
                        ],
                    ]
            ]
        )
        
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="{{ asset('images/team/team-7.jpg') }} " alt="board of directors member" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">Mr. Thanaphol Leelangamwong</h4>
                                <span>Director</span>
                            </div>
                            <div class="text-content mt-20">
                                <p></p><strong>Age:</strong> 48</p>
                                <strong>Education:</strong>
                                <p>Bachelor of Arts ( Business English ) from Assumption University (ABAC).</p>
                                <strong>Experience:</strong>
                                <p>Prior to joining Univanich, Thanaphol Leelangamwong had 23 years experience in agricultural products having worked in Thailand’s leading rice exporting company as Deputy General Manager (Cambodia), Rice Purchasing Manager, and Foreign Trade Manager. He joined the company in July 2016 as Senior Commercial Manager responsible for fruit purchasing operations in Thailand and marketing operations throughout the Univanich Group.</p>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->
    
@endsection
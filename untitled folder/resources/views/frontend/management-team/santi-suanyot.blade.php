@extends('frontend.layouts.master')
@section('title', 'Santi Suanyot')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Santi Suanyot",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Management Team',
                            'url' => 'management-team'
                        ],

                        [
                            'item_title' => 'Santi Suanyot'
                        ],
                    ]
            ]
        )
        
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="{{ asset('images/team/team-6.jpg') }} " alt="board of directors member" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">Mr. Santi Suanyot</h4>
                                <span>Director</span>
                            </div>
                            <div class="text-content mt-20">
                                <p></p><strong>Age:</strong> 57</p>
                                <strong>Education:</strong>
                                <p>Bachelor of Science (Agriculture) from Chiang Mai University in Thailand.</p>
                                <strong>Experience:</strong>
                                <p>Prior to joining Univanich, as an Estate Manager in 1993, Santi Suanyot had been employed in the government’s Horticulture Research Centre and as an assistant estate manager elsewhere in the oil palm industry. Since 1993 he has managed several Univanich estates and in 2006 was appointed as the company’s District Manager in LamThap District, Krabi Province and in Cha Uat District, Nakhon Sri Thammarat Province. In January 2016 he was promoted to be Senior Plantations Manager with responsibility for management of all Univanich’s plantations in Thailand.</p>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->
    
@endsection
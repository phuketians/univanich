@extends('frontend.layouts.master')
@section('title', 'Project Details')
@section('content')
<section class="hero-section ptb-100 gradient-overlay" style="background: url('img/header-bg-5.jpg')no-repeat center center / cover">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-7">
                <div class="page-header-content text-white text-center pt-sm-5 pt-md-5 pt-lg-0">
                    <h1 class="text-white mb-0">Domestic Projects</h1>
                    <div class="custom-breadcrumb">
                        <ol class="breadcrumb d-inline-block bg-transparent list-inline py-0">
                            <li class="list-inline-item breadcrumb-item">
                                <a href="home.html">Home</a>
                            </li>
                            <li class="list-inline-item breadcrumb-item">Power Plants</li>
                            <li class="list-inline-item breadcrumb-item active">Domestic Projects</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--header section end-->

<!--project details section start-->
<section class="project-details-section ptb-100">
    <div class="container">
        <div class="row justify-content-between">

            <div class="col-md-12">
                <div class="btn-back text-right">
                    <a href="{{ route('projects') }}">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                </div>
            </div>

            <div class="col-md-12 col-lg-8">
                <div class="img-wrap mb-md-4 mb-lg-0">
                    <img src="img/hero-bg7.jpg" alt="project" class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div id="project-detail" class="col-md-12 col-lg-4">
                <!--all services list-->
                <aside class="widget widget-categories">
                    <div class="widget-title col-lg-12">
                        <h4>Progress Interchem (Thailand) Co., Ltd.</h4>
                        <span class="media-object"><i class="fa fa-radiation-alt" aria-hidden="true"></i>
                            Project In Operation</span>
                    </div>
                    <div class="col-lg-12">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td><strong>Plant Type</strong></td>
                                    <td>Waste to Energy</td>
                                </tr>
                                <tr>
                                    <td><strong>Electricity Capacity </strong></td>
                                    <td>5 MW</td>
                                </tr>
                                <tr>
                                    <td><strong>Location </strong></td>
                                    <td>Ratchaburi</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>                          
                </aside>
            </div>
        </div>

        <!--project details row start-->
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="project-details-content">
                    <h5>Project Description</h5>
                    <p>Synergistically syndicate frictionless architectures via global e-services. Assertively
                        pontificate adaptive e-tailers rather than cross-unit results. Assertively engineer top-line
                        portals through one-to-one growth strategies. Efficiently.</p>
                    <p>Proactively reinvent standards compliant applications before timely ROI. Uniquely negotiate
                        installed base results rather than resource-leveling e-markets. Continually plagiarize
                        magnetic technologies vis-a-vis synergistic infomediaries. Globally communicate progressive
                        users without resource maximizing growth strategies. Objectively evolve enterprise.</p>
                    <p>Collaboratively conceptualize flexible best practices via cooperative methodologies. Assertively negotiate an expanded array of alignments with 24/365 "outside the box" thinking. Assertively enable fully researched vortals rather than alternative niche markets.</p>
                </div>
            </div>
        </div>                
        <!--project details row end-->
    </div>
</section>

@endsection
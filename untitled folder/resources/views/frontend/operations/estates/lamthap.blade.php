@extends('frontend.layouts.master')
@section('title', 'Lamthap')
@section('content')


@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Lamthap",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],
                        
                        [
                            'item_title' => 'Operations',
                            'url' => 'operations'
                        ],

                        [
                            'item_title' => 'Estates',
                            'url' => 'estates'
                        ],

                        [
                            'item_title' => 'Lamthap'
                        ],
                    ]
            ]
        )
        
<!--services details start-->
    <section class="service-details-section ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="service-details-wrap">
                        <img src="/images/estates/est4.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="sidebar-right pl-4">
                        <!--Factory Details-->
                        <aside class="widget widget-categories">
                            <div class="widget-title">
                                <h5>LAMTHAP</h5>
                            </div>
                            <ul class="primary-list mt-25">

                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Perm. Workers: </strong>2</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Cont. Workers: </strong>33</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Total Workers: </strong>35</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Total Land (Ha): </strong>88.29  </li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Mature (Ha): </strong>88.29  </li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Mature (%): </strong>100%  </li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Immature (%): </strong>0%  </li>
                            </ul>
                        </aside>
                        <div class="action-btns mt-4">
                            <a href="/operation-map" class="btn btn primary-solid-btn mr-1">Back to Operation Map</a>
                            <a href="/estates/chauat" class="btn btn primary-solid-btn mr-2">Next</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--services details end-->
    
@endsection
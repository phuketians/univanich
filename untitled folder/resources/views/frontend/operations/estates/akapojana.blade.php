@extends('frontend.layouts.master')
@section('title', 'Akapojana')
@section('content')


@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Akapojana",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

			[
                            'item_title' => 'Operations',
                            'url' => 'operations'
                        ],

                        [
                            'item_title' => 'Estates',
                            'url' => 'estates'
                        ],

                        [
                            'item_title' => 'Akapojana'
                        ],
                    ]
            ]
        )
        
<!--services details start-->
    <section class="service-details-section ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <div class="service-details-wrap">
                        <img src="/images//estates/est8.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5">
                    <div class="sidebar-right pl-4">
                        <!--Factory Details-->
                        <aside class="widget widget-categories">
                            <div class="widget-title">
                                <h5>AKAPOJANA</h5>
                            </div>
                            <ul class="primary-list mt-25">

                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Perm. Workers: </strong>16</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Cont. Workers: </strong>79</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Total Workers: </strong>95</li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Total Land (Ha): </strong>836.10 </li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Mature (Ha): </strong>836.10  </li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Mature (%): </strong>100%  </li>
                                <li><span class="ti-check-box mr-2 color-primary"></span><strong>Immature (%): </strong>0%  </li>
                            </ul>
                        </aside>
                        <div class="action-btns mt-4">
                            <a href="/operation-map" class="btn btn primary-solid-btn mr-1">Back to Operation Map</a>
                            <a href="/estates/siampalm" class="btn btn primary-solid-btn mr-2">Next</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--services details end-->
    
@endsection
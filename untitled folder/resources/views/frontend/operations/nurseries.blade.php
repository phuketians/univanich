@extends('frontend.layouts.master')
@section('title', 'Nurseries')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Nurseries",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Operations',
                            'url' => 'operations'
                        ],

                        [
                            'item_title' => 'Nurseries'
                        ],
                    ]
            ]
        )

<section class="col_wrap ptb-100" style="min-height: 910px">
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <img src="{{ asset('images/nurseries/04.jpg') }} " alt="Univanich Nurseries" class="img-fluid rounded shadow-sm mb-4">
            </div>
            <div class="col-md-12 col-lg-5">
                        <div class="about-img-wrap">
                            <img src="/images/nurseries/09.jpg" alt="organic planting image" class="img-fluid rounded shadow-sm">
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-7">
                        <div class="about-content-right mb-md-4 mb-lg-0">
                            <h2>Nurseries</h2>
                            <p>Univanich owns a total of 5 estates within Thailand, located in Krabi, Phang Nga, Nakhon
                                Si Thammarat and Phatthalung province. The company has been providing Univanich Palm
                                Seedlings to smallholder farmers since 1985, with an average of 1.5 million seedlings
                                per year.
                                Besides commercial sales, a portion of our seedlings from our nurseries are brought to
                                be replanted within the company’s estates every year.
                                Our nurseries offer 2 different types of seedlings for sale.</p>
                            <div class="action-btns mt-4">
                                <a href="/products/seedlings" class="btn primary-solid-btn mr-2">Seedlings</a>
                                <a href="/operation-map" class="btn primary-solid-btn mr-2">Operation Map</a>
                            </div>
                            
                            <div class="feature-tabs-wrap">
                                <ul class="nav nav-tabs mb-4 border-bottom-0 feature-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center active" href="#feature-tab-1"
                                            data-toggle="tab">
                                            <h6 class="mb-0">Krabi</h6>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" href="#feature-tab-2"
                                            data-toggle="tab">
                                            <h6 class="mb-0">Phang Nga</h6>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" href="#feature-tab-3"
                                            data-toggle="tab">
                                            <h6 class="mb-0">Nakhon Si Thammarat</h6>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" href="#feature-tab-4"
                                            data-toggle="tab">
                                            <h6 class="mb-0">Phatthalung</h6>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content feature-tab-content">
                                    <div class="tab-pane active" id="feature-tab-1">
                                        <aside class="widget widget-categories">
                                            <div class="widget-title">
                                                <h5>Krabi Nursery</h5>
                                            </div>
                                            <p><strong>Aoluk Branch</strong></p>
                                            <ul class="primary-list mt-25">
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-map-marker-alt mr-2"></span>592 Aoluk-Prasaeng
                                                    Road, Tambol Plaipraya, Plaipraya District, Krabi 81160</li>
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-phone-alt mr-2"></span> Tel: 075-634-634 Ext. 233
                                                </li>
                                            </ul>
                                            <p><strong>Lamthap Branch</strong></p>
                                            <ul class="primary-list mt-25">
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-map-marker-alt mr-2"></span> 142 Moo 1 Tambol
                                                    Thung Sai Thong, Lamthap District, Krabi 81120</li>
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-phone-alt mr-2"></span> Tel: 086-470-2729 Ext. 222
                                                </li>
                                            </ul>
                                        </aside>
                                    </div>
                                    <div class="tab-pane" id="feature-tab-2">
                                        <aside class="widget widget-categories">
                                            <div class="widget-title">
                                                <h5>Phang Nga Nursery</h5>
                                            </div>
                                            <p><strong>Khura Buri</strong></p>
                                            <ul class="primary-list mt-25">
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-map-marker-alt mr-2"></span>Bangwan Nursery: 355
                                                    Moo 3 Tambol
                                                    Bangwan, Kuraburi District, Pang-nga 82150</li>
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-phone-alt mr-2"></span> Tel: 076-670-271</li>
                                            </ul>
                                        </aside>
                                    </div>
                                    <div class="tab-pane" id="feature-tab-3">
                                        <aside class="widget widget-categories">
                                            <div class="widget-title">
                                                <h5>Nakhon Si Thammarat Nursery</h5>
                                            </div>
                                            <p><strong>Cha Uat Branch</strong></p>
                                            <ul class="primary-list mt-25">
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-map-marker-alt mr-2"></span>Cha-Uat Nursery: 173/2
                                                    Tambol
                                                    Thapaja, Cha-Uat, Nakhon Sri Thammarat 80180</li>
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-phone-alt mr-2"></span> Tel: 075-380-754,
                                                    Facsimile: 075-380-754 Ext.100</li>
                                            </ul>
                                        </aside>
                                    </div>
                                    <div class="tab-pane" id="feature-tab-4">
                                        <aside class="widget widget-categories">
                                            <div class="widget-title">
                                                <h5>Phatthalung Nursery</h5>
                                            </div>
                                            <p><strong>Pabon Branch</strong></p>
                                            <ul class="primary-list mt-25">
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-map-marker-alt mr-2"></span>659 Moo 8 Tambol
                                                    Pabon, Pabon District, Phatthalung 93170</li>
                                                <li class="d-flex align-items-center py-2"><span
                                                        class="fas fa-phone-alt mr-2"></span> Tel: 074-820-371 Ext. 222
                                                </li>
                                            </ul>
                                        </aside>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap -->

@endsection



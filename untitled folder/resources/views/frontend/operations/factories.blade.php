@extends('frontend.layouts.master')
@section('title', 'Factories and Biogas')
@section('content')


@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Factories and Biogas",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Operations',
                            'url' => 'operations'
                        ],

                        [
                            'item_title' => 'Factories and Biogas'
                        ],
                    ]
            ]
        )
        
<section class="col_wrap ptb-100" style="min-height: 830px">
    <div class="container">
        <div class="row">   
            <div class="col-md-12">                            
                <table class="table responsive table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>    
                            <th>Capacity (T/hr)</th>
                            <th>Steam/electricity</th>   
                            <th>Commission Date</th> 
                            <th>Location</th>
                            <th># Employees</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>TOPI</td>    
                            <td>60 FFB 190 PK</td>  
                            <td>952 kW x 3 = 2.856 MW</td>   
                            <td>1974</td>    
                            <td>Krabi Province</td>  
                            <td>128</td>
                        </tr>
                        <tr>
                            <td>Siam</td>    
                            <td>45 FFB</td>  
                            <td>952 kW x 3 = 2.856 MW</td>   
                            <td>1980 </td>    
                            <td>Krabi Province </td>  
                            <td>69</td>
                        </tr>
                        <tr>
                            <td>Lamthap</td>    
                            <td>45 FFB 155 PK</td>  
                            <td>Biogas 952 kW x 2 = 1.904 MW</td>   
                            <td>2004 </td>    
                            <td>Krabi Province</td>  
                            <td>92</td>
                        </tr>
                        <tr>
                            <td>CVP</td>    
                            <td>60 FFB</td>  
                            <td>Biogas 2.828 MW under BOOT by TBEC</td>   
                            <td>2013 </td>    
                            <td>Phang Nga</td>  
                            <td>66</td>
                        </tr> 
                        <tr>
                            <td>Pabon</td>    
                            <td>30 FFB</td>  
                            <td></td>   
                            <td>2017 </td>    
                            <td>Phatthalung</td>  
                            <td>46</td>
                        </tr> 
                        <tr>
                            <td>UCPOC</td>    
                            <td>30/60 </td>  
                            <td></td>   
                            <td></td>    
                            <td>Philippines</td>  
                            <td></td>
                        </tr> 
                    </tbody>
                </table>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-6">
                <img src=" {{ asset('images/biogas/02.jpg') }} " alt="Biogas" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <p>All byproducts of our factories do not go to waste. One important byproduct of crushing mills is the
                    wastewater- also known as POME (Palm Oil Mill Effluent). POME is moisture extracted from palm
                    bunches and is filled with nutrients, with a high biological oxygen demand which can be harmful to
                    local ecosystems if discharged untreated. Furthermore, POME generates and emits methane, a powerful
                    greenhouse gas that damages the atmosphere. To solve this problem, we congregate our POME into a
                    biogas reactor, where bacteria anaerobically digests POME, generating large amounts of methane in
                    the process that we can use to generate electricity through a gas engine. POME that is treated
                    through our biogas reactors have their BOD reduced substantially, up to 5 times its initial size and
                    down to safe levels for the environment.</p>
                <p>The methane we generate goes to a gas engine which generates up to 2 MW of electricity per biogas
                    reactor, which we utilize as a backup generator for our operations, as well as selling the surplus
                    back to the national grid.</p>

                <p style="margin-bottom: 0">In doing so, our environmental objectives have been achieved:</p>
                <ul class="ml-5">
                    <li>1. A large reduction in Greenhouse Gas Emissions</li>
                    <li>2. The renewable energy (biogas) source has replaced fossil fuels</li>
                    <li>3. Wastewater treatment has improved</li>
                    <li>4. Factory efficiencies have improved</li>
                    <li>5. New skilled jobs have been created in rural communities</li><br>
                </ul>
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a href="/estates/topi">TOPI</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a href="/estates/siampalm">SIAM PALM</a></li>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a href="/estates/lamthap">LAMTHAP</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span><a href="/estates/pabon">PABON</a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap --> 


                                    
                
                        
@endsection
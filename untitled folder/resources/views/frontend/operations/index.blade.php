@extends('frontend.layouts.master')
@section('title', 'Investor Relations')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Operations",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Operations'
                        ]
                    ]
            ]
        )


<section class="our-portfolio-section ptb-100">
    <div class="container">
    Operations
    </div>
</section>

@endsection
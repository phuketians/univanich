@extends('frontend.layouts.master')
@section('title', 'Operation Map')
@section('content')
<section class="PowerPlantWrap power-plants-map-wrapper">


   <div class="map" id="operation_map"> 

         <iframe src="https://www.google.com/maps/d/u/1/embed?mid=1AqStJJ5HbXNjI16iInX03wDs8pXEPLcd" width="100%" allowfullscreen height="920"></iframe>
   </div>

    <div class="container">
       <div class="row justify-content-end">
            <div class="col-md-2 plant-lists">
                  <ul>
                     <li>
                        <a class="active map-listing" id="Estates" href="#Estates">Estates<span><img src=" {{ asset('images/estate.png' ) }} "></span></a>
                     </li>
                     <li>
                        <a class="map-listing" id="Nurseries" href="#Nurseries">Nurseries<span><img src=" {{ asset('images/nursery.png' ) }} "></span></a>
                     </li>
                     <li>
                        <a class="map-listing" id="Factories" href="#Factories">Factories & Biogas<span><img src=" {{ asset('images/factories.png' ) }} "></span></a>
                     </li>                    
                  </ul>  
            </div>
       </div>
    </div>
</section>
@endsection
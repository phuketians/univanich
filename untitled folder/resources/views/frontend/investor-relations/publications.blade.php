@extends('frontend.layouts.master')
@section('title', 'Shareholder Information')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Publications",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Investor Relations',
                            'url' => 'investor-relations'
                        ],

                        [
                            'item_title' => 'Publications'
                        ],
                    ]
            ]
        )
        
<!--counter section start-->
    <section>
        <div class="container">
            <div class="row justify-content-center">
                
                <div class="col-md-8 col-lg-8">
                    <div class="call-to-action-content text-white text-center mb-4">
                        <h2 class="text-white mb-1">ANNUAL REPORT</h2>
                    </div>
                </div>
            </div>
            <div class="row ptb-100">
                <div class="col-md-12 col-lg-12">
                    <h2><center>Shareholder Meeting</center></h2><br><br>
                    <h5><center>Minutes of Annual General Meeting of Shareholders No. 51<br><br>
                        <a href="/publications/20201111-uvan-agm2020-minutes-en.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a></center></h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2019.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2019</h5>
                        <a href="/publications/20200515-uvan-ar2019.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2018.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2018</h5>
                        <a href="/publications/20190404-uvan-ar2018.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2017.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2017</h5>
                        <a href="/publications/20180409-uvan-ar2017-02.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2016.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2016</h5>
                        <a href="/publications/20170410-uvan-ar2016.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2015.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2015</h5>
                        <a href="/publications/20160407-uvan-ar2015.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2014.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2014</h5>
                        <a href="/publications/20150416-uvan-ar2014.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2013.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2013</h5>
                        <a href="/publications/20140407-UVAN-AR2013.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2012.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2012</h5>
                        <a href="/publications/20130507-UVAN-AR2012.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2011.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2011</h5>
                        <a href="/publications/20120417-UVAN-AR2011.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2010.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2010</h5>
                        <a href="/publications/ar2010.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2009.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2009</h5>
                        <a href="/publications/ar2009en.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2008.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2008</h5>
                        <a href="/publications/ar08.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2007.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2007</h5>
                        <a href="/publications/ar07.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2006.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2006</h5>
                        <a href="/publications/ar06_en.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="single-counter rounded p-4 text-center text-white">
                        <img src="/images/ar2005.jpg" alt="services" class="img-fluid rounded shadow-sm"/>
                        <h5 class="mb-0">Annual Report - 2005</h5>
                        <a href="/publications/UVAN_AR2005.pdf" target="_blank"><span class="pl-4"></span>Download | View Report</span></a>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <center><h2 class="mb-0">Brochure</h2><br>
                <a href="/publications/seedbr.pdf" target="_blank"><span class="pl-4"></span>Download | View Brochure</span></a></center><br><br>
            </div>
            <div class="col-md-12 col-lg-6">
                <center><h2 class="mb-0">Fact Sheet</h2><br>
                <a href="/publications/fact-sheet.pdf" target="_blank"><span class="pl-4"></span>Download | View Fact Sheet</span></a></center><br><br>
            </div>
        </div>
    </div>
</section>
<!--counter section end-->

@endsection

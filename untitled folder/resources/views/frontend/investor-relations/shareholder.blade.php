@extends('frontend.layouts.master')
@section('title', 'Shareholder Information')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Shareholder Information",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Investor Relations',
                            'url' => 'investor-relations'
                        ],

                        [
                            'item_title' => 'Shareholder Information'
                        ],
                    ]
            ]
        )
<section class="col_wrap ptb-100" style="min-height: 630px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Shareholdings</h3>

                <p>Shareholding Structure The first 10 major shareholders as at 31st December 2019</p>

                <table class="table responsive table-hover mb-4">
                    <thead>
                        <tr>
                            <th class="text-center"></th>
                            <th>Shareholders</th>
                            <th>Number Of Shares</th>
                            <th>Percent</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">1.</td>
                            <td>Mr. Apirag Vanich</td>
                            <td>175,910,420</td>
                            <td>18.71</td>
                        </tr>
                        <tr>
                            <td class="text-center">2.</td>
                            <td>Mrs. Chantip Vanich</td>
                            <td>91,116,300</td>
                            <td>9.69</td>
                        </tr>
                        <tr>
                            <td class="text-center">3.</td>
                            <td>UBS AG SINGAPORE BRANCH</td>
                            <td>57,300,000</td>
                            <td>6.09</td>
                        </tr>
                        <tr>
                            <td class="text-center">4.</td>
                            <td>Chean Vanich Co.,Ltd.</td>
                            <td>55,966,620</td>
                            <td>5.95</td>
                        </tr>
                        <tr>
                            <td class="text-center">5.</td>
                            <td>Mrs. Nuanchan Ratanakulsereeroengrith</td>
                            <td>45,370,900</td>
                            <td>4.83</td>
                        </tr>
                        <tr>
                            <td class="text-center">6.</td>
                            <td>Mr. Narut Chitrudiamphai</td>
                            <td>27,100,320</td>
                            <td>2.88</td>
                        </tr>
                        <tr>
                            <td class="text-center">7.</td>
                            <td>Bangkok Life Assurance Co., Ltd.</td>
                            <td>26,868,500</td>
                            <td>2.86</td>
                        </tr>
                        <tr>
                            <td class="text-center">8.</td>
                            <td>Ms. Aungkhana Vanich</td>
                            <td>23,634,990</td>
                            <td>2.51</td>
                        </tr>
                        <tr>
                            <td class="text-center">9.</td>
                            <td>Ms. Rojana Vanich</td>
                            <td>23,294,820</td>
                            <td>2.48</td>
                        </tr>
                        <tr>
                            <td class="text-center">10.</td>
                            <td>Ms. Oranutch Vanich</td>
                            <td>22,554,980</td>
                            <td>2.40</td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /.col-md-12 -->

            <div class="col-md-12">
                <h3>Dividend Policy</h3>

                <p>The Company has a policy to pay cash dividends of not lessthan 50% of its net profit after taxation.
                    However, whether the Company will pay a dividend and the amount thereof will depend, amongst other
                    things, on the Company’s financial condition and future plans.</p>

                <div class="text-right">
                    <p>Unit: Baht</p>
                </div>

                <table class="table responsive table-hover mb-4">
                    <thead>
                        <tr>
                            <th>Board Date</th>
                            <th>X-Date</th>
                            <th>Payment Date</th>
                            <th>Type</th>
                            <th>Dividend (Per Share)</th>
                            <th>Operation Period</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>10/04/2020</td>
                            <td>12/03/2020</td>
                            <td>08/05/2020</td>
                            <td>Cash Dividend</td>
                            <td>0.17</td>
                            <td>01/01/2019-31/12/2019</td>
                        </tr>
                        <tr>
                            <td>08/08/2019</td>
                            <td>21/08/2019</td>
                            <td>06/09/2019</td>
                            <td>Cash Dividend</td>
                            <td>0.17</td>
                            <td>01/01/2019-30/06/2019</td>
                        </tr>
                        <tr>
                            <td>22/02/2019</td>
                            <td>07/03/2019</td>
                            <td>24/05/2019</td>
                            <td>Cash Dividend</td>
                            <td>0.23</td>
                            <td>01/01/2018-31/12/2018</td>
                        </tr>
                        <tr>
                            <td>14/08/2018</td>
                            <td>27/08/2018</td>
                            <td>13/09/2018</td>
                            <td>Cash Dividend</td>
                            <td>0.17</td>
                            <td>01/01/2018-30/06/2018</td>
                        </tr>
                        <tr>
                            <td>23/02/2018</td>
                            <td>08/03/2018</td>
                            <td>25/05/2018</td>
                            <td>Cash Dividend</td>
                            <td>0.33</td>
                            <td>01/01/2017-31/12/2017</td>
                        </tr>
                        <tr>
                            <td>11/08/2017</td>
                            <td>23/08/2017</td>
                            <td>08/09/2017</td>
                            <td>Cash Dividend</td>
                            <td>0.17</td>
                            <td>01/01/2017-30/06/2017</td>
                        </tr>
                        <tr>
                            <td>24/02/2017</td>
                            <td>08/03/2017</td>
                            <td>26/05/2017</td>
                            <td>Cash Dividend</td>
                            <td>0.23</td>
                            <td>01/01/2016-31/12/2016</td>
                        </tr>
                        <tr>
                            <td>05/08/2016</td>
                            <td>17/08/2016</td>
                            <td>02/09/2016</td>
                            <td>Cash Dividend</td>
                            <td>0.17</td>
                            <td>01/01/2016-30/06/2016</td>
                        </tr>
                        <tr>
                            <td>26/02/2016</td>
                            <td>11/03/2016</td>
                            <td>26/05/2016</td>
                            <td>Cash Dividend</td>
                            <td>0.23</td>
                            <td>01/01/2015-31/12/2015</td>
                        </tr>
                        <tr>
                            <td>14/08/2015</td>
                            <td>26/08/2015</td>
                            <td>14/09/2015</td>
                            <td>Cash Dividend</td>
                            <td>0.17</td>
                            <td>01/01/2015-30/06/2015</td>
                        </tr>
                        <tr>
                            <td>27/02/2015</td>
                            <td>12/03/2015</td>
                            <td>29/05/2015</td>
                            <td>Cash Dividend</td>
                            <td>0.30</td>
                            <td>01/07/2014-31/12/2014</td>
                        </tr>
                        <tr>
                            <td>14/08/2014</td>
                            <td>26/08/2014</td>
                            <td>12/09/2014</td>
                            <td>Cash Dividend</td>
                            <td>0.20</td>
                            <td>01/01/2014-30/06/2014</td>
                        </tr>
                        <tr>
                            <td>28/02/2014</td>
                            <td>13/03/2014</td>
                            <td>23/05/2014</td>
                            <td>Cash Dividend</td>
                            <td>0.35</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>13/08/2013</td>
                            <td>23/08/2013</td>
                            <td>12/09/2013</td>
                            <td>Cash Dividend</td>
                            <td>0.20</td>
                            <td>01/01/2013-30/06/2013</td>
                        </tr>
                        <tr>
                            <td>01/03/2013</td>
                            <td>13/03/2013</td>
                            <td>23/05/2013</td>
                            <td>Cash Dividend</td>
                            <td>5.00</td>
                            <td>01/07/2012-31/12/2012</td>
                        </tr>
                        <tr>
                            <td>10/08/2012</td>
                            <td>22/08/2012</td>
                            <td></td>
                            <td>Cash Dividend</td>
                            <td> 2.00</td>
                            <td>01/01/2012-30/06/2012</td>
                        </tr>
                        <tr>
                            <td>24/02/2012</td>
                            <td>12/03/2012</td>
                            <td></td>
                            <td>Cash Dividend</td>
                            <td>5.00</td>
                            <td>01/10/2011-31/12/2011</td>
                        </tr>
                        <tr>
                            <td>11/11/2011</td>
                            <td>23/11/2011</td>
                            <td></td>
                            <td>Cash Dividend</td>
                            <td>1.50</td>
                            <td>01/07/2011-30/09/2011</td>
                        </tr>
                        <tr>
                            <td>11/08/2011</td>
                            <td>23/08/2011</td>
                            <td></td>
                            <td>Cash Dividend</td>
                            <td>3.00</td>
                            <td>01/01/2011-30/06/2011</td>
                        </tr>
                        <tr>
                            <td>25/02/2011</td>
                            <td>10/03/2011</td>
                            <td></td>
                            <td>Cash Dividend</td>
                            <td>2.50</td>
                            <td>01/07/2010-31/12/2010</td>
                        </tr>
                        <tr>
                            <td>13/08/2010</td>
                            <td>26/08/2010</td>
                            <td></td>
                            <td>Cash Dividend</td>
                            <td>2.00</td>
                            <td>01/01/2010-30/06/2010</td>
                        </tr>
                        <tr>
                            <td>26/02/2010</td>
                            <td>12/03/2010</td>
                            <td>21/05/2010</td>
                            <td>Cash Dividend</td>
                            <td>2.50</td>
                            <td>01/07/2009-31/12/2009</td>
                        </tr>
                        <tr>
                            <td>14/08/2009</td>
                            <td>25/08/2009</td>
                            <td>11/09/2009</td>
                            <td>Cash Dividend</td>
                            <td>2.00</td>
                            <td>01/01/2009-30/06/2009</td>
                        </tr>
                        <tr>
                            <td>26/02/2009</td>
                            <td>12/03/2009</td>
                            <td>22/05/2009</td>
                            <td>Cash Dividend</td>
                            <td>4.00</td>
                            <td>01/10/2008-31/12/2008</td>
                        </tr>
                        <tr>
                            <td>14/11/2008</td>
                            <td>25/11/2008</td>
                            <td>12/12/2008</td>
                            <td>Cash Dividend</td>
                            <td>2.50</td>
                            <td>01/07/2008-30/09/2008</td>
                        </tr>
                        <tr>
                            <td>13/08/2008</td>
                            <td>22/08/2008</td>
                            <td></td>
                            <td>Cash Dividend</td>
                            <td>5.00 </td>
                            <td>01/01/2008-30/06/2008</td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap -->

@endsection

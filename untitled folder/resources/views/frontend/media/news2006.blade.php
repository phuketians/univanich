@extends('frontend.layouts.master')
@section('title', '2006-2004 News Highlights')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "2006-2004 News Highlights",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Media',
                            'url' => 'media'
                        ],

                        [
                            'item_title' => '2006-2004 News Highlights'
                        ],
                    ]
            ]
        )
<!--blog section start-->
    <section class="our-blog-section ptb-100 gray-light-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading mb-5">
                        <h2><center>UVAN News</center></h2>
                        <h5><center>
                            News Highlights 2006-2004</center></h5>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Palm Oil Exports to Europe</span>
                            <img src="/images/blog/cpo-export3.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">In 2006, Univanich has exported a record 110,000 tonnes of crude palm oils from the Krabi port of Laemphong. This included the first ever shipments of Thai palm oil for European markets, including this vessel loading 10,000 tonnes of Univanich oil for delivery to Rotterdam and Hamburg.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">International Society of Oil Palm Breeders visit Univanich Research Centre</span>
                            <img src="/images/blog/ltcigar.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">Over 60 oil palm breeders from around the world visited Thailand in November 2006 for the ISOPB International Conference. The delegates visited the Univanich Oil Palm Research Centre in Krabi, where the company’s hybrid progeny trials were discussed.</p>
                            <p>It was widely acknowledged that the breeding programme is producing some exceptionally promising material and that production methods in the Univanich seed production unit are to world-class standards.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Directors visit Univanich Oil Palm Research Centre</span>
                            <img src="/images/blog/directors-visit.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">The Strategic Planning Subcommittee of the Univanich Board visited the Company’s Oil Palm Research Centre to review research strategies and inspect the new Tissue Culture and seed production laboratories. Univanich now has 3,793 rai (607 hectares) of oil palm field trials, mainly focused on testing more than 400 new hybrid progeny from the company’s breeding programme.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Expanding Seed Exports to India</span>
                            <img src="/images/blog/exp-seed.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <p class="card-text">There has been increasing overseas interest in oil palm seeds from the Univanich breeding programme, especially from countries where there is an annual dry season. Univanich planting material has been selected for increased tolerance of this seasonal drought stress which occurs regularly in Thailand . Pictured above, oil palm growers inspect a nursery of Univanich seedlings in the Indian state of Andhra Pradesh where the company is now a leading supplier of high yielding hybrid seeds.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Univanich receives Best Performance Award for Agro-Food Sector</span>
                            <img src="/images/blog/set-award.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>Award for Univanich: John Clendon, managing director of Univanich Palm Oil Plc,accepts the best Performance Award for Agro-Industry and Food sector from Finance Minister Somkid Jatusripitak at the recent SET Awards ceremony.Mr Clendon was also a nominee for the Best CEO of the year award.</h5>
                            <p>At a glittering gala dinner for the 2005Thailand Business Leaders Awards, Univanich Palm Oil PCL took away the top award in the Agro-Food Sector and the company’s Managing Director, John Clendon, was one of eight nominees for Thailand’s CEO of the Year.</p>
                            <p>The annual event held in the Queen Sirikit National Convention Centre is organised by the Stock Exchange of Thailand and CNBC Television. In the first phase of judging a team from the University of Chicago Graduate School of Business (Chicago GSB) shortlisted companies based on four performance measures : Sales Growth, Operating Margin, Earnings before Interest (EBIT) Per Employee and Return on Capital Employed (ROCE)</p>
                            <p>The second phase of the judging involved face-to-face interviews with a team from Development Dimensions International (DDI), a global consulting company specialising in leadership development, DDI worked closely with Chicago GSB to evaluate the performance of all semi-finalists based on a set of key parameters.</p>
                           <img src="/images/blog/party2-1.jpg" class="card      img-top position-relative img-fluid" alt="blog">
                           <h5>Trophy and Certificates won by Univanich at the 2005 SET Awards</h5>
                           <img src="/images/blog/party1.jpg" class="card      img-top position-relative img-fluid" alt="blog">
                           <h5>Staff gathered at the Univanich Clubhouse on 28th July to celebrate The Company's success at the 2005 Awards</h5>
                        </div>
                    </div>
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <img src="/images/blog/apirak.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>On 29th December 2004 Univanich Palm Oil PCL donated Baht 1,000,000 to the emergency Tsunami relief fund in Krabi Province.</h5>
                            <p class="card-text">In the first announcement of annual results since Univanich was listed on the SET in November 2003, Chairman, Mr Apirag Vanich, noted that strong growth in Thailand's oil palm industry has assisted the Company in achieving several new growth milestones. "Thailand's production of palm fruit increased to 4.8 million tonnes in 2003 with most of the growth in the small farmer sector.This enabled Univanich to also increase its production since the Company purchased 76% of its fruit from outside growers " said Mr Vanich. "Due to this increasing production from our fruit suppliers Univanich factories reached the significant milestone of more than 500,000 tonnes fruit processed in one year, an increase of 18.6% over the prevous year. This is the maximum of the Company's present capacity and a new factory is already under construction for opening in the 4th quarter of 2004" he said.</p>
                            <img src="/images/blog/graph-uv.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p>Mr Apirag Vanich noted that Thailand has now moved up into thirdplace amongst the World's producers of palm oil, behind the giant producers of Malaysia and Indonesia. In 2003 Thailand produced more than 860,000 tonnes of Crude Palm Oil, of which 86,000 tonnes, or 10% of production was exported. "When our first oil palms were planted back in 1969 it was intended to supply just the local market for cooking oil" said Mr Vanich, "But we have now developed into a competitive export industry. In 2003 Univanich exported about half its palm oil production, or more than 50,000 tonnes, to new customers in India and in Malaysia" he said.</p>
                            <p>"We expect this exporting will continue since Thailand's palm oil production easily exceeds local consumption. Furthermore, there is still strong interest from farmers to plant oil palms. In 2003 Univanich's oil palm breeding programme supplied 1.6 million hybrid seeds and seedlings to local growers. This high yielding material was enough to plant another 60,000 rai which will come into production in 2 or 3 years time. We estimate that Univanich supplies about one third of the local market for high quality seeds so this indicates to us Thailand's oil palm industry is still expanding at a rapid rate" said Mr Vanich.</p>
                            <p>World consumption of palm oil is about 28 million tonnes per year and demand is increasing due to population growth and higher vegetable oil consumption in developing economies like India and China. This growing demand has helped to keep prices firm especially during 2003 - 2004 as world stocks are low. "We expect Thailand's farmers to receive good prices for their palm fruit again this year and this will encourage a further expansion of oil palm plantings" said Mr Vanich.</p>
                            <p>Univanich Palm Oil PCL (UVAN) announced 2003 sales of Baht 1,955 million, an increase of 23% over the previous year. The Company's net profit increased by 80% to reach Baht 400.2 million or earnings of Baht 4.76 per share.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">On 29th December 2004 Univanich Palm Oil PCL donated Baht 1,000,000 to the emergency Tsunami relief fund in Krabi Province.</span>
                            <img src="/images/blog/donated.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>Picture : Univanich’s Plantations Manager,Dr Palat Tittinutchanon hands the Company’s donation of Baht 1,000,000 to Krabi Governor Khun Anond Promnart</h5>
                            <p class="card-text">When presenting the Company’s donation to Krabi Governor Khun Anond Promnart, Univanich’s Plantations Manager, Dr Palat Tittinutchanon commented that the Company and its employees had been very fortunate to escape damage from the disaster which struck Krabi and neigbouring provinces on 26th December. The Company quickly recognised the need for urgent assistance in nearby communities and was pleased to contribute to the relief effort.</p>
                        </div>
                    </div>
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">THAI OIL PALM SEEDS FIND WORLD MARKETS</span>
                            <img src="/images/blog/nur-drcoley.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>Univanich Palm Oil PCL awarded ISO 9001 : 2000 for seed production</h5>
                            <p class="card-text">In announcing the ISO Certification the Company's Managing Director, John Clendon, commented that this was further recognition that Univanich has become a world class producer of high yielding oil palm seeds. He noted that the breeding programme had been a long term investment since the first pedigree palms were imported by the Company more than 30 years ago. During the 1980s more selected parent palms were imported in an exchange with Unilever's Plantations Group and other international breeding programmes.</p>
                            <p>Previously, local oil palm growers have had to import their hybrid seeds. But according to John Clendon, oil palms bred in other countries will not always be high yielding in Thailand's drier climate. "Only years of local selection trials over several generations of palms can give that assurance", he said.</p>
                            <p>The Univanich Research Department in Krabi Province has 1,453 rai planted in research trials and this area is increasing each year as new generations of hybrid palms are planted for yield testing. Sixty nine people are currently employed on this programme.</p>
                            <img src="/images/blog/lso.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <p>Head of Univanich R&D, Dr Palat Tittinachanon, announced that the Company produces five million hybrid seeds each year and this will increase to seven million seeds by 2007. "Our main priority is to supply high quality seedlings to Thailand's oil palm growers but we are also meeting growing demand from overseas customers", he said. The Company isnow exporting seeds to South America,Africa and to neighbouring Asian countries. Dr Palat said that the reputation of Univanich seeds in these new markets had been strengthened by the award of ISO 9001 : 2000 Certification.</p>
                        </div>
                    </div>
                    <div class="single-blog-card card border-0 shadow-sm">
                        <div class="blog-img">
                            <span class="category position-absolute">Univanich Palm Oil enters South American market for seed exports</span>
                            <img src="/images/blog/news-seedep1.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                        </div>
                        <div class="card-body">
                            <h5>Univanich 's Staff packing Univanich seeds for first export to Colombia</h5>
                            <p class="card-text">Univanich Palm Oil PCL announced that it has completed Thailand's first export of oil palm seeds to growers in Colombia. The consignment of 150,000 hybrid seeds was air freighted in January, with further orders received for shipment in February and March.</p>
                            <p>The Company's Managing Director, John Clendon, commented that obtaining export orders from South America is a breakthrough for Univanich. He noted that Colombia is South America's largest palm oil producing country and for Univanich to obtain seed orders in that competitive market is recognition that the Company's breeding programme is producing world class material.</p>
                            <img src="/images/blog/news-seedep2.jpg" class="card-img-top position-relative img-fluid" alt="blog">
                            <h5>Loading the first consignment of oil palm seeds  from Thailand to Colombia, airfreighted from Phuket on 29th January 2004</h5>
                            <p>Univanich imported its first selected breeding palms back in 1972 and since then has imported pedigree oil palms from breeding programmes around the world. John Clendon noted that some of the company's palm families can now be traced back several generations to selections made by Unilever in Congo and Cameroons more than 70 years ago. Univanich had acquired this valuable material through exchange with the international Plantations Group of Unilever PLC during the 1980s and 1990s, he said.</p>
                            <p>The Company's head of Research and Development, Dr Palat Tittinutchanon, commented that since Univanich seed has been specially developed for the Company's plantations in Southern Thailand, it has attracted interest from growers in Colombia and Brazil as well as Philippines, Cambodia and other countries which have a similar growing season to Southern Thailand. He reported that in 2003 the Company sold 1.9 million germinated seeds and 450,000 seedlings and is the leading domestic supplier of seeds and seedlings to Thailand's rapidly growing oil palm industry.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--blog section end-->

@endsection
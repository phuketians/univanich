@extends('frontend.layouts.master')
@section('title', 'Whistleblowing')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Whistleblowing",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Whistleblowing',
                            'url' => 'contact'
                        ],

                        [
                            'item_title' => 'Whistleblowing'
                        ],
                    ]
            ]
        )
<!--services details start-->
        <section class="service-details-section ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="service-details-wrap">
                            <div class="services-detail-content mt-4">
                                <h5>Policy Objective</h5>
                                <p>Univanich Palm Oil Public Company Limited (“the Company”) has established a Whistle
                                    Blowing Policy to support and provide opportunities for stakeholders. Internal and
                                    external sources are able to report clues or complaints about unethical behaviour
                                    related to the company. Occurrences such as corruption, violation of company
                                    regulations, unethical practices, or other illegal acts can be reported to the
                                    company committee to be investigated and solution implemented accordingly.
                                    Reporters with legitimate complaints are assured protection from the company and
                                    immune to external response measures.
                                </p>
                                <p>The objective of having a written and formal policy is to act as a precautionary
                                    measure to prevent and deter future premature misconducts, as well as supporting
                                    transparency, justice, and the promotion of a good working environment.</p>

                                <h5>Contact channel for whistleblowing or making complaints</h5>
                                <p>If the complainant notices or suspects any wrongdoing, the complainant can notify the
                                    committee via various channels as follows:</p>
                                <ul>
                                    <li>1. Notify their supervisor or other trusted supervisors</li>
                                    <li>2. By mail: to Audit Committee</li>
                                    <ul>
                                        <li>Univanich Palm Oil Public Company Limited</li>
                                        <li>258 Ao Luek -Laem Sak Road, PO Box 8-9</li>
                                        <li>Ao Luek Tai Subdistrict, Ao Luek District, Krabi 81000</li>
                                    </ul>
                                    <li>3. By email: info@univanich.com</li>
                                    <li>4. Through the message box under the “Whistleblowing” page within the company’s
                                        website</li>
                                </ul><br>

                                <h5>Contact channel for whistleblowing or making complaints</h5>
                                <p>Examples of offences that can be complained include, but not limited to:</p>
                                <ul>
                                    <li>● Criminal offences or advocacy for an offence </li>
                                    <li>● Failure to comply with laws or regulations </li>
                                    <li>● Forgery of financial statements, documents, embezzlement or use of company
                                        funds without authorization </li>
                                    <li>● Violations of clearly communicated work procedures in the form of company
                                        policies, regulations or rules </li>
                                    <li>● Cases of fraud and corruption </li>
                                    <li>● Cases of bullying colleagues or treating unfairly towards others </li>
                                    <li>● Cases of harm to the health or safety of any person </li>
                                    <li>● Cases of damage to the environment </li>
                                    <li>● Detrimental behaviour or unethical behaviour </li>
                                    <li>● Sexual abuse, whether physical or verbal </li>
                                    <li>● Leakage of company secrets</li>
                                </ul><br>
                                <p>Details of whistleblowing and complaints must be true and sufficient for the company
                                    to continue the investigation. The complainants will receive assured protection by
                                    the company and immune to external threats. Complaint information obtained will be
                                    treated with utmost confidentiality, and will only be disclosed as necessary. The
                                    company will mainly consider the safety and potential damages to the complainant,
                                    and the name will not be disclosed without the consent of the whistleblower.</p>
                                <p>If the complainant is under risk for possible ramifications and damages, the company
                                    will establish and implement safeguards and policies to protect such individuals or
                                    groups. In the case of complainants receiving threats or ramifications, the company
                                    will support and compensate the complainants with fair and just procedures.
                                    These measures will only be used in cases where the complainant has pure intentions
                                    and the disclosure is in good faith.</p>
                                <p>If the complaints are investigated and found to be slander, defamation, or false
                                    statements for personal gain, the company will treat it as a disciplinary offence
                                    and corrupt behaviour. In this case, the company will implement serious disciplinary
                                    measures to the complainant. If the complainant is an employee of Univanich, the
                                    company will punish according to the regulations of the company accordingly. In the
                                    case of a complainant originating from external sources, the complainant will be
                                    notified, and legal proceedings will be issued.</p><br>
                                <h5>The operational process when receiving complaints</h5>
                                <ul>
                                    <li>1. When the Operations Committee receives information on reports of wrongdoing
                                        and
                                        fraud, an investigation department will be established to carry out the
                                        collection of evidence and facts in the processing and judgement of wrongdoing.
                                        The appointed persons must not be involved in or having interests in matters
                                        that are related to the complaint.</li>
                                    <li>2. The investigation department will investigate the facts through the
                                        collection of evidence and questioning witnesses and the reported.</li>
                                    <li>3. The operational committee will call a meeting with the investigation
                                        department to analyze such information and evidence collected, and consider
                                        appropriate steps in furthering the investigation in order to maintain
                                        confidentiality and limitations of the law. If in this step there is not enough
                                        evidence collected or there is no evidence, the case may be dropped.</li>
                                    <li>4. If the case has substance and requires further investigation such as
                                        coordination with the police department, or external legal issues, the
                                        operational committee will coordinate with external law departments and continue
                                        the investigation. </li>
                                    <li>5. The operational committee will produce a report for the complainant if he/she
                                        chooses to disclose their identity. If not, the operational committee will
                                        report to the Management Committee or Board of Directors, depending on the
                                        severity of the case.</li>
                                    <li>6. The operational committee will produce penalties for those who have done
                                        wrong or compensate those who have been damaged/impacted.</li>
                                </ul><br>
                                <h5>Measures for the protection of complainants</h5>
                                <p>The company has the policy to accept complaints both anonymously and non-anonymously.
                                    In both cases, the company will keep the name and identity of the complainant
                                    throughout the process as confidential as possible in order to prevent the
                                    complainant from harassment, intimidation, harm or impact of any relationship at
                                    work.</p>
                                <p>If in the event with the necessity to disclose information, name or identity of the
                                    complainant such as disclosure of further information to give to authorities,
                                    testimonies or attending hearings, the company will limit the number of people
                                    involved to as little as possible. Appropriate protection measures for the
                                    complainant will also be assured.</p>
                                <p>If the complainant is affected by the complaint or damaged in any way related to the
                                    complaint, the complainant can notify the company. In such cases, the company will
                                    compensate for appropriate damages or prescribing safeguard measures in order to
                                    protect the complainant.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--services details end-->

@endsection
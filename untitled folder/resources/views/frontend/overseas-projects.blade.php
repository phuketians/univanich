<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="BizBite corporate business template or agency and marketing template helps you easily create websites for your business.">
    <meta name="author" content="ThemeTags">


    <!--title-->    
    <title>Power Plants || Univanich</title>

    <!--favicon icon-->
    <link rel="icon" href="img/favicon.png" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans:400,600&display=swap" rel="stylesheet">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!--Magnific popup css-->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!--Themify icon css-->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!--Fontawesome icon css-->
    <link rel="stylesheet" href="css/all.min.css">
    <!--animated css-->
    <link rel="stylesheet" href="css/animate.min.css">
    <!--ytplayer css-->
    <link rel="stylesheet" href="css/jquery.mb.YTPlayer.min.css">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!--custom css-->
    <link rel="stylesheet" href="css/style.css">
    <!--responsive css-->
    <link rel="stylesheet" href="css/responsive.css">

</head>
<body>

<!--loader start-->
<div id="preloader">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!--loader end-->

<!--header section start-->
<header class="header">    
    <!--start navbar-->
    <nav class="navbar navbar-expand-lg fixed-top white-bg">
        <div class="container position-relative">
            <a class="navbar-brand" href="home.html">
                <img src="img/logo-white.png" alt="logo" class="img-fluid"/>
            </a>

            <div class="main-title">
                <div class="title"> Univanich Plam Oil Public Co., Ltd.</div>
                <div class="sub-title">
                    <span>Univanich | 45.50 (+2.25 | 5.70%)</span>                    
                    <span>
                        <a href="#">TH</a> |
                        <a href="#">EN</a>
                    </span> 
                </div>                
            </div>             

            <div class="navbar-header">

                <a class="navbar-brand text-uppercase" href="#">open menu</a>

                <button data-target="#top-nav" data-toggle="collapse" type="button" class="navbar-toggle">
                    <i class="fa fa-bars"></i>
                </button>
                
            </div>            
        </div>

        <div class="MenuWrap">
            <div class="container">
                <ul>
                  <li><a href="home.html">Home</a></li>
                  <li><a href="company_profile.html">Company Profile</a>
                      <ul>
                          <li><a href="chairman_report.html">Message from CEO and/or? Chairman</a></li>
                          <li><a href="principles-code.html">Code of Business Principles</a></li>
                          <li><a href="company_profile.html">Company Profile/Who we are</a></li>
                          <li><a href="timeline.html">Timeline</a></li>
                          <li><a href="#">Board of Directors</a></li>
                          <li><a href="#">Management Team</a></li>
                          <li><a href="#">Awards</a></li>                            
                      </ul>
                  </li>
                  <li><a href="#">Operations</a>
                      <ul>
                          <li><a href="estates.html">Estates</a></li>
                          <li><a href="factories.html">Factories</a></li>
                          <li><a href="#">Biogas</a></li>
                          <li><a href="nurseries.html">Nurseries</a></li>
                      </ul>

                      <a href="#">Products</a>

                      <ul>
                          <li><a href="#">CPO & PKO</a></li>
                          <li><a href="#">Seeds & Seedlings</a></li>
                          <li><a href="#">Byproducts</a></li>
                          <li><a href="#">Biogas</a></li>                        
                      </ul>
                  </li>

                  <li><a href="#">Investor Relations</a>
                      <ul>
                          <li><a href="company_profile.html">Company Profile</a></li>
                          <li><a href="financial_information.html">Financial Information</a></li>
                          <li><a href="#">Stock Information (stock quotes, embed like BGP)</a></li>
                          <li><a href="shareholder.html">Shareholder Information</a></li>
                          <li><a href="#">Newsroom</a></li>
                          <li><a href="awards.html">Awards</a></li>
                          <li><a href="#">Publications and Download</a>
                              <ul>
                                  <li><a href="#">Annual report</a></li>
                                  <li><a href="#">Company factsheet</a></li>
                                  <li><a href="#">Brochure</a></li>
                              </ul>
                          </li>                            
                      </ul>
                  </li>
                  <li><a href="#">Sustainability</a>
                      <ul>
                          <li><a href="#">CSR (Social Sustainability)</a></li>
                          <li><a href="#">RSPO (Environmental Sustainability)</a></li>
                          <li><a href="#">Corporate Governance</a></li>
                          <li><a href="#">Environment (Environmental Sustainability)</a></li>
                      </ul>                            
                  </li>
                  <li><a href="#">Media</a>
                      <ul>
                          <li><a href="#">News</a></li>
                          <li><a href="#">Downloads</a>
                              <ul>
                                  <li><a href="#">Fact sheets/brochures/etc</a></li>
                              </ul>
                          </li>
                      </ul>    

                      <a href="#">Careers</a>
                      <ul>
                          <li><a href="#">Careers Center</a></li>
                      </ul>

                      <a href="#">Contact Us</a>

                      <ul>
                          <li><a href="#">Whistleblowing</a></li>
                          <li><a href="#">Feedback and Inquiries</a></li>
                      </ul> 

                  </li> 
              </ul>
            </div>
        </div>        
    </nav>
</header>
<!--header section end-->

<!--body content wrap start-->
<div class="main">     

    <!--our work or portfolio section start-->
    <section class="bg-image fixed ptb-100" style="background-image: url('img/list-bg.jpg'); min-height: 94vh">
        <div class="container">
           <div class="row">
                <div class="col-md-12">
                    <div class="page-header-content text-black pt-sm-5 pt-md-5 pt-lg-0">
                        <div class="custom-breadcrumb">
                            <ol class="breadcrumb breadcrumbDark d-inline-block bg-transparent list-inline py-0">
                                <li class="list-inline-item breadcrumb-item">
                                    <a href="home.html">Home</a>
                                </li>
                                <li class="list-inline-item breadcrumb-item">
                                    <a href="power-plants.html">Power Plants</a>
                                </li>
                                <li class="list-inline-item breadcrumb-item active">Overseas Projects</li>
                            </ol>
                        </div>

                        <h2 class="text-black mt-3">Overseas Projects</h2>
                    </div>
                </div> 

                <div class="col-md-12 p-b-md text-right mb-4">
                    <select name="country">
                        <option value="https://www.bgrimmpower.com/en/power-plants/overseas">All</option>
                        <option value="https://www.bgrimmpower.com/en/power-plants/overseas/laos" selected="">Laos</option>
                        <option value="https://www.bgrimmpower.com/en/power-plants/overseas/cambodia">Cambodia</option>
                        <option value="https://www.bgrimmpower.com/en/power-plants/overseas/vietnam">Vietnam</option>
                        <option value="https://www.bgrimmpower.com/en/power-plants/overseas/south-korea">South Korea</option>
                    </select>
                </div><!-- /.col-md-12 -->
           </div>

           <div class="row">
                <div class="col-md-6">
                   <div class="block-project">
                       <header class="text-center">
                          <h6 class="text-bold text-uppercase m-y-0">In Operation</h6>
                       </header>

                       <div id="accordion-1" class="accordion accordion-faq">
                            <!-- Accordion card 1 -->
                            <div class="card">
                                <div class="card-header py-4" id="heading-1-1" data-toggle="collapse" role="button"
                                     data-target="#collapse-1-1" aria-expanded="false" aria-controls="collapse-1-1">
                                    <h6 class="mb-0">
                                        <span class="ti-receipt mr-3"></span> 
                                        Xenamnoy and Xekatam Hydro Power Co., Ltd. 
                                        <span class="d-block ml-4 text-bold text-uppercase"> Plant Type : Hydro Power</span>
                                    </h6>
                                </div>
                                <div id="collapse-1-1" class="collapse" aria-labelledby="heading-1-1"
                                     data-parent="#accordion-1">
                                    <div class="card-body">
                                        <figure>
                                           <a href="project-details.html">
                                              <img class="img-fluid" src="img/portfolios/2.jpg" alt="">
                                              <figcaption>
                                                 <small class="text-uppercase">DETAILS</small>
                                              </figcaption>
                                           </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                            <!-- Accordion card 2 -->
                            <div class="card">
                                <div class="card-header py-4" id="heading-1-2" data-toggle="collapse" role="button"
                                     data-target="#collapse-1-2" aria-expanded="false" aria-controls="collapse-1-2">
                                    <h6 class="mb-0"><span class="ti-gallery mr-3"></span> 
                                      Xenamnoy and Xekatam Hydro Power Co., Ltd. 
                                        <span class="d-block ml-4 text-bold text-uppercase"> Plant Type : Hydro Power</span>
                                      </h6>
                                </div>
                                <div id="collapse-1-2" class="collapse" aria-labelledby="heading-1-2"
                                     data-parent="#accordion-1">
                                    <div class="card-body">
                                        <figure>
                                           <a href="project-details.html">
                                              <img class="img-fluid" src="img/portfolios/1.jpg" alt="">
                                              <figcaption>
                                                 <small class="text-uppercase">DETAILS</small>
                                              </figcaption>
                                           </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                            <!-- Accordion card 3 -->
                            <div class="card">
                                <div class="card-header py-4" id="heading-1-3" data-toggle="collapse" role="button"
                                     data-target="#collapse-1-3" aria-expanded="false" aria-controls="collapse-1-3">
                                    <h6 class="mb-0"><span class="ti-wallet mr-3"></span> Xenamnoy and Xekatam Hydro Power Co., Ltd. 
                                        <span class="d-block ml-4 text-bold text-uppercase"> Plant Type : Hydro Power</span>                                      
                                    </h6>
                                </div>
                                <div id="collapse-1-3" class="collapse" aria-labelledby="heading-1-3"
                                     data-parent="#accordion-1">
                                    <div class="card-body">
                                        <figure>
                                           <a href="project-details.html">
                                              <img class="img-fluid" src="img/portfolios/3.jpg" alt="">
                                              <figcaption>
                                                 <small class="text-uppercase">DETAILS</small>
                                              </figcaption>
                                           </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
               </div><!-- /.col-md-6 -->

                <div class="col-md-6">
                   <div class="block-project development">
                       <header class="text-center">
                          <h6 class="text-bold text-uppercase m-y-0">In Development</h6>
                       </header>

                       <div id="accordion-2" class="accordion accordion-faq">
                            <!-- Accordion card 1 -->
                            <div class="card">
                                <div class="card-header py-4" id="heading-2-1" data-toggle="collapse" role="button"
                                     data-target="#collapse-2-1" aria-expanded="false" aria-controls="collapse-2-1">
                                    <h6 class="mb-0"><span class="ti-receipt icon mr-3"></span> Xenamnoy and Xekatam Hydro Power Co., Ltd. 
                                        <span class="d-block ml-4 text-bold text-uppercase"> Plant Type : Hydro Power</span>
                                      </h6>
                                </div>
                                <div id="collapse-2-1" class="collapse" aria-labelledby="heading-2-1"
                                     data-parent="#accordion-2">
                                    <div class="card-body">
                                        <figure>
                                           <a href="project-details.html">
                                              <img class="img-fluid" src="img/portfolios/4.jpg" alt="">
                                              <figcaption>
                                                 <small class="text-uppercase">DETAILS</small>
                                              </figcaption>
                                           </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                            <!-- Accordion card 2 -->
                            <div class="card">
                                <div class="card-header py-4" id="heading-2-2" data-toggle="collapse" role="button"
                                     data-target="#collapse-2-2" aria-expanded="false" aria-controls="collapse-2-2">
                                    <h6 class="mb-0"><span class="ti-lock icon mr-3"></span> Xenamnoy and Xekatam Hydro Power Co., Ltd. 
                                        <span class="d-block ml-4 text-bold text-uppercase"> Plant Type : Hydro Power</span>
                                      </h6>
                                </div>
                                <div id="collapse-2-2" class="collapse" aria-labelledby="heading-2-2"
                                     data-parent="#accordion-2">
                                    <div class="card-body">
                                        <figure>
                                           <a href="project-details.html">
                                              <img class="img-fluid" src="img/portfolios/5.jpg" alt="">
                                              <figcaption>
                                                 <small class="text-uppercase">DETAILS</small>
                                              </figcaption>
                                           </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                            <!-- Accordion card 3 -->
                            <div class="card">
                                <div class="card-header py-4" id="heading-2-3" data-toggle="collapse" role="button"
                                     data-target="#collapse-2-3" aria-expanded="false" aria-controls="collapse-2-3">
                                    <h6 class="mb-0"><span class="ti-widget icon mr-3"></span> Xenamnoy and Xekatam Hydro Power Co., Ltd. 
                                        <span class="d-block ml-4 text-bold text-uppercase"> Plant Type : Hydro Power</span>
                                    </h6>
                                </div>
                                <div id="collapse-2-3" class="collapse" aria-labelledby="heading-2-3"
                                     data-parent="#accordion-2">
                                    <div class="card-body">
                                        <figure>
                                           <a href="project-details.html">
                                              <img class="img-fluid" src="img/portfolios/6.jpg" alt="">
                                              <figcaption>
                                                 <small class="text-uppercase">DETAILS</small>
                                              </figcaption>
                                           </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>                 
                    </div>
               </div><!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="list-categories">
                        <ul class="list-inline">
                            <li class="text-uppercase"><span class="bg-grey"></span>In Operation
                            </li>
                            <li class="text-uppercase">
                                <span class="bg-orange"></span>In Development
                            </li>
                        </ul>
                    </div>
                </div>
           </div>
        </div>
    </section>
    <!--our work or portfolio section end-->

</div>
<!--body content wrap end-->

<!--footer section start-->
<footer class="footer-section">  
    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg py-3">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12">
                    <p class="copyright-text pb-0 mb-0">Copyrights © 2020. All
                        rights reserved by Univanich Plam Oil Public Co. Ltd.                        
                </div>
                
            </div>
        </div>
    </div>
    <!--footer copyright end-->
</footer>
<!--footer section end-->

<!--bottom to top button start-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="ti-angle-up"></span>
</button>
<!--bottom to top button end-->


<!--jQuery-->
<script src="js/jquery-3.4.1.min.js"></script>
<!--Popper js-->
<script src="js/popper.min.js"></script>
<!--Bootstrap js-->
<script src="js/bootstrap.min.js"></script>
<!--Magnific popup js-->
<script src="js/jquery.magnific-popup.min.js"></script>
<!--jquery easing js-->
<script src="js/jquery.easing.min.js"></script>
<!--jquery ytplayer js-->
<script src="js/jquery.mb.YTPlayer.min.js"></script>
<!--Isotope filter js-->
<script src="js/mixitup.min.js"></script>
<!--wow js-->
<script src="js/wow.min.js"></script>
<!--owl carousel js-->
<script src="js/owl.carousel.min.js"></script>
<!--countdown js-->
<script src="js/jquery.countdown.min.js"></script>
<!--custom js-->
<script src="js/scripts.js"></script>
</body>
</html>
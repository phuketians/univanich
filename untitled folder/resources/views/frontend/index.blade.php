@extends('frontend.layouts.master')
@section('title', 'Home')
@section('content')

<section class="hero-equal-height hero-tab-slider pt-165 pb-100 gradient-overfly-right-color"
    style="background: url('images/mainbg.jpg')no-repeat center center / cover">
    <div class="container">
        <div class="row">
            <div class="owl-carousel owl-theme custom-indicator-slider">
                <div class="item">
                    <div class="col-md-8 col-lg-7 col-12">
                        <div class="hero-content-wrap text-white position-relative z-index">
                            <h1 class="text-white">Univanich Operations</h1>
                            <p class="lead">Univanich is proud to run a fully integrated and sustainable operation. From our seed production and  seedling sales, to delivering high quality fresh fruit from our own estates, and purchasing fresh fruit from over 4,000 smallholder farmers. Fresh fruit is processed in our 5 factories and the by products provide valued energy feedstocks to help power our own operations, as well as supplying third party users.</p>

                            <div class="action-btns mt-3">
                                <a href="{{ route('operation-map') }}" class="btn secondary-solid-btn">Explore</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="col-md-8 col-lg-7 col-12">
                        <div class="hero-content-wrap text-white position-relative z-index">
                            <h1 class="text-white">Investor Relations</h1>
                            <p class="lead">Since Univanich’s public listing under UVAN in 2005, we have continuously maintained strong financial performance and relations with our investors and shareholders.</p>

                            <div class="action-btns mt-3">
                                <a href="{{ route('investor-relations') }}" class="btn secondary-solid-btn">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="col-md-8 col-lg-7 col-12">
                        <div class="hero-content-wrap text-white">
                        <h1 class="text-white">Seed Sales</h1>

                            <p class="lead">Since 1983, Univanich had been in a constant search for the best Palm Oil breeds possible. Today, our internationally-recognized breeding program and research centre consistently offers the latest high-quality seeds and seedlings available for sale.</p>


                            <div class="action-btns mt-3">
                                <a href="{{ route('seed-seedlings') }}" class="btn secondary-solid-btn">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="col-md-8 col-lg-7 col-12">
                        <div class="hero-content-wrap text-white">
                            <h1 class="text-white">Sustainability</h1>
                            <p class="lead">As the pioneer of the Palm Oil industry in Thailand, Univanich has emphasized what it perceives as the “Four Pillars of Sustainability” to develop a competitive, expansive, and sustainable industry. Today, Thailand is the world’s third-largest palm oil producer with annual production exceeding two million tonnes.</p>

                            <div class="action-btns mt-3">
                                <a href="{{ route('sustainability') }}" class="btn secondary-solid-btn">Explore</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--custom indicator list start-->
    <ul id="carousel-custom-indicator" class="owl-dots list-inline text-center custom-indicators">
        <li class="list-inline-item">
            <button class="d-flex align-items-center gray-light-bg"><span
                    class="ti-world icon-sm color-secondary mr-2"></span> Operations Map
            </button>
        </li>
        <li class="list-inline-item">
            <button class="d-flex align-items-center gray-light-bg"><span
                    class="ti-bar-chart icon-sm color-secondary mr-2"></span> Investor Relations
            </button>
        </li>
        <li class="list-inline-item">
            <button class="d-flex align-items-center gray-light-bg"><span
                    class="ti-wallet icon-sm color-secondary mr-2"></span> Seed Sales
            </button>
        </li>
        <li class="list-inline-item">
            <button class="d-flex align-items-center gray-light-bg"><span
                    class="ti-wallet icon-sm color-secondary mr-2"></span> Sustainability
            </button>
        </li>
    </ul>
    <!--custom indicator list end-->
</section>

@endsection

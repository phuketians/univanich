@extends('frontend.layouts.master')
@section('title', 'Byproducts')
@section('content')


@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "By Products",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Products',
                            'url' => 'products'
                        ],

                        [
                            'item_title' => 'By Products'
                        ],
                    ]
            ]
        )

<section class="col_wrap ptb-100" style="min-height: 550px">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src=" {{ asset('images/products/byproducts01.jpg') }} " alt="cpo-pko" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <h5>By-Products</h5>
                <p>The Palm Oil milling process generates large amounts of waste/ by-products from converting Fresh
                    fruit bunches to oil. In a typical palm oil mill, almost 70% of the fresh fruit bunches are turned
                    into wastes in the form of empty fruit bunches, fibre and shell, as well as liquid wastewater.
                    Thanks to efficient energy technologies these waste products can now be converted to value-added
                    products or energy to fuel our own operations or be sold to other energy-producing operations.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Empty Fruit Bunches</h5>
                <p>
                    Once the fresh fruit bunch has been sterilized and been through the thresher to remove the fruits.
                    The empty bunch is then pressed for residual oil and shredded. It is a fibrous material of purely
                    biological origin. It can be used as an extremely effective organic fertilizer and mulch for the
                    plantation or alternatively, it can be further dried and used as a fuel for biomass power stations.
                    Univanich utilizes the empty fruit bunches as a mulch and fertilizer source for the plantation
                    estates that are surrounding the palm oil factory. Univanich also encourages the surrounding
                    smallholders to utilize the empty fruit bunches as an alternative to inorganic fertilizer. For
                    factories that have no surrounding estate, the empty fruit bunches are purchased by biomass energy
                    companies in Southern Thailand.</p>
            </div>
            <div class="col-md-6">
                <img src=" {{ asset('images/products/empty-fruit-bunches.jpg') }} " alt="cpo-pko" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->
        </div>
        <div class="row">
            <div class="col-md-6">
                <img src=" {{ asset('images/products/fibre.jpg') }} " alt="cpo-pko" class="img-fluid rounded shadow-sm">
        </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <h5>Mesocarp Fibre</h5>
                <p>
                    Mesocarp fibre is produced after the fruit press stage in the milling process. It is dried and
                    primarily used for boiler fuel that then energises a steam turbine that is used to provide the
                    factory with power. With improved boiler technology and efficiency, there is now often a surplus
                    mesocarp fibre. The fibre is easy to handle and less bulky to transport and therefore it lends
                    itself to be a valuable feedstock for Biomass energy businesses. Univanich focuses on the efficient
                    use of fibre for its own energy requirements to allow for additional value-added sales.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Palm Kernel Shell</h5>
                <p>
                    Palm kernel shell is the residue that remains after the nut has been cracked and removed. The
                    moisture content of shell is much lower and due to the oil residue that remains on the shell, it has
                    a higher calorific value than the other fibrous by-product material from the milling process. It is
                    less bulky and easily handled. This makes it a very suitable biomass fuel, that is highly sought
                    after both for domestic use and often for export.</p>
                    <h5>Palm Oil Mill Waste Water</h5>
                <p>
                    When milling 1 ton of fresh fruit, the process uses approx 0.7 tons of water. This wastewater is
                    predominantly made up of 95% water, residual oil, soil particles and biological suspended solids.
                    This is broken down through anaerobic and aerobic digestion in a series of settling ponds. Bacteria
                    break down the organic matter in the water and lowers the biological oxygen demand (BOD) to a safe
                    level. Univanich is able to use this plentiful feedstock for two much-needed services. Firstly
                    through the design of in-ground anaerobic reactors at four of its factories. (CVP Biogas project is
                    a joint venture with Asia Biogas who build and operate the anaerobic reactor and gas engine). The
                    wastewater is fed into the anaerobic reactor and methane gas is collected. This protects the
                    environment from potent greenhouse gas and also provides an energy source that is able to power a
                    gas engine and produce electricity that is used for our own operations and also supplied to the
                    local electrical grid.</p>
                <p>Secondly following the reduction of the Biological oxygen demand to a level below 20 parts per
                    million (PPM). The wastewater is then used as a source of irrigation to our plantation estates to
                    enhance growth and performance of our palms. Southern Thailand has prolonged dry weather
                    intervals throughout the year, and this creates a soil moisture deficit that can have a negative
                    effect on FFB production. However, through sensible use of our wastewater Univanich is able to
                    utilize this plentiful supply of nutrient-enriched wastewater in an environmentally and sustainable
                    manner to increase our FFB production in our company estates.</p>
            </div>
            <div class="col-md-6">
                <img src=" {{ asset('images/products/palm-fruits.jpg') }} " alt="cpo-pko" class="img-fluid rounded shadow-sm">
                <h5>Palm Kernel Cake</h5>
                <p>
                    This is the residue that remains after the oil has been pressed from the Kernel. It is a high
                    protein feed for animals. Univanich produces approximately 30,000 tons per annum. All palm kernel
                    cake is sold domestically in Thailand for animal feed.</p>
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap -->


@endsection

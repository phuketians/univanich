@extends('frontend.layouts.master')
@section('title', 'Biogas')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Biogas",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Products',
                            'url' => 'products'
                        ],

                        [
                            'item_title' => 'Biogas'
                        ],
                    ]
            ]
        )

<section class="col_wrap ptb-100" style="min-height: 630px">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src=" {{ asset('images/biogas/02.jpg') }} " alt="Biogas" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <p>All byproducts of our factories do not go to waste. One important byproduct of crushing mills is the
                    wastewater- also known as POME (Palm Oil Mill Effluent). POME is moisture extracted from palm
                    bunches and is filled with nutrients, with a high biological oxygen demand which can be harmful to
                    local ecosystems if discharged untreated. Furthermore, POME generates and emits methane, a powerful
                    greenhouse gas that damages the atmosphere. To solve this problem, we congregate our POME into a
                    biogas reactor, where bacteria anaerobically digests POME, generating large amounts of methane in
                    the process that we can use to generate electricity through a gas engine. POME that is treated
                    through our biogas reactors have their BOD reduced substantially, up to 5 times its initial size and
                    down to safe levels for the environment.</p>
                <p>The methane we generate goes to a gas engine which generates up to 2 MW of electricity per biogas
                    reactor, which we utilize as a backup generator for our operations, as well as selling the surplus
                    back to the national grid.</p>

                <p style="margin-bottom: 0">In doing so, our environmental objectives have been achieved:</p>
                <ul class="ml-5">
                    <li>1. A large reduction in Greenhouse Gas Emissions</li>
                    <li>2. The renewable energy (biogas) source has replaced fossil fuels</li>
                    <li>3. Wastewater treatment has improved</li>
                    <li>4. Factory efficiencies have improved</li>
                    <li>5. New skilled jobs have been created in rural communities</li>
                </ul>
            </div><!-- /.col-md-6 -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap -->

@endsection

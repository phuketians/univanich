@extends('frontend.layouts.master')
@section('title', 'Seedlings')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Seedlings",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Products',
                            'url' => 'products'
                        ],

                        [
                            'item_title' => 'Seedlings'
                        ],
                    ]
            ]
        )

<section class="col_wrap ptb-100" style="min-height: 630px">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src=" {{ asset('images/products/seedlings01.jpg') }} " alt="Biogas" class="img-fluid rounded shadow-sm"><br>
                <img src=" {{ asset('images/products/seedlings02.jpg') }} " alt="Biogas" class="img-fluid rounded shadow-sm"><br>
                <img src=" {{ asset('images/products/seedlings03.jpg') }} " alt="Biogas" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <h5>Seedlings</h5><br>
                <p>TSimilar to our seeds (hyperlink), we provide commercial Seedlings from our strategically located nurseries in Krabi, Phang Nga, Nakhon Si Thammarat and Phatthalung Univanich seedlings and our elite clonal seedlings are grown in our EFB compost-enriched medium using wastewater from factories for added nutrient supply and minimising the use of inorganic fertilisers,  to produce high-quality seedlings for domestic sales. Seedlings are available for purchase throughout the year from all our nursery locations and can also be booked in advance. Additional guidance on planting information and fertiliser requirements can be obtained from our Agricultural experts at the time of seedling purchase.</p>
                <p style="margin-bottom: 0">All our seeds have two key features</p>
                <ul class="ml-5">
                    <li><strong>1. Irrigation of oil palms in Southern Thailand </strong>
                    <p>Palat Tittinutchanon, B G Smith and R H V Corley <br>
                    ISP International Planters Conference, May 2000 </p></li>
                    <li><strong>2. Replanting oil palms in Thailand : Underplanting with Various Thinning and Pruning Techniques </strong>
                    <p>J H Clendon and Palat Tittinutchanon<br>
                    Incorporated Society of Planters : National Seminar, 4 – 15 June 2004</p></li>
                    <li><strong>3. The Univanich oil palm Breeding Programme and Progeny Trial Results from Thailand</strong>
                    <p>V. Rao, Palat Tittinutchanon, Chayawat Nakharin and R H V Corley <br>
                    The Planter. Vol. 84. No. 989, August 2008</p></li>
                    <li><strong>4. A Review of Three CDM Biogas Projects Based on Palm Oil Mill Effluent, in Southern Thailand </strong>
                    <p>Sompol Tantitham, Phiphit Khlaisombat, J H Clendon, M Campbell-Board and B McIntosh <br>
                    PIPOC International Palm Oil Conference. 9 – 12 November 2009</p></li>
                    <li><strong>5. Maximising oil palm Yield by High Density Planting and Thinning </strong>
                    <p>Palat Tittinutchanon, Chayawat Nakharin and R H V Corley<br>The Planter, 88 (1033) : 241 – 256 2012</p></li>
                    <li><strong>6. Maximising Lifetime Yield for Greater Economic Sustainability</strong>
                    <p>R H V Corley & Palat Tittinutchanon <br>
                    PIPOC International Palm Oil Conference, Kuala Lumpur, 2013
</p></li>
                </ul>
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div>
</section>

@endsection
@extends('frontend.layouts.master')
@section('title', 'Sustainability')
@section('content')



<!--body content wrap start-->
<div class="main">

    <!--hero section start-->
    <section class="hero-equal-height" style="background: url('/images/sustainability/sustainability-main.jpg')no-repeat center center / cover">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-lg-6">
                    <div class="hero-slider-content">
                        <span class="text-uppercase">UNIVANICH PALM OIL PUBLIC CO. LTD.</span>
                        <h1>SUSTAINABILITY</h1>
                        <p class="lead">Univanich Palm Oil PCL has led the way in changing that negative perception into a new
                            reality.
                            </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--hero section end-->

    <!--promo section start-->
    <section class="promo-block ptb-100 mt--165 z-index position-relative">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-3">
                    <div class="single-promo-block promo-hover-bg-1 hover-image shadow-lg p-5 custom-radius white-bg">
                        <div class="promo-block-content">
                            <a href="/sustainability/social-sustainability"><h5><center>Social Sustainability</center></h5></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3">
                    <div class="single-promo-block promo-hover-bg-2 hover-image shadow-lg p-5 custom-radius white-bg">
                        <div class="promo-block-content">
                            <a href="/sustainability/environmental-sustainability"><h5><center>Environmental Sustainability</center></h5></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3">
                    <div class="single-promo-block promo-hover-bg-3 hover-image shadow-lg p-5 custom-radius white-bg">
                        <div class="promo-block-content">
                            <a href="/sustainability/economic-sustainability"><h5><center>Economic Sustainability</center></h5></a>
                        </div>
                    </div>
                </div><div class="col-md-3 col-lg-3">
                    <div class="single-promo-block promo-hover-bg-3 hover-image shadow-lg p-5 custom-radius white-bg">
                        <div class="promo-block-content">
                            <a href="/sustainability/scientific-sustainability"><h5><center>Scientific Sustainability</center></h5></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--promo section end-->

    <!--about us section start-->
    <section class="about-us-section pb-100">
        <div class="container">
            <div class="row justify-content-between align-items-top">
                <div class="col-md-12 col-lg-6">
                    <div class="video-promo-content mb-md-4 mb-lg-0">
                        <p class="lead">When the first oil palms were planted back in 1969 Thailand was regarded as an unpromising
                            region for this new crop. Fruit yields were limited by a severe annual dry season and there
                            were few large areas of land available for commercial oil palm plantations. These constraints
                            coupled with inadequate supporting infrastructure led to the view that Thailand’s oil palm
                            growers could never compete successfully with the much larger and more developed palm oil
                            industries in Malaysia and Indonesia.</p>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="video-promo-content mb-md-4 mb-lg-0">
                        <p class="lead">Univanich Palm Oil PCL has led the way in changing that negative perception into a new
                            reality. Thailand is now the world’s third-largest palm oil producer with annual production
                            exceeding two million tonnes. In helping to promote this successful transformation the
                            company has emphasized what it perceives as the “Four Pillars of Sustainability.” </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--about us section end-->

</div>
<!--body content wrap end-->

@endsection

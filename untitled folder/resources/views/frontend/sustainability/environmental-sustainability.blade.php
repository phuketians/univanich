@extends('frontend.layouts.master')
@section('title', 'Environmental Sustainability')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Environmental Sustainability",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Sustainability',                         
                        ]
                    ]
            ]
        )
<!--services details start-->
        <section class="service-details-section ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="service-details-wrap">
                            <img src="/images/sustainability/env-sus01.jpg" alt="services" class="img-fluid rounded shadow-sm" />
                            <div class="services-detail-content mt-4">
                                <h5>At Univanich, a pursuit of environmental sustainability is one of our utmost
                                    priorities. </h5>
                                <p>Some key environmental sustainability features at Univanich operations include:</p>
                                <ul>
                                    <li><strong>1. Fully-integrated internal power generation from our boilers, which uses palm
                                        byproducts as our fuel. Exhaust from boilers is treated with wet scrubbers to
                                        minimize air pollution.</strong></li>
                                    <ul class="list-unstyled tech-feature-list mb-3">
                                        <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>In
                                            addition, four of our five crushing mills in Thailand are attached to
                                            biogas reactors that generate clean and sustainable electricity from our
                                            wastewater which are then sold to the grid to provide power for the local
                                            community. </li>
                                    </ul>
                                </ul>

                                <ul>
                                    <li><strong>2. Chemical usage is minimal to none. These examples include:</strong></li>
                                    <ul class="list-unstyled tech-feature-list mb-3">
                                        <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>Organic
                                            Pest control at our estates including biological controls such as barn owls
                                            for mice, or pheromone buckets for rhinoceros beetles.</li>
                                        <ul>
                                            <li>i. Local farmers adopt more environmental friendly Barn Owls to help
                                                control rats in their family farms</li>
                                        </ul>
                                    </ul>
                                    <ul class="list-unstyled tech-feature-list mb-3">
                                        <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>The
                                            growth of Napier grass as a cover crop to suppress weeds, as well as
                                            Desmodium grass which provides nitrogen fixation, keeping our land fertile.
                                        <li>
                                            <ul class="list-unstyled tech-feature-list mb-3">
                                                <li class="py-1"><span
                                                        class="ti-check-box mr-2 color-secondary"></span>The operations
                                                    within our crushing mills are done entirely with steam boiled from
                                                    local watersheds with no chemicals added.
                                                <li>
                                            </ul>
                                            <ul>
                                                <li><strong>3. All of our byproducts can be reused within our operations. Two
                                                    key byproducts include:</strong></li>
                                                <ul class="list-unstyled tech-feature-list mb-3">
                                                    <li class="py-1"><span
                                                            class="ti-check-box mr-2 color-secondary"></span>Wastewater
                                                        or Palm Oil Mill Effluent (POME) which are composed of water and
                                                        organic palm content. They can cause ecological damage to the
                                                        environment due to their high Biological Oxygen Demand (BOD), as
                                                        well as high methane emissions to the atmosphere when disposed
                                                        of improperly. As such, POME is brought into either of our 5
                                                        biogas reactors which capture methane to generate electricity
                                                        and purify the fluids simultaneously. The purified POME are then
                                                        brought into treatment ponds to lower BOD levels before they are
                                                        sent to irrigate our estates. In particular, the high levels of
                                                        nutrients in POME confers further growth benefits to our palm
                                                        plants.</li>
                                                </ul>
                                                <ul class="list-unstyled tech-feature-list mb-3">
                                                    <li class="py-1"><span
                                                            class="ti-check-box mr-2 color-secondary"></span>Empty Fruit
                                                        Bunches (EFB) are the largest source of biomass byproducts in
                                                        our operations. At Univanich, they are shredded and mixed with
                                                        POME to create high-biomass compost, or simply returned to the
                                                        field. The EFB compost replenishes nutrients within the topsoil,
                                                        suppresses weed growth, and prevents soil erosion.
                                                    <li>
                                                </ul>
                                            </ul>
                                            <ul>
                                                <li><strong>4. There is no clearing of forests to plant oil palms in Thailand.</strong>
                                                </li>
                                                <ul class="list-unstyled tech-feature-list mb-3">
                                                    <li class="py-1"><span
                                                            class="ti-check-box mr-2 color-secondary"></span>Small
                                                        farmers owning less than 8 ha (50 rai) comprise more than 90% of
                                                        the planted area for our palms. The expansion of production is
                                                        from the conversion of existing agronomical crops grown by
                                                        smallholder farmers to oil palm.</li>
                                                </ul>
                                            </ul>
                                            <ul>
                                                <li><strong>5. In line with our sustainability goals along with the objective to
                                                    provide the company with consistent international standards,
                                                    Univanich has cooperated with Mahidol University, the government
                                                    sector, the GIZ group, and smallholder farmers to form one of the
                                                    world's first four RSPO compliant groups in 2012.</strong></li>
                                                <ul class="list-unstyled tech-feature-list mb-3">
                                                    <li class="py-1"><span
                                                            class="ti-check-box mr-2 color-secondary"></span>The
                                                        formation of the RSPO compliant group has helped farmers to
                                                        improve the quality of palm harvests, enhance delivery levels,
                                                        increase revenue as well as productivity.</li>
                                                </ul>
                                                <ul class="list-unstyled tech-feature-list mb-3">
                                                    <li class="py-1"><span
                                                            class="ti-check-box mr-2 color-secondary"></span>The group
                                                        goes through frequent lectures and training to educate the
                                                        members and monitor their performance. In addition, the farmers
                                                        themselves are responsible for maintaining the fertility of the
                                                        soil and the environment, and the use of fertilizers and
                                                        chemicals must be recorded. There are controls to minimize
                                                        negative impacts on both the community and the environment in
                                                        the practice of planting palms.
                                                    <li>
                                                </ul>
                                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--services details end-->

@endsection

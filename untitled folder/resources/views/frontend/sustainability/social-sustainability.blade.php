@extends('frontend.layouts.master')
@section('title', 'Social Sustainability')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Social Sustainability",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Sustainability',                         
                        ]
                    ]
            ]
        )
<!--about us section start-->
        <section class="team-single-section ptb-100">
        <div class="container">
            <div class="row justify-content-between align-items-top">
                <div class="col-md-12 col-lg-6">
                    <div class="video-promo-content mb-md-4 mb-lg-0">
                        <p">Independent smallholders now make-up more than 80% of Thailand’s 750,000 hectares oil
                            palm industry, mostly located in the southern provinces of Krabi and Surat Thani. What had
                            been an impoverished and politically unstable region 40 years ago has achieved new
                            prosperity largely as a result of this successful agricultural development. Univanich has
                            encouraged this development through the annual supply of more than 1.5 million high-yielding
                            seedlings to local farmers and through field-days at the company’s OPRC training centre. The
                            company strategy of ‘sharing the technology and spreading the prosperity’ has paid off for the
                            entire community. As thousands of small farmers have expanded and prospered, so too has the
                            Univanich business.</p>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="video-promo-content mb-md-4 mb-lg-0">
                        <p>Following the success of this integrated development in Thailand, Univanich has recently
                            moved to replicate a similar business model in Mindanao, southern Philippines. After
                            supplying seeds to the region over several years, in 2014, a joint venture Univanich – Carmen
                            Palm Oil Corporation commissioned its first Philippine crushing mill to service new oil palm
                            smallholders in North Cotabato Province. Univanich believes that given the right investment
                            incentives and bold investment decisions, the southern Philippines can also achieve peaceful
                            prosperity through the successful development of this remarkable crop.
                             </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--about us section end-->

    <!--about us section start-->
    <section class="team-single-section ptb-10">
        <div class="container">
            <div class="row justify-content-between align-items-top">
                <div class="col-md-12 col-lg-12">
                        <p>In addition, we partake in the sponsorship and support of local community projects and provisions, such as:</p>
                        <p>1. Sponsoring a new ambulance and new Portable Oxygen Generators for Aoluk Hospital</p>
                        <p>2. Sponsoring the construction of the Community Hall at Plaiphraya School</p>
                        <p>3. Sponsoring a new Dental Clinic for Plaiphraya Hospital </p>
                        <p>Over the years, Thailand’s oil palm industry has brought many dramatic improvements to previously impoverished rural communities, and we aim to create new prosperity for many thousands of rural families in the future.</p>
                    </div>
                </div>
            </div>
    </section>
    <!--about us section end-->

        <!--team single section start-->
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="/images/sustainability/sos-01.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-02.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-03.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">2020</h4>
                                <h5 class="mb-1">January</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated 33 Univanich saplings to Mr. Danai Suksakul (Aoluk District Marshal)</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated 2,000 THB in the development of the event “Run for Love”</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated 2,000 THB in the sponsorship of awards and refreshments in the development of the event dedicated for the disabled.</li>
                                </ul>
                                <h5 class="mb-1">February</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated 2,000 THB towards the award for the annual Red Cross Naval meeting.</li>
                                </ul>
                                <h5 class="mb-1">March</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated 4,600 THB for the purchase of EPSON L3150 printer for its use at the Damrongtham center (Ministry of Interior) at Aoluk district.     </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated 3,000 THB towards the sponsorship of the “Dharma for Youth” project.</li>
                                </ul>
                                <h5 class="mb-1">April</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated 2,000 THB towards the purchase of food-preparing equipments for the Screening Center for Ao Luk district.     </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated 20 sacks of rice with a value of 3,500 THB for the screening point of Baan Rim Suan village / community.   </li>
                                </ul>
                                <h5 class="mb-1">May</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company provided supplies to victims of COVID-19 with consumable items in the Pantry Project with a value of 15,000 THB.     </li>
                                </ul>
                                <h5 class="mb-1">August</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company donated 18000BTU air conditioning unit in the amount of 18,500 THB for Imam Mosque of Ban Hin Lad.     </li>
                                </ul>
                                <h5 class="mb-1">September</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 15 table tennis sportswear, 200 THB per set, amounting to 3,000 THB to Ao Luek School.     </li>
                                </ul>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->

        <!--team single section start-->
        <section class="about-us-section ptb-100 gray-light-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="/images/sustainability/sos-04.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-05.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-06.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-07.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-08.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-09.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-10.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-11.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-12.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-13.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-14.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-15.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">2019</h4>
                                <h5 class="mb-1">January</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has funded 5,000 THB to support the waiting chairs for patients at Plai Phraya Hospital. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 1,000 THB to support the budget for purchasing awards for the Children's Day activities at Wang Mai Subdistrict Administrative Organization.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has donated 1,000 THB to support the National Children's Day gift, Pa Bon Subdistrict Administrative Organization, Ban Pa Bon Child Development Center. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has contributed 2,000 THB to support the budget for purchasing gifts of the International Day of Disabled Persons 2019.      </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 2,000 THB to fund the 6th Krabi Village Headman Football Club.  </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the budget for the uniform for the security of the village in Plai Phraya district in the amount of 40,000 THB.  </li>
                                </ul>
                                <h5 class="mb-1">February</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported a sum of 5,000 THB to support the renovation of the installation of the local shrine. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the development of the library at Wat Don Maprang School Nakhon Si Thammarat Province with 6 sets of reading tables for an amount of 13,200 THB. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the educational quality development fund for teaching and learning of Ao Luek School through a computer set and classroom equipment totaling 42,290 THB.      </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the amount of 2,000 THB for the religious donation ceremony for educational development at Wat Pa Bon Tam School     </li>
                                </ul>
                                <h5 class="mb-1">March</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored an amount of 2,000 THB to support a performance award on Little Graduation Day at Ban Khlong Praya School  </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported a budget for renovating classrooms and bathrooms at the Nakhon Sri Special Education Center in the amount of 10,000 THB.     </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated 280 oil palm seedlings to Plai Phraya Wittayakhom School.    </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has provided 2,000 THB to support the expenses of Chor Ror Bor Officers, Plai Phraya Village.</li>
                                </ul>
                                <h5 class="mb-1">April</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported the budget for farmer training under the New Theory Farmers Promotion Project at an amount of 5,000 THB   </li>
                                </ul>
                                <h5 class="mb-1">May</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored 100 oil palm seedlings to the NSO 61 group to give to Wat Khanabnak School for planting, and Palms produced will be sold to support the school lunch program.</li>  
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported 30 oil palm seedlings to the president of Tha Samet Subdistrict Administrative Organization.  </li>    
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has funded 5,000 THB to support the village security kit budget at Thung Sai Thong Subdistrict.</li>      
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored a sum of 5,000 THB to support the budget for police football games.      </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 5,000 THB to support the religious work of Thai Muslims at Nurulmutakim Mosque.      </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored an amount of 2,000 THB to support the trophy for “Running for charity” at Ban Bok Nine Room School.  </li>
                                </ul>
                                <h5 class="mb-1">June</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported the annual walking-running project at Krabi hospital in the amount of 5,000 THB </li>     
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has provided scholarships at Ban Thung Sung School, Ao Luek, for the amount of 3,000 THB</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company supported 300 oil palm seedlings to the director of Ao Luek Prachasan School.       </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company supported 150 oil palm seedlings to Colonel Yuthnam Petchmuang (Commander of the 15th Infantry Regiment, Vajiravudh Camp Golf Course) to improve the area and landscape at the Army Sports Development Center Region 4 Golf Course. </li>                                       
                                </ul>
                                <h5 class="mb-1">July</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company sponsored athlete uniforms at Ao Luek School for an amount of 3,000 THB    </li>  
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 5,000 THB to support the budget for organizing activities on the annual village headman's day.      </li>
                                </ul>
                                <h5 class="mb-1">August</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 3,000 THB for organizing a sporting event for harmony and good health</li>     
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company provided an athlete uniform for children at the Ban Bang Liao Child Development Center at an amount of 3,000 THB   </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has provided 2,000 THB to support the budget for tree planting in honor of the King.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored the “Love Lamthap” event in remembrance of the Great Buddha in the amount of 10,000 THB.  </li>                                       
                                </ul>
                                <h5 class="mb-1">September</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored 50 oil palm seedlings to Phra Permanent Secretary Posan Panyawaro (Acting Abbot of Wat Bang Ngoen).</li>     
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 2,000 THB for organizing a futsal sporting event.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has helped sponsor the Royal Retirement Awards for an amount of 2,000 THB</li>                                   
                                </ul>
                                <h5 class="mb-1">October</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has helped support the tradition of the annual blessing boat parade at an amount of 2,000 THB </li>     
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 5,000 THB to support walking and running activities (Ao Luek Rai Phung Year 9) to the director of the Ao Luek Hospital</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 2,000 THB to support a charity walking-running activity at Ban Ao Luek Nuea Community School</li>                                   
                                </ul>
                                <h5 class="mb-1">November</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored 2,000 THB to support activities in Loi Krathong, Baan Na Suan, Plai Phraya District.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has helped Support the 12th Love Ao Luek Fair in an amount of 10,000 THB</li>                                   
                                </ul>
                                <h5 class="mb-1">December</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has helped sponsor a prize for the 2020 Phang Nga Red Cross Macha Fair, in the amount of 5,000 THB.</li>  
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 3,000 THB to support the public service checkpoints during the New Year festival 2020, Krabi Province </li>    
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has helped support the red cross shop event award during New Year and Red Cross Fair 2020 in Plai Phraya District For an amount of 5,000 THB</li>      
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has helped support the red cross shop event award New Year and Red Cross Festival 2020 in Lam Thap District, an amount of 5,000 THB </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has helped support the red cross shop event award New Year and Red Cross Festival 2020 in Ao Luek District, an amount of 5,000 THB</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has helped support the red cross shop event award New Year and Red Cross Annual Party 2020 for the Governor General Industry Krabi Province for an amount of 5,000 THB</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has provided an amount of 5,000 THB to the Governor of Krabi to promote the Red Cross Shop and the New Year 2020</li>
                                </ul>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->

        <!--team single section start-->
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="/images/sustainability/sos-17.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-16.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-18.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-19.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-20.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-21.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-22.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-23.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">2018</h4>
                                <h5 class="mb-1">January</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported one 25,300 BTU air conditioner for the Ao Luek Tai Life Quality Development Center.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored 1 computer for Wat Don Maprang School, Cha-Uat District.</li>
                                </ul>
                                <h5 class="mb-1">February</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored 2 activity cards for 4,000 THB for the project to “share smiles, add love, and increase happiness” for the blind in Krabi Province.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 5,000 THB to support audio equipment at Baan Bang Hien village headman.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 2,000 THB to support the organization of sports activities. Santitham Wittaya School, Ban Hin Lad</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has provided the Chor Ror Bor Dress (Moo 4, Ao Luek Tai) for an amount of 10,000 THB.</li>
                                </ul>
                                <h5 class="mb-1">March</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has donated 2,000 THB to donate the Red Cross shop at Ranong Municipality (Kapoe District) </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported the budget to build a check dam at Klong Sinpun Lam Thap at an amount of 20,000 THB. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has joined the merit of 2,000 THB as the host for the Mahachat sermon. Vessantara Chataka </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored 2,000 THB for the Bhumibol Bhikkhu Foundation.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has provided 10,000 THB to support the budget to produce peacekeeping clothes in the village of Ban Klang sub-district.</li>
                                </ul>
                                <h5 class="mb-1">April</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported the budget for football matches for 8 agencies, Plai Phraya Subdistrict Administrative Organization at an amount of 20,000 THB. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company provided 1 office computer and 1 document printer for the amount of 15,890 THB to the Plai Phraya Police Station.</li>
                                </ul>
                                <h5 class="mb-1">June</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company supported 25 oil palm seedlings to Village Headman No. 5, Khlong Thom, to replant the dead trees at the public land of the village. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported a budget of 3,000 THB to support the budget for purchasing sportswear in the 2018 Sports-Athletics event at Ban Huai Sai Mittraphap School, Pa Bon.</li>
                                </ul>
                                <h5 class="mb-1">July</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company donated scholarships to students in Phang Nga Province totaling 10,000 THB, divided into 5 scholarships, 2,000 THB each.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored a sum of 5,000 THB to support the organization of a football match for 5 organizations in Ao Luek Tai Sub-District.</li>
                                </ul>
                                <h5 class="mb-1">August</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has provided an amount of 3,000 THB to support the Village Sports Center (Village No. 5) in Khlong Thom District. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 2,000 THB to support the event of the village headman, Pa Bon District.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported 220 oil palm seedlings to the mayor of Tha Pracha Sub-district.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored a scholarship of Youth football for the ASEAN community Surat Thani Province for an amount of 1,000 THB </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has donated 3 bicycles in the amount of 4,500 THB for the annual Red Cross Fair Khong Dee Muang Lung at Pa Bon district</li>
                                </ul>
                                <h5 class="mb-1">September</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 5,000 THB to support the athletic shirts of the Running for Health Project.</li>
                                </ul>
                                <h5 class="mb-1">October</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has provided water coolers and support equipment to Ban Kuan Din School in Phatthalung Province at an amount of 4,000 THB </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored a sum of 5,000 THB to be the host of Kathin Unity at Laem Takhian Thong Monastery, Pa Bon District</li>
                                </ul>
                                <h5 class="mb-1">November</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has provided 5,000 THB for the donation of Phra Kathin cloth and promoting education to schools in Singburi Province </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company donated a prize for the Loy Krathong Festival of the year 2018 in Pa Bon district in the amount of 2,000 THB.    </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has provided 2,000 THB to the director of Ao Luek Hospital to support the event "Ao Luek No Belly" </li>
                                </ul>
                                <h5 class="mb-1">December</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the Red Cross shop in Krabi Province in Ao Luek District, an amount of 10,000 THB. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the Red Cross shop in Krabi Province in Lam Thap District, an amount of 10,000 THB.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the Red Cross shop in Krabi Province in Plai Phraya District, an amount of 10,000 THB.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has provided an amount of 10,000 THB to support the work capital at "Ten Billion Mining and Phang Nga Red Cross Fair" in Kuraburi District</li>
                                </ul>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->

        <!--team single section start-->
        <section class="about-us-section ptb-100 gray-light-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="/images/sustainability/sos-25.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-26.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-27.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-28.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-29.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-30.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-31.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                            <img src="/images/sustainability/sos-34.jpg" alt="social sustainability" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">2017</h4>
                                <h5 class="mb-1">January</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 2,000 THB to support a football match for 7 senior players, the Provincial Electricity Authority at Lam Thap District. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 5,000 THB to support the charity boxing card of "The Battle of Ao Luek Lovers" to improve the Ao Luek District Auditorium  </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company supports the organization of the International People's Day at Krabi Province in an amount of 2,000 THB for gifts, prizes or food and beverages.</li>
                                </ul>
                                <h5 class="mb-1">March</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has donated decanter cake from the Siam factory in the amount of 5 truckloads to the head of the Than Yogkharnee National Park   </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported the budget for the Din Udom Subdistrict Association Sports Competition No. 19/2017 in the amount of 2,000 THB.  </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored the Thai Red Cross lottery in the Ministry of Industry, amounted to 2,500 THB.</li>
                                </ul>
                                <h5 class="mb-1">May</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has provided 2,000 THB to the village headman to support the budget for sports activities at Ban Bang Pru, Kapoe District.</li>    
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company provided a courtesy and sponsorship of table tennis equipment in Ao Luek School, consisting of a table tennis table with a net, a set of table tennis racket of 12,500 THB per set  </li>      
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has provided an amount of 10,000 THB to Lam Thap District Chief to support travel expenses to pay homage to King Rama IX</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported 500 oil palm seedlings to Surat Thani College of Agriculture and Technology. </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported 220 oil palm seedlings to the Territorial Volunteer Company, Krabi Province to plant in the vacant area to increase income and reduce expenses for the company members.</li>
                                </ul>
                                <h5 class="mb-1">June</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has provided an amount of 10,000 THB to support the addition of a mosque building, Naka sub-district, Ranong province.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has helped to build a fence around the area of ​​Ban Ao Luek Nuea Community School at an amount of 10,000 THB</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored a kit for soccer refereeing for the Kuraburi Referee Team with 3 black shirts and 1 green shirt, totaling 920 THB.</li>                                       
                                </ul>
                                <h5 class="mb-1">August</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored sportswear to the Ban Bang Neaw - Plai Phraya Child Development Center at an amount of 6,300 THB</li>     
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored 3,000 THB to support the 8th “Laem Sak Games” anti-drug sport competition.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported 300 oil palm seedlings to the 43rd Army County Command.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored the 2017 small child sports competition in Ao Luek District for an amount of 2,000 THB.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored a football match for 7 people for the 1st Phaya FC Cup education at Ban Sri Phraya Ratbumrung School at an amount of 2,000 THB</li>                                       
                                </ul>
                                <h5 class="mb-1">September</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company provided a support for building a 4 x 6 meter fortress with a bathroom in the amount of 10,000 THB for Village Headman No. 3.</li>     
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has funded 5,000 THB to support the budget for the Cataract Eye Surgery Campaign Project of Plai Phraya Hospital.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has provided 20,000 THB to support the budget to build a multi-purpose pavilion at Ban Thung Sung School </li>  
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 2,000 THB to support morality training and the family love bond project</li>                                 
                                </ul>
                                <h5 class="mb-1">October</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the arrangement of the moon flower offering ceremony. In the Royal Cremation Ceremony for His Majesty King Rama IX with an amount of 10,000 THB</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored 3,000 THB to donate the royal Kathina cloth and give scholarships in Phatthalung Province</li>                                   
                                </ul>
                                <h5 class="mb-1">November</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored 2,000 THB to support an award at the 2017 Annual General Meeting for the Phang Nga Oil Palm Cooperative.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has sponsored 5,000 THB to sponsor the Miss Nopamas contestant in the Loi Krathong event in Plai Phraya Subdistrict   </li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has supported the budget in organizing a charity football match, Mother of Land Fund in Khuan Khuan Village at an amount of 2,000 THB</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the Red Cross shop in Krabi in Plai Phraya District, an amount of 10,000 THB.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the Red Cross shop in Krabi in Lam Thap district, an amount of 10,000 THB.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has sponsored the award. The 10th Ao Luek Love Fair, an amount of 10,000 THB</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has contributed to the construction of dust-free roads and the sponsorship of the cycling trophy Ban Klongya School with an amount of 3,000 THB</li>                                  
                                </ul>
                                <h5 class="mb-1">December</h5>
                                <ul class="list-unstyled tech-feature-list">
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has supported the Red Cross fair in Krabi province in Ao Luek District, an amount of 10,000 THB.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The company has donated 5,000 THB to support gifts, rewards and food and drinks In organizing the International People's Day Krabi Province</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company has provided an amount of 3,000 THB to students at Ao Luek Tai Municipality School to support snacks or prizes in New Year's Eve activities.</li>
                                    <li class="py-1"><span class="ti-control-forward mr-2 color-secondary"></span>The Company supports the exhibition of Red Cross Annual Fair and Phang Nga Red Cross Fair for an amount of 10,000 THB</li>
                                </ul>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->
        
        <!--call to action section start-->
    <section class="team-single-section ptb-100">
        <div class="container">
            <div class="row justify-content-around align-items-center">
                <div class="col-md-7">
                    <div class="subscribe-content">
                        <h3 class="mb-1">Consulting Agency for Your Business</h3>
                        <p>Rapidiously engage fully tested e-commerce with progressive architectures.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="action-btn text-lg-right text-sm-left">
                        <a href="#" class="btn secondary-solid-btn">Become a Client</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--call to action section end-->

@endsection

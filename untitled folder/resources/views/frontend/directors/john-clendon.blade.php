@extends('frontend.layouts.master')
@section('title', 'John Clendon')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "John Clendon",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Board of Directors',
                            'url' => 'directors'
                        ],

                        [
                            'item_title' => 'John Clendon'
                        ],
                    ]
            ]
        )
        
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="{{ asset('images/team/team-1.jpg') }} " alt="board of directors member" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">Mr. John Clendon</h4>
                                <span>Director</span>
                            </div>
                            <div class="text-content mt-20">
                                <p></p><strong>Age:</strong> 70</p>
                                <strong>Education:</strong>
                                <p>Diplomas in Agriculture and in Valuation and Farm Management, from Lincoln University in New Zealand.</p>
                                <strong>Experience:</strong>
                                <p>From 1980 to 1998 John Clendon was employed by Unilever Plantations and Plant Science Group of the UK, holding various management positions in the Asia-Pacific region, including that of Managing Director of the Univanich joint venture in Thailand from 1991 to 1996. Prior to re-joining Univanich in October 1998, Mr Clendon was Director of Unilever's oil palm operations in East Asia, Chairman and Managing Director of Unipamol Malaysia Sdn Bhd. and director of Unifield PLC in the UK.</p>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->
    
@endsection
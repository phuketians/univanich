@extends('frontend.layouts.master')
@section('title', 'Supapang Chanlongbutra')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Supapang Chanlongbutra",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Board of Directors',
                            'url' => 'directors'
                        ],

                        [
                            'item_title' => 'Supapang Chanlongbutra'
                        ],
                    ]
            ]
        )
        
    
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="{{ asset('images/directors/dir-9.jpg') }} " alt="management team member" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">Ms. Supapang Chanlongbutra</h4>
                                <span>Audit Committee / Independent Director</span>
                            </div>
                            <div class="text-content mt-20">
                                <p></p><strong>Age:</strong> 62</p>
                                <strong>Education:</strong>
                                <p>A degree in Accounting from Thammasart University and Master's degree in Public Administration from the National Institute of Development Administration.</p>
                                <strong>Experience:</strong>
                                <p>Ms Chanlongbutra joined Univanich as a Management Trainee in 1985 and was the Company's Financial Controller from 1994 to 1997 before being promoted to the position of Commercial Manager in charge of the Company's financial and accounting activities. During her 15 years with the Company she participated in Unilever's commercial and management training activities in several countries, and in 1990 was seconded as an overseas auditor in Unilever's Internal Audit Department. In 2000 Ms Chanlongbutra resigned her management position with Univanich to take up management responsibility for her family's business interests in plantations, rubber manufacturing and exporting. Ms Chanlongbutra was appointed to the Univanich Board as an Independent Director on 27 July 2003.</p>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->
    
@endsection
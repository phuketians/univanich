@extends('frontend.layouts.master')
@section('title', 'Chantip Vanich')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Chantip Vanich",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Board of Directors',
                            'url' => 'directors'
                        ],

                        [
                            'item_title' => 'Chantip Vanich'
                        ],
                    ]
            ]
        )
        
    
        <section class="team-single-section ptb-100">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-md-12 col-sm-12 col-lg-5">
                        <div class="team-single-img">
                            <img src="{{ asset('images/directors/dir-5.jpg') }} " alt="board of directors member" class="img-fluid rounded shadow-sm" />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6">
                        <div class="team-single-text">
                            <div class="team-name mb-4">
                                <h4 class="mb-1">Mrs. Chantip Vanich</h4>
                                <span>Director</span>
                            </div>
                            <div class="text-content mt-20">
                                <p></p><strong>Age:</strong> 54</p>
                                <strong>Education:</strong>
                                <p>Mrs. Chantip Vanich graduated MBA in Finance from University of San Francisco, and
                                    has a Bachelor Degree of Science from University of Redlands, California, USA</p>
                                <strong>Experience:</strong>
                                <p>Mrs.Chantip Vanich has a background in banking and finance, as senior Business Analyst and in property development. Since 1991 she has been the Director of Finance and Accounting for the Vanich Group of Companies with interests in Agriculture, Mining, Transport and Property development. She serves on the Boards of a number of other companies including N.C. Property Co.,Ltd. and the Princess Health and Spa Beach Resort Co.,Ltd. Since 1991 Mrs. Chantip Vanich has also served as a Director of the Chean Wannee Vanich Charitable Foundation providing education scholarships and healthcare assistance to local communities.</p>
                                <p>Mrs. Chantip Vanich was appointed to the Univanich Board of Directors on 10th November 2017</p>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team single section end-->
    
@endsection
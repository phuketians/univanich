@extends('frontend.layouts.master')
@section('title', 'Company Profile')
@section('content')


@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Company Profile",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Company Profile',
                        ],
                    ]
            ]
        )

<section class="col_wrap ptb-100" style="min-height: 630px">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('images/company-profile/01.jpg') }} " alt="Company Profile" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <p>Univanich Palm Oil Public Company Limited is a pioneer of the oil palm plantation industry in
                    Thailand with a track record going back to the company’s first plantings in 1969. The company was
                    established on 26th December 1995 as a consolidation of three of Thailand’s pioneer palm oil
                    companies. The three companies were Thai Oil Palm Industry and Estate Company Limited, Siam Palm Oil
                    and Refinery Industry Company Limited, and Hup Huat Palm Oil Industry Company Limited. </p>
                <p>Today, Univanich is one of Thailand’s leading palm oil producers and the country’s largest exporter
                    of crude palm oil, palm kernel oil and high-quality oil palm seeds. The company has been listed on
                    the Stock Exchange of Thailand since 25th November 2005, under the abbreviation of UVAN.</p>
            </div><!-- /.col-md-6 -->
            <div class="col-md-12">

                <p>From 1983 to 1988, Univanich was managed as part of Anglo-Dutch corporation Unilever’s Plantations
                    and Plant Science Group from Cambridge, UK. Since establishment the Company’s emphasis has always
                    been on best environmental and sustainable practice, and building on efficiency improvements and
                    innovation into all our operations.</p>

                <p>Univanich currently operates oil palm plantations in Krabi, Surat Thani, Phatthalung, and Nakhon Si
                    Thammarat Provinces of Southern Thailand, with a total planted area in 2020 of approximately 37,325
                    Rai (6000 Ha). In 2019, the Company’s own plantations produced 9.8% of the fresh fruit bunches
                    (“FFB”) processed by the Company's factories, with 90.2% of FFB being purchased from independent
                    smallholder farmers. </p>
                <p>The overall planted area of oil palms in Thailand is expanding rapidly, and it is this expansion of
                    small-holder plantings that are fueling the Company’s long-term growth.</p>
            </div>

            <div class="col-md-6">
                <p>The Univanich Oil Palm Research Centre (OPRC) is internationally recognized for pioneering oil palm
                    research and advanced palm breeding, employing approximately 100 people and with more than 650 Ha
                    under field trials. Our Oil palm breeding programme was established in 1983, and has since been
                    producing the highest-yielding hybrid seeds for local and overseas oil palm growers. The company’s
                    oil palm nurseries supply annually more than 1.5 million high yielding seedlings to Thailand’s
                    smallholder growers, and our drought-tolerant hybrid seeds are now exported to Oil palm growers in
                    more than fifteen countries. Univanich commissioned Thailand’s first laboratory for Oil palm tissue
                    culture in 2006, which has since then been testing elite oil palm clones in field trials, and in
                    2016 the company commenced production of clonal seeds from these elite oil palm clones.</p>
            </div><!-- /.col-md-6 -->

            <div class="col-md-6">
                <img src="{{ asset('images/company-profile/03.jpg') }}" alt="Company Profile" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-6 -->

            <div class="col-md-12">

                <p>Univanich operates 5 oil palm crushing mills located throughout Southern Thailand, namely in Krabi
                    Province as well as Phang Nga and Phatthalung province. These five factories purchase approximately
                    90% of their palm fruit from smallholder farmers and together produce more than 200,000 tonnes of
                    palm oil annually.</p>

                <p>Univanich also operates a palm oil exporting facility at the deep-water port of Laemphong in Krabi
                    Province. </p>

                <p>Four of the crushing mills operate Methane-Capture Biogas systems to reduce greenhouse gas emissions
                    and to generate approximately 10.5 MW of renewable electricity for our own usage, and for sale to
                    the Provincial Electricity Authority grid, to provide power for the local community</p>

                <img src="{{ asset('images/company-profile/04.jpg' ) }} " alt="Company Profile" class="img-fluid rounded shadow-sm mb-3">

                <p>In 2013, Univanich expanded its business to the Philippines by establishing a 51% owned joint venture
                    company named Univanich Carmen Palm Oil Corporation (UCPOC). UCPOC has constructed a new palm oil
                    crushing mill (30 ton per hour capacity) in North Cotabato province of Mindanao, which was
                    commissioned in 2014 The factory was further expanded (60 ton per hour capacity) and commissioned in
                    March 2020.</p>

                <p>Univanich employs 1270 people in Thailand and ???? people in Philippines. The company is
                    professionally managed to an International standard with support from overseas expert consultants.
                </p>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap -->
<!--promo section start-->
        <section class="call-to-action ptb-100 gradient-overlay"
            style="background: url('/images/slider-img-3.jpg')no-repeat top center / cover fixed">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="app-business-content text-center text-white">
                            <h2 class="text-white">Univanich Family</h2>
                            <p class="lead">The company continues to strive for sustainable growth and expansion, to
                                continue the successes and promote the Thailand Palm oil Industry and Thailand
                                agricultural sector. </p>

                            <div class="action-btns mt-5">
                                <ul class="list-inline app-download-list">
                                    <li class="list-inline-item">
                                        <a href="/operation-map" class="d-flex align-items-center border rounded">
                                            <span class="fab fa-google-play icon-sm mr-3"></span>
                                            <div class="download-text text-left">
                                                <span>Operation Map</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="/investor-relations" class="d-flex align-items-center border rounded">
                                            <span class="fab fa-google-play icon-sm mr-3"></span>
                                            <div class="download-text text-left">
                                                <span>Investor Relations</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="/sustainability" class="d-flex align-items-center border rounded">
                                            <span class="fab fa-google-play icon-sm mr-3"></span>
                                            <div class="download-text text-left">
                                                <span>Sustainability</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--promo section end-->

@endsection

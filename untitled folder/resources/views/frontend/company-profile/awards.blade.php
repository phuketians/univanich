@extends('frontend.layouts.master')
@section('title', 'Awards')
@section('content')


@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Awards",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Company Profile',
                            'url' => 'company-profile'
                        ],

                        [
                            'item_title' => 'Awards'
                        ],
                    ]
            ]
        )
        
<section class="col_wrap ptb-10" style="min-height: 630px">
    <div class="container">
        <div class="row">     

            <div class="col-md-12 mb-12 ptb-100">
                <img src="{{asset('images/awards/05.jpg')}}" alt="" class="img-fluid rounded shadow-sm">
            </div><!-- /.col-md-12 -->

            <div class="col-md-6"> 
                <h5>2005</h5>
                <p class="pl-4"><strong>●   SET AWARDS 2005:</strong> Univanich Palm Oil Public Company Limited as a nominee for Best Performance - Agro & Food</p>

                <img src="{{asset('images/awards/01.jpg')}}" alt="" class="img-fluid rounded shadow-sm mb-4">

                <h5>2009</h5>
                <p class="pl-4"><strong>●   SET AWARDS 2009:</strong> Mr. John Clendon as a nominee for Best CEO Award: CEO Company listed on SET.</p>
                <p class="pl-4"><strong>●   SET AWARDS 2009:</strong> Univanich Palm Oil Public Company Limited as a nominee for Best Performance Award: Market capitalization less than 10 billion baht: 1st group - SET</p>
                <p class="pl-4">●   Forbes Asia. Best Under A Billion. The region’s top 200 Small and Midsize Companies.</p> 

                <img src="{{asset('images/awards/03.jpg')}}" alt="" class="img-fluid rounded shadow-sm mb-4">

                 <h5>2013-2018</h5>
                <p class="pl-4">●   Frost & Sullivan: Thailand Excellence Awards Palm Oil Plantation Company Of The Year</p>   

            </div>

            <div class="col-md-6">
                <h5>2008</h5>
                <p class="pl-4"><strong>●   SET AWARDS 2008:</strong> Univanich Palm Oil plc as a Finalist for the Best Performance Award: Company with market capitalization from THB2,500m. To under THB 10,000m.</p>                              

                <h5>2019</h5>
                <p class="pl-4"><strong>●    SET AWARDS 2019:</strong>Outstanding Innovative Company Awards, Business Excellence, Univanich Palm Oil pcl <br> 
                Innovation: Production of drought-tolerant oil Palm seeds using innovative Tissue Culture and Genomic Techniques</p>

                <img src="{{asset('images/awards/04.jpg')}}" alt="" class="img-fluid rounded shadow-sm mb-4">

                <p class="pl-4">●   Environmental Good Governance Award: Lamthap factory and Pabon factory received certificate for an establishment that meets the requirement of Environmental Good Governance.</p> 
            </div><!-- /.col-md-6 -->                         
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap -->

@endsection
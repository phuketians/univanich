@extends('frontend.layouts.master')
@section('title', 'Investor Relations')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Corporate Governance",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Company Profile',
                            'url' => 'company-profile'
                        ],

                        [
                            'item_title' => 'Corporate Governance'
                        ],
                    ]
            ]
        )


<!--services details start-->
        <section class="service-details-section ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="service-details-wrap">
                            <div class="services-detail-content mt-4">
                                <h5>Corporate Governance</h5>
                                <p>Before becoming a listed company in 2003, Univanich followed the Unilever Code of
                                    Corporate Governance. Since listing, the company has adopted the Stock Exchange of
                                    Thailand (SET) guidelines on Good Corporate Governance. The board of Directors has
                                    also published a Univanich Code of Business Principles to be seen on pages 6-9 of
                                    this Annual Report.</p>
                                <p>Disclosure of the company’s Code of Corporate Governance is included in the Annual
                                    SET Filings (Form 56-1), Annual Report (Form 56-2) and in the listing documents.</p>
                                <p>The 15 principles of Corporate Governance adopted by Univanich are as follows;</p>
                                <ul>
                                    <li><strong>1. Policy on corporate governance</strong></li>
                                    <ul class="list-unstyled tech-feature-list mb-3">
                                        <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>Treat
                                            all shareholders and other stakeholders equally.</li>
                                        <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>The
                                            Board of Directors must manage the Company with diligence and care and
                                            be fully responsible for their duties.</li>
                                        <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>Support
                                            the management and employees to work with ethical conduct.</li>
                                        <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>Set up
                                            an efficient organization structure with the delegation of
                                            decision-making authority to different levels to allow the correct planning,
                                            quick delegation and efficient supervision of the work.</li>
                                        <li class="py-1"><span class="ti-check-box mr-2 color-secondary"></span>Arrange
                                            for risk assessment from both internal and external sources through
                                            regular meetings between management and internal auditors to consider
                                            various
                                            risk factors that could affect the Company’s operation and projections.</li>
                                    </ul>

                                    <li><strong>2. Right and equality of shareholders</strong></li>
                                    <p>In 2019, the Company organized an Annual General Meeting of shareholders at a
                                        venue in central Bangkok for shareholders convenience. Notice of the meeting was
                                        posted on the company’s website one month before the meeting and the company
                                        sent invitation letters to the shareholders at least seven days prior to the
                                        meeting date, in accordance with the Company’s Articles of Association, and also
                                        prepared the minutes of the meeting within 14 days after the meeting date. The
                                        minutes are filed and can be reviewed by shareholders on the company’s website.
                                    </p>

                                    <li><strong>3. Right of other stakeholders</strong></li>
                                    <p>The Company places great importance on the rights of various groups of
                                        stakeholders as follows:</p>
                                    <li class="py-1"><span
                                            class="ti-check-box mr-2 color-secondary"></span><strong>Employees
                                            :</strong>
                                        The Company treats all its employees equally.</li>
                                    <li class="py-1"><span
                                            class="ti-check-box mr-2 color-secondary"></span><strong>Trading
                                            partners :</strong> The Company establishes fair policy and operational
                                        procedures
                                        with its trading partners.</li>
                                    <li class="py-1"><span
                                            class="ti-check-box mr-2 color-secondary"></span><strong>Creditors
                                            :</strong>
                                        The Company conforms with the terms and conditions of any loan.</li>
                                    <li class="py-1"><span
                                            class="ti-check-box mr-2 color-secondary"></span><strong>Clients :</strong>
                                        The Company sells quality products to its clients at fair prices.</li>
                                    <li class="py-1"><span
                                            class="ti-check-box mr-2 color-secondary"></span><strong>Competitors
                                            :</strong> The Company does not conduct business in a way that damages its
                                        competitors.</li>
                                    <li class="py-1"><span
                                            class="ti-check-box mr-2 color-secondary"></span><strong>Society :</strong>
                                        The Company conducts business that does not negatively affect society or
                                        the environment.</li>


                                    <li><strong>4. Right and equality of shareholders</strong></li>
                                    <p>In 2019, the Company organized an Annual General Meeting of shareholders at a
                                        venue in central Bangkok for shareholders convenience. Notice of the meeting was
                                        posted on the company’s website one month before the meeting and the company
                                        sent invitation letters to the shareholders at least seven days prior to the
                                        meeting date, in accordance with the Company’s Articles of Association, and also
                                        prepared the minutes of the meeting within 14 days after the meeting date. The
                                        minutes are filed and can be reviewed by shareholders on the company’s website.
                                    </p>

                                    <li><strong>5. Leadership and Vision</strong></li>
                                    <p>The Board of Directors participates in establishing and approving the Company’s
                                        policies including the business plan, and budgets. The Board constantly monitors
                                        the Company’s progress according such business plan and budgets. This also
                                        involves assigning responsibility to Management to implement such policies
                                        efficiently in order to increase the enterprise’s value and to generate expected
                                        returns to the shareholders, as well as to disclose accurate information to
                                        shareholders in a transparent and timely manner.</p>
                                    <p>Although the Board of Directors has delegated management authority to the
                                        Managing Director, the Board of Directors still retains the highest approval
                                        authority according to the Company’s Articles of Association.</p>

                                    <li><strong>6. Conflicts of Interest</strong></li>
                                    <p>The Company has implemented measures to prevent potential conflicts of interest.
                                        The Company will strictly comply with the SEC Act and SET rules and regulations.
                                        In any transactions in which directors, management or shareholders are persons
                                        whose personal interest may conflict with the Company’s interest, such persons
                                        will not be eligible to vote approval of such transactions. Any such
                                        transactions will be conducted according to the Company’s normal practice under
                                        which the market price will be regarded as a key benchmark and to be comparable
                                        to prices in transactions conducted with unrelated third parties.</p>
                                    <p>The Company publicly discloses its connected party transactions in order to
                                        safeguard investors’ interests. Details can be found in the notes to the audited
                                        financial statements. These connected transactions also have to have Audit
                                        Committee approval regarding the necessity and reasonableness of the
                                        transactions.</p>
                                    <p>In order to prevent exploitation of inside information, the Company strictly
                                        prohibits its directors, management and employees from using any undisclosed
                                        inside information that can affect the share price. The Company shall seek
                                        litigation against any person who is found to use such inside information for
                                        personal benefit.</p>

                                    <li><strong>7. Code of Best Practice</strong></li>
                                    <p>The Company’s directors comply with the code of best practice as set out by SET
                                        guidelines.</p>

                                    <li><strong>8. Directors' Roles</strong></li>
                                    <p>In 2019 , eight directors of the Company are not members of the management team.
                                        Three of these eight directors are Independent Directors who make up the Audit
                                        Committee.</p>

                                    <li><strong>9. Separation of Responsibilities</strong></li>
                                    <p>Currently, the Chairman of the Board of Directors, Mr. Apirag Vanich, does not
                                        hold a management position in the Company. Although Mr. Apirag Vanich is a
                                        representative of the Vanich Group, the major shareholder, he is not the
                                        Managing Director. Therefore, there is added assurance that the company’s
                                        management and policies are independent.</p>

                                    <li><strong>10. Director and Management Remuneration</strong></li>
                                    <p>Compensations and incentives provided to directors and management are comparable
                                        to other companies in the same industry and therefore, are justifiable.</p>
                                    <p>The Company agrees to disclose information concerning director and management
                                        remuneration according to the requirement from the SEC.</p>

                                    <li><strong>11. The Board of Directors' Meeting</strong></li>
                                    <p>The Company organises Board of Directors’ meeting at least once every three
                                        months.
                                        Invitations are sent out to all directors not less than seven days prior to the
                                        meeting,
                                        according to the Company’s Articles of Association. During 2019, the Company
                                        organized four
                                        Board of Directors’ meetings with directors’ participation as follows:</p>

                                    <table class="table responsive table-hover mb-4">
                                        <thead>
                                            <tr>
                                                <th class="text-center"></th>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>2019 Meeting Attended</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">1.</td>
                                                <td>Mr. Apirag Vanich</td>
                                                <td>Chairman</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">2.</td>
                                                <td>Mrs.Phortchana Manoch</td>
                                                <td>Director</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">3.</td>
                                                <td>Ms.Kanchana Vanich</td>
                                                <td>Director</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">4.</td>
                                                <td>Mrs.Chantip Vanich</td>
                                                <td>Director</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">5.</td>
                                                <td>Dr.Prote Sosothikul</td>
                                                <td>Director</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">6.</td>
                                                <td>Mr.John Clendon</td>
                                                <td>Director</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">7.</td>
                                                <td>Dr.Palat Tittinutchanon</td>
                                                <td>Director</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">8.</td>
                                                <td>Mr.Suchad Chiaranussati</td>
                                                <td>Chairman of Audit Committee</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">9.</td>
                                                <td>Ms.Supapang Chanlongbutra</td>
                                                <td>Audit Committee</td>
                                                <td>3</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <li><strong>12. Subcommittees of the Board</strong></li>
                                    <p>In 2019 there were three Board Subcommittees</p>
                                    <li class="py-1"><span
                                            class="ti-check-box mr-2 color-secondary"></span><strong>Audit
                                            Committee:</strong>
                                        The Board created the Audit Committee on 29th July 2003. Members of the Audit
                                        Committee have tenure of three years. During 2019, the Audit Committee conducted
                                        four meetings to consider the implementation of the Company’s Internal Audit
                                        system.</li>
                                    <li class="py-1"><span
                                            class="ti-check-box mr-2 color-secondary"></span><strong>Strategic Planning
                                            Subcommittee:</strong>
                                        On 13th November 2004 the Board created the Strategic Planning Subcommittee to
                                        consider the Company’s’ long-term growth options, the Company’s Long Term Plan,
                                        and other long-term strategic issues including the succession of senior
                                        management.</li>
                                    <li class="py-1"><span
                                            class="ti-check-box mr-2 color-secondary"></span><strong>Nominating and
                                            Remuneration Subcommittee:</strong>
                                        On 11th August 2005 the Board created the Nominating and Remuneration
                                        Subcommittee to review and recommend to the Board the framework of senior
                                        management appointment and remuneration, and to ensure that there is a
                                        succession plan.</li>

                                    <li><strong>13. Internal Control System and Internal Audit</strong></li>
                                    <p>The Company set up an internal audit system on 29th July 2003 to be supervised by
                                        the Audit Committee. This team may include an internal audit firm employed by
                                        the company from time to time to help set up and monitor the internal control
                                        system.</p>

                                    <li><strong>14. Report of the Board of Directors</strong></li>
                                    <p>The Board of Directors appointed the Audit Committee for the purpose of seeking
                                        opinions regarding the necessity and reasonableness of connected transactions,
                                        which were certified and documented in the notes of the financial statement. In
                                        the case where the Audit Committee cannot or does not have the capability to
                                        assess transactions in a reasonable manner, the Company will be responsible for
                                        finding an independent specialist to provide opinions on the transaction on the
                                        Audit Committee’s behalf in order to provide the Board of Directors and
                                        shareholders with the best information for their consideration.</p>

                                    <li><strong>15. Investor Relations</strong></li>
                                    <p>In compliance with SEC requirements, the Company has provided sufficient
                                        information in the filing report and prospectus. The Company will continue to
                                        disclose relevant information to the public in a timely and correct manner with
                                        respect to the SEC and SET regulations. The Company has appointed the Managing
                                        Director and the Commercial Manager as the persons responsible for investor
                                        relations affairs.</p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--services details end-->


        <!--table section start-->
        <section class="col_wrap ptb-10" style="min-height: 630px">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h3>Internal Control</h3>

                        <p>Under the management of Unilever PLC, during the years 1983 - 1998, the company established
                            systems of internal controls which were periodically reviewed by Unilever’s Internal Audit
                            Department. In 2003 the company established its own Internal Audit function, reporting to
                            the Audit Committee which comprises three Independent Directors.</p>
                        <p>During 2004 - 2005, in order to further develop the Company’s internal audit skills the Audit
                            Committee appointed BDO Richfield Ltd, of Bangkok, to carry out risk assessments and audits
                            of key business activities.</p>
                        <p>During 2010 - 2018, the Audit Committee appointed KPMG Phoomchai Business Advisory Ltd., to
                            develop an internal audit plan based on the results of risk assessment and conducting the
                            internal audit execution of key business activities. Details of current internal audit
                            activities are recorded in the Report of the Audit Committee Chairman.</p>
                        <p>During 2019 , the Audit Committee appointed Grant Thornton to review the key business
                            processes relating to the Internal Control implemented by the company. Details of internal
                            audit are recorded in the Report of the Audit Committee Chariman.</p>

                        <div class="col-md-12">
                            <h3>Governance for subsidiaries</h3>

                            <p>In order to manage and monitor the company’s oversea subsidiaries, the Board of Directors
                                has
                                appointed representatives to be directors in these subsidiaries as shown below :</p>

                            <table class="table responsive table-hover mb-4">
                                <thead>
                                    <tr>
                                        <td rowspan="3" style="text-align:center; padding: 0.5em;" width="2%">No.</td>
                                        <td rowspan="3" style="text-align:left; padding: 0.5em;" width="18%">Name</td>
                                        <td colspan="5" style="text-align:center; padding: 0.5em;" width="65%">Position
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" style="text-align:center; padding: 0.5em;">Univanich Palm Oil
                                            PCL<br> (Thailand) </td>

                                        <td width="20%" style="text-align:center; padding: 0.5em;">Univanich
                                            Agribusiness<br>Corporation (Philippines) </td>

                                        <td width="20%" style="text-align:center; padding: 0.5em;">Univanich Carmen
                                            Palm<br>Oil Corporation (Philippines) </td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td class="text-left">1.</td>
                                    <td>Mr.Apirag Vanich</td>
                                    <td style="text-align: center;">Chairman</td>
                                    <td style="text-align: center;">Chairman</td>
                                    <td style="text-align: center;">Director</td>
                                </tr>
                                <tr>
                                    <td class="text-left">2.</td>
                                    <td>Mrs.Phortchana Manoch</td>
                                    <td style="text-align: center;">Director</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-left">3.</td>
                                    <td>Kanchana Vanich</td>
                                    <td style="text-align: center;">Director</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-left">4.</td>
                                    <td>Mrs.Chantip Vanich</td>
                                    <td style="text-align: center;">Director</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-left">5.</td>
                                    <td>Dr.Prote Sosothikul</td>
                                    <td style="text-align: center;">Director</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-left">6.</td>
                                    <td>Mr.John Clendon</td>
                                    <td style="text-align: center;">Director</td>
                                    <td style="text-align: center;">Director</td>
                                    <td style="text-align: center;">Director</td>
                                </tr>
                                <tr>
                                    <td class="text-left">7.</td>
                                    <td>Dr.Palat Tittinutchanon</td>
                                    <td style="text-align: center;">Director</td>
                                    <td></td>
                                    <td style="text-align: center;">Director</td>
                                </tr>
                                <tr>
                                    <td class="text-left">8.</td>
                                    <td>Mr.Suchad Chiaranussati</td>
                                    <td style="text-align: center;">Chairman of Audit Committee</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-left">9.</td>
                                    <td>Ms.Supapang Chanlongbutra</td>
                                    <td style="text-align: center;">Audit Committee</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-left">10.</td>
                                    <td>Mr.Pramoad Phornprapha</td>
                                    <td style="text-align: center;">Audit Committee</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </section>

        <!--table section end-->

        <!--call to action section start-->
        <section class="call-to-action py-5">
            <div class="container">
                <div class="row justify-content-around align-items-center">
                    <div class="col-md-7">
                        <div class="subscribe-content">
                            <h3 class="mb-1">Consulting Agency for Your Business</h3>
                            <p>Rapidiously engage fully tested e-commerce with progressive architectures.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="action-btn text-lg-right text-sm-left">
                            <a href="#" class="btn secondary-solid-btn">Contact With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--call to action section end-->

@endsection
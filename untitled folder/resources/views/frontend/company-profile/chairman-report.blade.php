@extends('frontend.layouts.master')
@section('title', "Chairman's Report")
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Chairman's Report",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Company Profile',
                            'url' => 'company-profile'
                        ],

                        [
                            'item_title' => "Chairman's Report"
                        ],
                    ]
            ]
        )


<section class="col_wrap ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="team-single-img">
                    <img src="/images/team/dir-1.jpg" alt="chairman image" class="img-fluid rounded shadow-sm" />
                </div>
            </div>
            <div class="col-md-8">
                <h5>Chairman's Report</h5>
                <p>The inception of Univanich started when Khun Chean Vanich, along with Khun Osot Kosin and their friends, jointly introduced Oil palm to Southern farmers in 1968. Back then, Southern farmers wholly relied on rubber plantations as their main source of income, and as such perceived Oil palm as an alien and novel crop. During the introduction, the planting areas loomed with bandits, kidnappers and political separatists. Decades of poor rubber prices placed villages in abject poverty, which led to discontent and revolt. As such, robberies, attacks, and even arson at government offices in the area were not uncommon phenomena. Khun Chean’s effort at building a new business in such a volatile environment was seen as most peculiar to the people. Villagers at that time mockingly called him the “Crazy Chinese guy.”</p>
                <p>Regardless of the insurmountable dangers and hardships, Thailand’s very first oil palm plantation surrounded by dens of thieves and separatists was finally established in the province of Krabi in April 1969.</p>
            </div>
            <div class="col-md-12">
                <p> Being a good neighbour and pioneer, Khun Chean loaned quality seedlings to would-be oil palm farmers. He told them that they could pay back through the delivery of palm fruits to his factory, four years later, without interest.  He strongly believed that farmers and business enterprises have to be interdependent and inseparable to achieve mutual success. Thai Oil Palm Industry Company Limited was the very first company established in our history, comprised of a 10 Tonnes per hour crushing mill and 16,000 Rai of plantation area. Hup Huat Palm Oil Co., Ltd. and Siam Palm Oil and Refinery Industry Limited were later added to the group. A joint venture with Unilever, under the Chairmanship of Khun Akapojana Vanich, son of Khun Chean Vanich, signified another chapter in the history of Univanich between 1983-1998.  A new wave of technology, knowledge, people and management systems played a significant part in moulding Univanich into what it is today. The departure of Unilever’s Plant and Plant Science group in 1998 led to the formation of Univanich Palm Oil Public Company Limited in 2000, and the eventual public listing on the Stock Exchange of Thailand in 2003.</p>
                <p>The next big step in our milestone was forming a joint venture in Southern Mindanao, Philippines, and our first factory was commissioned in 2013.  We are determined to take our success story to the Philippines and replicate our “Sharing of Knowledge and Wealth” there in the years to come.</p>
            </div>
        </div>
    </div>
</section>

<section class="col_wrap">
    <div class="container">
        <div class="row">
                <div class="col-md-12">
                <h5>Vision and Business Philosophy</h5>
                <p>Univanich is committed to making a difference wherever we go and to prosper along with local farmers, exemplified by Khun Chean’s case study in 1977. In line with increment prosperity of the farmers, the activities of bandits and separatists declined and eventually died down in the 1990s.</p>

                <p>We believe that we can forge paths and stay ahead of our competitors through science, technology and relentless research and development on things that we do. Throughout our history, Univanich was the first company in Thailand that introduced the insect pollination method that improved yields tremendously. Univanich’s first palm breeding programme was dated back to 1986. The fully tested hybrid seeds of Univanich were planted in 1999. Univanich debuted Thailand’s first Oil Palm tissue culture laboratory in 2006 and consecutively, Thailand’s first generation of Clonal  Oil Palm seeds in 2016.</p>
                <p>On the agricultural side, Univanich was deemed a global pioneer of oil palm irrigation in 1991. We have introduced the “Thinning method” of planting that has helped farmers improve yields by 15%. The unconventional replanting method of Univanich by felling only 50% of old trees and simultaneously planting new ones, has proven to help improve farmers’ revenue stream by as much as 36% over the conventional methods. </p>

                <p>Univanich holds sustainability and environmentally-friendly practices as one of our top priorities, and account for that in all things that we do. In 2003, Univanich plantations and three of our crushing mills received certification from RSPO and CSPO. In the period of 2007-2009, Univanich’s first biogas project was registered by the United Nations as a “Clean Development Mechanism” for greenhouse gas reductions.</p>

                <p>Following the footsteps of our founders who made a difference for Southern Thai farmers, Univanich aims to continue such endeavours in North Cotabato, Philippines: rife with poverty and without constant income. The farmers who converted to oil palms before 2014 can now harvest their fresh fruit bunches every two weeks and sell to our factory, providing much-needed income for the local community. In 2018 and 2019, Univanich had injected 8.8 million and 6.3 million USD consecutively through such transactions with farmers. This direct contribution to the community with a population of only around 100,000 people has been substantial for a sleepy economy; a difference that we are continuously contributing towards.</p>

                <p>On behalf of the Board of Directors, management and staff, may I extend my sincere gratitude to our shareholders for your trust and support through our ups and downs.  Regardless of how far and fast we grow, we will always be a part of the community.  We will continue to make a difference, growing socially and economically while remaining mindful of the environment and our world. </p>
                <div class="thanksRight">
                    <p>Thank you for your support.</p>
                    <p>AV</p><br><br>
                </div>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap -->

@endsection

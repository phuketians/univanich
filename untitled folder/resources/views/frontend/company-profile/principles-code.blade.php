@extends('frontend.layouts.master')
@section('title', "Introduction by Board of Directors")
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Business Principles",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Company Profile',
                            'url' => 'company-profile'
                        ],

                        [
                            'item_title' => 'Business Principles'
                        ],
                    ]
            ]
        )
        
<section class="col_wrap ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <p><strong>Code of Business Principles</strong> <br>
                    Univanich enjoys a reputation for conducting its business with integrity and with respect for the
                    interests of those people and environments our activities can affect. This reputation is an asset,
                    just as real as our people, factories, and brand.</p>

                <p>Our first priority is to be a profitable business and that means investing for growth and balancing
                    short term and long term business interests. It also means caring about our customers, employees,
                    shareholders, suppliers, and the communities and environments in which we conduct our operations.
                </p>

                <p>In the course of meeting our business objectives, we consider it essential that all employees
                    understand and comply with our values and therefore share the Univanich way of doing things.</p>

                <p>It is very easy in the realm of business ethics to make high sounding statements of little practical
                    value. The general principles contained in this Code are the bedrock; more detailed guidance
                    tailored to the needs of different countries and companies will be further developed.</p>

                <p>This Code of Business Principles is a core Univanich statement and we commend it to you.</p>
            </div>

            <div class="col-md-4 pt-4">
                <img src=" {{ asset('images/principles-code/01.jpg') }} " alt="" class="img-fluid rounded shadow-sm mb-4">
            </div><!-- /.col-md-5 -->

            <div class="col-md-12">

                <p><strong> Apirag Vanich</strong> </p>


                <p><strong> Standard of Conduct </strong> <br>
                    Univanich will conduct its business with honesty and integrity and with respect for the interests of
                    those with whom it has relationships.</p>

                <p><strong> Obeying the Law </strong> <br>
                    Univanich will comply with the laws and regulations of the countries in which we operate.</p>

                <p><strong>Employees</strong> <br>
                    Univanich will recruit, employ, and promote employees on the sole basis of the qualifications and
                    abilities needed for the work to be performed. Univanich is committed to providing safe and healthy
                    working conditions for all its employees. Univanich believes it is essential to maintain good
                    communications with employees, through transparent information and consultation procedures.</p>

                <img src="{{ asset('images/principles-code/02.jpg') }}" alt="" class="img-fluid rounded shadow-sm mb-4">

                <p><strong>Conflicts of Interest</strong> <br>
                    Univanich expects its employees to avoid personal activities and financial interests which could
                    conflict with their commitments to their jobs.</p>

                <p><strong>Public Activities</strong><br>
                    Univanich neither supports political parties nor contributes to the funds of groups whose activities
                    are calculated to promote party interests.</p>

                <p>Univanich companies are encouraged to promote and defend their legitimate business interests. In so
                    doing they may either directly, or through bodies such as trade associations, raise questions, and
                    discuss particular government actions or decisions.</p>
                <p>Univanich will respond to requests from governments and other agencies for information, observations,
                    or opinions on issues relevant to business and the community in which operate.</p>

                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ asset('images/principles-code/03.jpg') }}" alt="" class="img-fluid rounded shadow-sm mb-4">
                    </div><!-- /.col-md-6 -->

                    <div class="col-md-6">
                        <p><strong>Product Assurance</strong><br>
                            Univanich is committed to providing products that consistently offer value in terms of price
                            and quality. Where possible this will be supported by ISO certification.</p>

                        <p><strong>Environmental Issues</strong> <br>
                            Univanich is committed to running its business in an environmentally sound and sustainable
                            manner. Our aim is to ensure that our processes and products have the minimum adverse
                            environmental impact commensurate with the legitimate needs of the business. Where possible
                            this will be supported by an appropriate certification of environmental sustainability.</p>

                        <p><strong>Competition </strong> <br>
                            Univanich believes in vigorous yet fair competition and supports the development of
                            appropriate competition laws. Employees receive guidance to ensure that they understand such
                            laws and do not transgress them.</p>

                    </div><!-- /.col-md-6 -->
                </div>

                <p><strong>Reliability of Financial Reporting</strong><br>
                    Univanich accounting records and supporting documents must accurately describe and reflect the
                    nature of the underlying transactions. No undisclosed or unrecorded account, fund, or asset will be
                    established or maintained.</p>

                <p><strong>Bribery</strong><br>
                    Univanich does not give or receive bribes in order to retain or bestow business or financial
                    advantages. Univanich employees are directed that any demand for or offer of such bribe must be
                    immediately rejected.</p>

                <p><strong>Application</strong><br>
                    This Code applies to Univanich’s activities throughout the world. Where Univanich companies
                    participate in joint ventures the application of these principles will be actively promoted.</p>

                <img src="{{ asset('images/principles-code/04.jpg') }}" alt="" class="img-fluid rounded shadow-sm mb-4">

                <p><strong>Compliance</strong><br>
                    It is the responsibility of Univanich managers to ensure that the principles embodied in this Code
                    are communicated to, understood, and observed by all employees. Independent Internal Auditors will
                    support the Board in monitoring compliance with the Code.</p>
                <p>The Board of Univanich will not criticize management for any loss of business resulting from
                    adherence to these principles. Equally, the Board of Univanich undertakes that no employee will
                    suffer as a consequence of bringing to their attention or that of senior management, a breach or
                    suspected breach of these principles.<p>

                        <p>In this Code, the expressions “Univanich” and “Univanich companies” are used for convenience
                            and mean the Univanich</p>
                        <p>Group of companies comprising Univanich Palm Oil Public Company Limited and its subsidiary
                            companies.
                            The Board of Univanich means the Directors of Univanich Palm Oil PCL.
                        </p>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.col_wrap -->


@endsection

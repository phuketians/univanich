@extends('frontend.layouts.master')
@section('title', 'Investor Relations')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Board Of Directors",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Company Profile',
                            'url' => 'company-profile'
                        ],

                        [
                            'item_title' => 'Board Of Directors'
                        ],
                    ]
            ]
        )


    <section class="team-two-section ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src=" {{ asset('images/directors/dir-1.jpg' ) }} " alt="Mr. Apirag Vanich" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr. Apirag Vanich</h5>
                                <span>Chairman - Age 54</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/directors/apirag-vanich" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr. Apirag Vanich</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/directors/dir-2.jpg' ) }}" alt="Mrs. Phortchana Manoch" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mrs. Phortchana Manoch</h5>
                                <span>Director</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/directors/phortchana-manoch" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mrs. Phortchana Manoch</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/directors/dir-3.jpg' ) }}" alt="Ms. Kanchana Vanich" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Ms. Kanchana Vanich</h5>
                                <span>Director</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/directors/kanchana-vanich" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Ms. Kanchana Vanich</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/directors/team-3.jpg' ) }}" alt="Dr. Palat Tittinutchanon" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Dr. Palat Tittinutchanon</h5>
                                <span>Director</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/directors/palat-tittinutchanon" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Dr. Palat Tittinutchanon</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/directors/dir-6.jpg' ) }}" alt="Dr. Prote Sosothikul" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Dr. Prote Sosothikul</h5>
                                <span>Director</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/directors/prote-sosothikul" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Dr. Prote Sosothikul</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/directors/team-1.jpg' ) }}" alt="Mr. John Clendon" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr. John Clendon</h5>
                                <span>Group Chief Executive Officer</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/directors/john-clendon" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr. John Clendon</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/directors/dir-8.jpg' ) }}" alt="Mr. Suchad Chiaranussati" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr. Suchad Chiaranussati</h5>
                                <span>Chairman of the Audit Committee</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/directors/suchad-chiaranussati" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr. Suchad Chiaranussati</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/directors/dir-9.jpg' ) }}" alt="Ms. Supapang Chanlongbutra" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Ms. Supapang Chanlongbutra</h5>
                                <span>Audit Committee, Independent Director</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/directors/supapang-chanlongbutra" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Ms. Supapang Chanlongbutra</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="staff-member my-lg-3 my-md-3 my-sm-0">
                        <div class="card gray-light-bg text-center border-0">
                            <img src="{{ asset('images/directors/dir-10.jpg' ) }}" alt="Mr. Pramoad Phornprapha" class="card-img-top">
                            <div class="card-body">
                                <h5 class="teacher mb-0">Mr. Pramoad Phornprapha</h5>
                                <span>Audit Committee, Independent Director</span>
                            </div>
                        </div>
                        <div class="overlay d-flex align-items-center justify-content-center">
                            <div class="overlay-inner">
                                <a href="/directors/pramoad-phornprapha" class="teacher-name">
                                <h5 class="mb-0 teacher text-white">Mr. Pramoad Phornprapha</h5></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
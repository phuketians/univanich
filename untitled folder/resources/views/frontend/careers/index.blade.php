@extends('frontend.layouts.master')
@section('title', 'Careers')
@section('content')

@include('frontend.layouts.breadcrumb', 
            [
                'page_title' => "Careers Center",
                'b_items' => [
                        [
                            'item_title' => 'Home',
                            'url' => 'home'
                        ],

                        [
                            'item_title' => 'Careers',
                            'url' => 'careers'
                        ],

                        [
                            'item_title' => 'Careers Center'
                        ],
                    ]
            ]
        )
<section class="col_wrap ptb-100" style="min-height: 630px">
    <div class="container">
    <br><h5><center>Careers Information</center></h5><br>
    <p><center>Coming Soon</center></p>
    </div><!-- /.container -->
</section><!-- /.col_wrap -->

@endsection
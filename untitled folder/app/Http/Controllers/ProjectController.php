<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\Project;
use App\ProjectStatus;
use App\Region;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    public function index()
    {
        $projects = Project::paginate(20);

        $data['projects'] = $projects;

        $regions = Region::get()->pluck('name', 'id');

        $data['regions'] = $regions;

        $projectStatuses = ProjectStatus::get()->pluck('name', 'id');

        $data['projectStatuses'] = $projectStatuses;

        return view('backend.project.index', $data);
    }


    public function create()
    {
        //
    }


    public function store(ProjectRequest $request)
    {
        Project::create($request->all());

        return redirect()->back()->with('success', 'Project Added Successfully!');
    }


    public function show(Project $project)
    {
        //
    }


    public function edit(Project $project)
    {
        $projects = Project::paginate(20);

        $data['projects'] = $projects;

        $data['project'] = $project;

        $regions = Region::get()->pluck('name', 'id');

        $data['regions'] = $regions;

        $projectStatuses = ProjectStatus::get()->pluck('name', 'id');

        $data['projectStatuses'] = $projectStatuses;

        return view('backend.project.edit', $data);
    }


    public function update(Request $request, Project $project)
    {
        $project->update($request->all());

        return redirect()->back()->with('success', 'Project Updated Successfully!');
    }


    public function destroy(Project $project)
    {
        $project->delete();

        return redirect()->route('projects.index')->with('success', 'Project Deleted Successfully!');
    }
}

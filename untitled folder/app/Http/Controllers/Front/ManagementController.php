<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class ManagementController extends Controller
{
    public function index()
    {
        return view('frontend.management-team.index');
    }
    
    public function johnClendon()
    {
        return view('frontend.management-team.john-clendon');
    }
    
    public function harryBrock()
    {
        return view('frontend.management-team.harry-brock');
    }
    
    public function palatTittinutchanon()
    {
        return view('frontend.management-team.palat-tittinutchanon');
    }
    
    public function nattapongDachanabhirom()
    {
        return view('frontend.management-team.nattapong-dachanabhirom');
    }
    
    public function phiphitKhlaisombat()
    {
        return view('frontend.management-team.phiphit-khlaisombat');
    }
    
    public function santiSuanyot()
    {
        return view('frontend.management-team.santi-suanyot');
    }
    
    public function thanapholLeelangamwong()
    {
        return view('frontend.management-team.thanaphol-leelangamwong');
    }
    
    
}
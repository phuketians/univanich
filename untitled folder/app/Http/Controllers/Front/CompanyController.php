<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    
    public function index()
    {
        return view('frontend.company-profile.index');
    }

    public function chairmanReport()
    {
        return view('frontend.company-profile.chairman-report');
    }

    public function principlesCode()
    {
        return view('frontend.company-profile.principles-code');
    }

    public function corporateGovernance(){
        return view('frontend.company-profile.corporate-governance');
    }

    public function timeline()
    {
        return view('frontend.company-profile.timeline');
    }

    public function boardOfDirectors()
    {
        return view('frontend.company-profile.board-of-directors');
    }

    public function managementTeam()
    {
        return view('frontend.company-profile.management-team');
    }

    public function awards()
    {
        return view('frontend.company-profile.awards');
    }
}

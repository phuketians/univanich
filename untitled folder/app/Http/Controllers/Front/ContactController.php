<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    
    public function index()
    {
        return view('frontend.contact.index');
    }

    public function whistleblowing()
    {
        return view('frontend.contact.whistleblowing');
    }

    public function feedbackInquiries()
    {
        return view('frontend.contact.feedback-inquiries');
    }


}

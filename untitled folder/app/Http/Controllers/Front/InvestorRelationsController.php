<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class InvestorRelationsController extends Controller
{
    
    public function index()
    {
        return view('frontend.investor-relations.index');
    }

    public function financialInformation()
    {
        return view('frontend.investor-relations.financial-information');
    }

    public function shareholder()
    {
        return view('frontend.investor-relations.shareholder');
    }

    public function publications()
    {
        return view('frontend.investor-relations.publications');
    }

}

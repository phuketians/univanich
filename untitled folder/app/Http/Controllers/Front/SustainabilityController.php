<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class SustainabilityController extends Controller
{
    
    public function index()
    {
        return view('frontend.sustainability.index');
    }

    public function social()
    {
        return view('frontend.sustainability.social-sustainability');
    }

    public function environmental()
    {
        return view('frontend.sustainability.environmental-sustainability');
    }

    public function economic()
    {
        return view('frontend.sustainability.economic-sustainability');
    }

    public function scientific()
    {
        return view('frontend.sustainability.scientific-sustainability');
    }

    public function corporate()
    {
        return view('frontend.sustainability.corporate-governance');
    }

}

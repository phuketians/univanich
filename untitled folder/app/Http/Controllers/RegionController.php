<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegionRequest;
use App\Region;

class RegionController extends Controller
{

    public function index()
    {
        $regions = Region::paginate(20);

        $data['regions'] = $regions;

        return view('backend.region.index', $data);
    }


    public function create()
    {
        //
    }


    public function store(RegionRequest $request)
    {
        Region::create($request->all());

        return redirect()->back()->with('success', 'Region Added Successfully!');
    }


    public function show($id)
    {
        //
    }


    public function edit(Region $region)
    {
        $regions = Region::paginate(20);

        $data['regions'] = $regions;

        $data['region'] = $region;  

        return view('backend.region.edit', $data);
    }


    public function update(RegionRequest $request, Region $region)
    {
        $region->update($request->all());

        return redirect()->back()->with('success', 'Region Updated Successfully!');
    }


    public function destroy(Region $region)
    {
        $region->delete();

        return redirect()->route('regions.index')->with('success', 'Region Deleted Successfully!');
    }
}

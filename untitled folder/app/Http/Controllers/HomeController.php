<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.index');
    }


 
 

    public function operationMap()
    {
        return view('frontend.operations.operation-map');
    }

  


    public function projects()
    {
        return view('frontend.projects');
    }

    public function projectDetails()
    {
        return view('frontend.project-details');
    }
}

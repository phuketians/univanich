<?php
$assetsLogoImages = array_slice(scandir('../assets/images/logo'), 2);

// convert the list to symbol => image associative array, e.g. ['AAPL' => 'AAPL.png']
$assetsLogoImages = array_combine(array_map(function ($image) {
    return substr($image, 0, strrpos($image, '.'));
}, $assetsLogoImages), $assetsLogoImages);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Premium Stock Market Widgets | Example</title>

  <link rel="stylesheet" type="text/css" href="../assets/dist/main.css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.10.0/styles/default.min.css">

  <style>
  h2 {
    border-bottom: 1px solid darkgrey;
  }

  .container {
    width: 80%;
    max-width: 1200px;
    margin-left: auto;
    margin-right: auto;
  }

  .flex {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  footer {
    margin-top: 3em;
    padding-top: 1em;
    border-top: 1px solid darkgrey;
  }
  </style>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<div class="container">
  <div>
    <h1>Premium Stock Market Widgets</h1>

    <h2>Usage example</h2>

    <div class="flex">
      <div>
        <stock-market-widget type="card" template="chart3" color="#5679FF" assets="MSFT" api="yf" chart_range="1mo" chart_interval="1d"></stock-market-widget>
      </div>
      <div>
        <stock-market-widget type="card" template="chart3" color="#5679FF" assets="AAPL" api="yf" chart_range="1mo" chart_interval="1d"></stock-market-widget>
      </div>
      <div>
        <stock-market-widget type="card" template="chart3" color="#5679FF" assets="FB" api="yf" chart_range="1mo" chart_interval="1d"></stock-market-widget>
      </div>
    </div>

    <br>
    <br>

    <stock-market-widget type="leaderboard" template="color-frame" color="#5679FF" assets="AAPL" api="yf"></stock-market-widget>

    <br>
    <br>

    <stock-market-widget type="table-quotes" template="basic" assets="MSFT,AAPL,FB" fields="logo_name,price,change_abs,change_pct,market_cap" display_header="true" display_chart="true" pagination="true" display_currency_symbol="true" search="false"
                         rows_per_page="5" sort_field="logo_name" sort_direction="asc" alignment="left_last_right" api="yf"></stock-market-widget>

    <br>
    <br>

    <stock-market-widget type="chart" template="basic" color="#5679FF" assets="AAPL" range="1mo" interval="1d" axes="true" range_selector="true" cursor="true" api="yf" style="height:300px;"></stock-market-widget>

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <stock-market-widget type="comparison" template="cards" color="#5679FF" assets="MSFT,AAPL,FB" fields="name,change_abs,change_pct,volume,dividend_yield,eps,pe_ratio,shares_outstanding,market_cap,chart" api="yf" chart_range="1mo"
                         chart_interval="1d"></stock-market-widget>
  </div>

<div>
  <stock-market-widget type="card" template="basic2" assets="UVAN.BK" api="yf"></stock-market-widget>
</div>
  

  <footer>
    &copy; <a href="https://financialplugins.com/" target="_blank">FinancialPlugins.com</a>
  </footer>
</div>
<script>
var premiumStockMarketWidgets = {
  pluginUrl: '..',
  ajaxUrl: '../ajax.php',
  assetsLogoImages: JSON.parse('<?php print json_encode($assetsLogoImages)?>')
}
</script>
<script src="../assets/dist/app.js"></script>
</body>
</html>
